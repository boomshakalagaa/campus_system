package com.example.campus_system.aop;

import com.example.campus_system.entity.Log;
import com.example.campus_system.service.LogService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;

@Aspect
@Component
public class ControllerAspect {


    @Autowired
    HttpServletRequest httpServletRequest;

    @Autowired
    LogService logService;

    private Object runAndSaveLog(ProceedingJoinPoint pjp, boolean cstresult) throws UnsupportedEncodingException {

        long beginTime = System.currentTimeMillis();
        Object result = null;
        try {
            result = pjp.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();

        }
        finally {

        }
        //用时
        String time = String.valueOf(System.currentTimeMillis()-beginTime);
        //操作
        String log_operation = pjp.getSignature().getName();
        //访问地址
        String log_access_addr = httpServletRequest.getRequestURL().toString();
        //访问路径
        String log_access_path = httpServletRequest.getServletPath();
        //来源ip
        String log_ip = httpServletRequest.getRemoteAddr();
        //访问方法
        String log_access_method = pjp.getSignature().toString();
        //访问controller
        String log_access_controller = pjp.getTarget().getClass().toString();
        //访问时间
        String log_access_time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis()));
        //参数
        String log_parameter = "null";
        if(!(httpServletRequest.getQueryString() ==null)){
            log_parameter=URLDecoder.decode(httpServletRequest.getQueryString(),"UTF-8");
        }
        Log log = new Log();
        log.setLog_operation(log_operation);
        log.setLog_access_addr(log_access_addr);
        log.setLog_access_path(log_access_path);
        log.setLog_ip(log_ip);
        log.setLog_access_method(log_access_method);
        log.setLog_access_controller(log_access_controller);
        log.setLog_access_time(log_access_time);
        log.setLog_time_use(time);
        log.setLog_parameter(log_parameter);

        /**
         * 记录日志
         */
        try {
            logService.addLog(log);
        }catch (Exception e){
            e.printStackTrace();
        }



        return result;
    }

    @Around("@within(org.springframework.stereotype.Controller)")
    Object aroundController(ProceedingJoinPoint pjp) throws UnsupportedEncodingException {
        return runAndSaveLog(pjp,false);
    }

    @Around("@within(org.springframework.web.bind.annotation.RestController)")
    Object aroundRestController(ProceedingJoinPoint pjp) throws UnsupportedEncodingException {
        return runAndSaveLog(pjp,false);
    }

}
