package com.example.campus_system.service;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.SystemPermissions;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SystemPermissionsService {

    /**
     * 管理员
     * 查询系统权限
     * 权限英文名，权限中文名，权限描述
     */
    BaseResponse<List<SystemPermissions>> selectSystemPermissions();

    /**
     * 管理员
     * 编辑系统权限
     * 权限英文名，权限中文名，权限描述
     */
    BaseResponse editSystemPermissions(SystemPermissions systemPermissions);

}
