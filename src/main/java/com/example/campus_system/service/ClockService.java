package com.example.campus_system.service;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.Clock;
import com.example.campus_system.entity.StudentInfo;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Service
public interface ClockService {

    /**
     * 管理员
     * 查询打卡
     * @return
     */
    BaseResponse<List<Clock>> adminSelectAllClock();

    /**
     * 管理员
     * 搜索打卡
     * @return
     */
    BaseResponse<List<Clock>> adminSearchClock(String user_id,String user_faculty,String cough,String fever,String temperature);

    /**
     * 管理员
     * 打卡
     * @return
     */
    BaseResponse adminAddClock(String user_id,String clock_time_type,String clock_area ,String temperature,String cough,String fever);

    /**
     * 管理员,辅导员
     * 删除打卡
     * @return
     */
    BaseResponse deleteClock(String clock_id);

    /**
     * 学生
     * 查询打卡
     * @return
     */
    BaseResponse<List<Clock>> studentSelectClock(String stu_id);

    /**
     * 学生
     * 查询打卡
     * @return
     */
    BaseResponse<List<Clock>> studentAddClock(String stu_id,String clock_time_type, String clock_area , String temperature, String cough, String fever);

    /**
     * 老师
     * 查询班级打卡
     * @return
     */
    BaseResponse<List<Clock>> teacherSelectAllClock(String teacher_id);

    /**
     * 老师
     * 查询个人打卡
     * @return
     */
    BaseResponse<List<Clock>> teacherSelectPSClock(String user_id);

    /**
     * 老师
     * 搜索班级打卡
     * @return
     */
    BaseResponse<List<Clock>> teacherSearchClock(String user_id,String cough, String fever, String temperature, String teacher_id);


    /**
     * 老师
     * 查询班级未打卡
     * @return
     */
    BaseResponse<List<StudentInfo>> teacherSearchNotClock(String teacher_id);
}
