package com.example.campus_system.service;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.Log;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface LogService {

    /**
     * 记录日志
     */
    void addLog(Log log);

    /**
     * 查询日志
     */
    BaseResponse<List<Log>> selectLog();

    /**
     * 搜索日志
     */
    BaseResponse<List<Log>> searchLog(String search);
}
