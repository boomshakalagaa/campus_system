package com.example.campus_system.service;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.ClockNum;
import com.example.campus_system.entity.LeaveNum;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IndexService {


    /**
     * 查日志数，通知数，学生数，教师数，申请数，核算报告数
     * @return
     */
    BaseResponse selectNum();


    /**
     * 查离校
     * @return
     */

    List<LeaveNum> selectmap1();

    /**
     * 查打卡数
     * @return
     */

    List<ClockNum> selectmap2();
}
