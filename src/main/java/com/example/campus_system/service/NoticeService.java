package com.example.campus_system.service;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.Notice;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Service
public interface NoticeService {

    /**
     * 查询总通知
     * @return
     */
    BaseResponse<List<Notice>> selectNotice();

    /**
     * 查询学校通知
     * @return
     */
    BaseResponse<List<Notice>> selectIndexNotice();

    /**
     * 查询教工通知
     * @return
     */
    BaseResponse<List<Notice>> selectTeacherNotice(String teacher_id);

    /**
     * 查询班级通知
     * @return
     */
    BaseResponse<List<Notice>> selectClassNotice(String stu_id);

    /**
     * 添加通知
     * @return
     */
    BaseResponse insertNotice(Notice notice);

    /**
     * 通知审核
     * @return
     */
    BaseResponse updateNotice(String notice_id,String notice_state);

    /**
     * 删除通知
     * @return
     */
    BaseResponse deleteNotice(String notice_id);

    /**
     * 搜索通知
     * @return
     */
    BaseResponse<List<Notice>> searchNotice(String search ,String searchtype);

    /**
     * * 学生
     * 搜索该班级通知
     * @return
     */
    BaseResponse<List<Notice>> searchNoticeClass(String search,String stu_id);

    /**
     * 学生
     * 搜索通知公告
     * @return
     */
    BaseResponse<List<Notice>> stuSearchNotice(String search,String searchtype);

    /**
     * 老师
     * 搜索该院系教工通知
     * @return
     */
    BaseResponse<List<Notice>> searchNoticeTeacher(String search);

    /**
     * 老师
     * 查询自己班级通知
     * @return
     */
    BaseResponse<List<Notice>> TeacherselectClassNotice(String teacher_id);


    /**
     * 老师
     * 搜索该班级通知
     * @return
     */
    BaseResponse<List<Notice>> teacherSearchClassNotice(String search,String teacher_id);

}
