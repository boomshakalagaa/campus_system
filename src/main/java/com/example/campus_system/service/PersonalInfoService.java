package com.example.campus_system.service;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.StudentInfo;
import com.example.campus_system.entity.TeacherInfo;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
public interface PersonalInfoService {

    /**
     * 查询教师个人信息
     */
    BaseResponse<TeacherInfo> selectTeacherInfo(String user_id);

    /**
     *老师或管理员
     *修改个人信息
     */
    BaseResponse updateTechaerInfo(TeacherInfo teacherInfo);

    /**
     * 查询学生个人信息
     */
    BaseResponse<StudentInfo> selectStudentInfo(String stu_id);

    /**
     *学生
     *修改个人信息
     */
    BaseResponse updateStudentInfo(StudentInfo studentInfo);

}
