package com.example.campus_system.service;

import com.example.campus_system.entity.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface EpidemicInfoService {

    BaseResponse<G_data> selectEpmcInfo();

    BaseResponse<List<P_data>> selectPEpmcInfo();

    BaseResponse<List<C_data>> selectCEpmcInfo(String p_name);
}
