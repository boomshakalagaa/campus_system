package com.example.campus_system.service;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.Report;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public interface ReportService {

    /**
     * 查询所有核算报告
     * @return
     */
    BaseResponse<List<Report>> selectReportAll();

    /**
     * 搜索报告
     * @return
     */
    BaseResponse<List<Report>> searchReport(String stu_id,String stu_name,String stu_faculty);

    /**
     *学生
     *查询核酸报告
     */
    BaseResponse<Report> StudentselectReport(String stu_id);


    /**
     *学生
     *上传核酸报告
     */
    BaseResponse uploadReport(Report report);

    /**
     * 老师
     * 查询班级报告
     * @return
     */
    BaseResponse<List<Report>> teacherSelectReport(String teacher_id);


    /**
     * 老师
     * 搜索报告
     * @return
     */
    BaseResponse<List<Report>> teacherSearchReport(String stu_id,String teacher_id);

}
