package com.example.campus_system.service.impl;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.C_data;
import com.example.campus_system.entity.G_data;
import com.example.campus_system.entity.P_data;
import com.example.campus_system.mapper.EpidemicInfoMapper;
import com.example.campus_system.service.EpidemicInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EpidemicInfoServiceImpl implements EpidemicInfoService {

    @Autowired
    EpidemicInfoMapper epidemicInfoMapper;

    @Override
    public BaseResponse<G_data> selectEpmcInfo() {
        BaseResponse<G_data> baseResponse = new BaseResponse<>();
        G_data g_data = epidemicInfoMapper.select_G_data();
        baseResponse.setData(g_data);
        return baseResponse;
    }

    @Override
    public BaseResponse<List<P_data>> selectPEpmcInfo() {
        BaseResponse<List<P_data>> baseResponse = new BaseResponse<>();
        List<P_data> p_data = epidemicInfoMapper.select_P_data();
        baseResponse.setData(p_data);
        return baseResponse;
    }

    @Override
    public BaseResponse<List<C_data>> selectCEpmcInfo(String p_name) {
        BaseResponse<List<C_data>> baseResponse = new BaseResponse<>();
        List<C_data> c_data = epidemicInfoMapper.select_C_data(p_name);
        baseResponse.setData(c_data);
        return baseResponse;
    }
}
