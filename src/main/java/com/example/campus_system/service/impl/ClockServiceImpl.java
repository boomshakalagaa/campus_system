package com.example.campus_system.service.impl;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.Clock;
import com.example.campus_system.entity.StudentInfo;
import com.example.campus_system.entity.TeacherInfo;
import com.example.campus_system.mapper.PersonalInfoMapper;
import com.example.campus_system.mapper.SystemPermissionMapper;
import com.example.campus_system.mapper.ClockMapper;
import com.example.campus_system.mapper.UserMapper;
import com.example.campus_system.service.ClockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class ClockServiceImpl implements ClockService {

    @Autowired
    ClockMapper clockMapper;

    @Autowired
    PersonalInfoMapper personalInfoMapper;

    @Autowired
    UserMapper userMapper;


    /**
     * 管理员
     * 查询打卡
     * @return
     */
    @Override
    public BaseResponse<List<Clock>> adminSelectAllClock() {
        BaseResponse<List<Clock>> baseResponse = new BaseResponse<>();
        List<Clock> clocks = clockMapper.adminSelectAllClock();
        if(null == clocks || clocks.size() ==0 ){
            baseResponse.setCode(500);
            baseResponse.setMsg("暂无信息");

        }else {
            baseResponse.setCode(200);
            baseResponse.setMsg("查询成功");
            baseResponse.setData(clocks);
        }
        return baseResponse;
    }

    /**
     * 管理员
     * 搜索打卡
     * @return
     */
    @Override
    public BaseResponse<List<Clock>> adminSearchClock(String user_id,String user_faculty,String cough,String fever,String temperature) {
        BaseResponse<List<Clock>> baseResponse = new BaseResponse<>();
        List<Clock> clocks = clockMapper.adminSearchClock(user_id,user_faculty,cough,fever,temperature);
        if(null == clocks || clocks.size() ==0 ){
            baseResponse.setCode(500);
            baseResponse.setMsg("暂无申请");

        }else {
            baseResponse.setCode(200);
            baseResponse.setMsg("查询成功");
            baseResponse.setData(clocks);
        }
        return baseResponse;
    }

    /**
     * 管理员，老师
     * 打卡
     * @return
     */
    @Override
    public BaseResponse adminAddClock(String user_id, String clock_time_type, String clock_area, String temperature, String cough, String fever) {
        BaseResponse baseResponse = new BaseResponse();
        TeacherInfo teacherInfo = personalInfoMapper.selectTeacherInfo(user_id);
        int rows = clockMapper.adminAddClock(user_id,teacherInfo.getTeacher_name(),teacherInfo.getTeacher_faculty(),new SimpleDateFormat("yyyy-MM-dd").format(new Date(System.currentTimeMillis())),clock_time_type,clock_area,temperature,cough,fever);
        if(rows>0){
            baseResponse.setCode(200);
            baseResponse.setMsg("打卡成功");
        }else {
            baseResponse.setCode(500);
            baseResponse.setMsg("失败");
        }
        return baseResponse;
    }

    /**
     * 管理员,辅导员
     * 删除打卡
     * @return
     */
    @Override
    public BaseResponse deleteClock(String clock_id) {
        BaseResponse baseResponse = new BaseResponse();
        int rows = clockMapper.deleteClock(clock_id);
        if(rows>0){
            baseResponse.setCode(200);
            baseResponse.setMsg("删除成功");
        }else {
            baseResponse.setCode(500);
            baseResponse.setMsg("删除失败");
        }
        return baseResponse;
    }


    /**
     * 学生
     * 查询打卡
     * @return
     */
    @Override
    public BaseResponse<List<Clock>> studentSelectClock(String stu_id) {
        BaseResponse<List<Clock>> baseResponse = new BaseResponse<>();
        List<Clock> clocks = clockMapper.studentSelectClock(stu_id);
        if(null == clocks || clocks.size() ==0 ){
            baseResponse.setCode(500);
            baseResponse.setMsg("暂无信息");

        }else {
            baseResponse.setCode(200);
            baseResponse.setMsg("查询成功");
            baseResponse.setData(clocks);
        }
        return baseResponse;
    }

    /**
     * 学生
     * 打卡
     * @return
     */
    @Override
    public BaseResponse<List<Clock>> studentAddClock(String stu_id,String clock_time_type,String clock_area,String temperature,String cough,String fever) {
        BaseResponse baseResponse = new BaseResponse();
        StudentInfo studentInfo = userMapper.selectStudent(stu_id);
        int rows = clockMapper.studentAddClock(stu_id,studentInfo.getStu_name(),studentInfo.getStu_faculty(),studentInfo.getStu_class(),new SimpleDateFormat("yyyy-MM-dd").format(new Date(System.currentTimeMillis())),clock_time_type,clock_area,temperature,cough,fever);
        if(rows>0){
            baseResponse.setCode(200);
            baseResponse.setMsg("打卡成功");
        }else {
            baseResponse.setCode(500);
            baseResponse.setMsg("失败");
        }
        return baseResponse;
    }

    /**
     * 老师
     * 查询班级打卡
     * @return
     */
    @Override
    public BaseResponse<List<Clock>> teacherSelectAllClock(String teacher_id) {
        BaseResponse<List<Clock>> baseResponse = new BaseResponse<>();
        String user_class=userMapper.selectClass(teacher_id);
        List<Clock> clocks = clockMapper.teacherSelectAllClock(user_class);
        if(null == clocks || clocks.size() ==0 ){
            baseResponse.setCode(500);
            baseResponse.setMsg("暂无信息");

        }else {
            baseResponse.setCode(200);
            baseResponse.setMsg("查询成功");
            baseResponse.setData(clocks);
        }
        return baseResponse;
    }

    /**
     * 老师
     * 查询个人打卡
     * @return
     */
    @Override
    public BaseResponse<List<Clock>> teacherSelectPSClock(String user_id) {
        BaseResponse<List<Clock>> baseResponse = new BaseResponse<>();
        List<Clock> clocks = clockMapper.teacherSelectPSClock(user_id);
        if(null == clocks || clocks.size() ==0 ){
            baseResponse.setCode(500);
            baseResponse.setMsg("暂无信息");

        }else {
            baseResponse.setCode(200);
            baseResponse.setMsg("查询成功");
            baseResponse.setData(clocks);
        }
        return baseResponse;
    }

    /**
     * 老师
     * 搜索班级打卡
     * @return
     */
    @Override
    public BaseResponse<List<Clock>> teacherSearchClock(String user_id, String cough, String fever, String temperature, String teacher_id) {
        BaseResponse<List<Clock>> baseResponse = new BaseResponse<>();
        String user_class = userMapper.selectClass(teacher_id);
        List<Clock> clocks = clockMapper.teacherSearchClock(user_id,cough,fever,temperature,user_class);
        if(null == clocks || clocks.size() ==0 ){
            baseResponse.setCode(500);
            baseResponse.setMsg("暂无信息");

        }else {
            baseResponse.setCode(200);
            baseResponse.setMsg("查询成功");
            baseResponse.setData(clocks);
        }
        return baseResponse;
    }


    /**
     * 老师
     * 查询班级未打卡
     * @return
     */
    @Override
    public BaseResponse<List<StudentInfo>> teacherSearchNotClock(String teacher_id) {
        BaseResponse<List<StudentInfo>> baseResponse = new BaseResponse<>();
        String user_class = userMapper.selectClass(teacher_id);
        List<StudentInfo> clocks = clockMapper.teacherSearchNotClock(new SimpleDateFormat("yyyy-MM-dd").format(new Date(System.currentTimeMillis())),user_class);
        if(null == clocks || clocks.size() ==0 ){
            baseResponse.setCode(500);
            baseResponse.setMsg("暂无信息");

        }else {
            baseResponse.setCode(200);
            baseResponse.setMsg("查询成功");
            baseResponse.setData(clocks);
        }
        return baseResponse;
    }
}
