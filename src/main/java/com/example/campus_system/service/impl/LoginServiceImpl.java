package com.example.campus_system.service.impl;

import com.example.campus_system.entity.UserLogin;
import com.example.campus_system.mapper.UserLoginMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Service("userDetailsService")
public class LoginServiceImpl implements UserDetailsService {

    @Autowired
    UserLoginMapper userLoginMapper;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    HttpSession httpSession;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserLogin userLogin = userLoginMapper.selectUserLogin(username);
        String role = userLoginMapper.findrole(username);
        if(userLogin==null){
            throw new UsernameNotFoundException("not found");
        }
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_"+role));
        User user = new User(userLogin.getUser_id(),userLogin.getUser_pwd(),authorities);
        return user;

    }
}
