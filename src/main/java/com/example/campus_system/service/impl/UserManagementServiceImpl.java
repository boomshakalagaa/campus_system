package com.example.campus_system.service.impl;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.StudentInfo;
import com.example.campus_system.entity.TeacherInfo;
import com.example.campus_system.entity.UserLogin;
import com.example.campus_system.mapper.UserMapper;
import com.example.campus_system.service.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class UserManagementServiceImpl implements UserManagementService {

    @Autowired
    UserMapper userMapper;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Resource
    DataSourceTransactionManager dataSourceTransactionManager;

    @Resource
    TransactionDefinition transactionDefinition;

    /**
     * 查询所有学生
     * @return
     */
    @Override
    public BaseResponse<List<StudentInfo>> selectStudentAll() {
        BaseResponse<List<StudentInfo>> baseResponse = new BaseResponse<>();
        List<StudentInfo> studentInfos = userMapper.selectStudentAll();
        baseResponse.setData(studentInfos);
        return baseResponse;
    }


    /**
     * 搜索学生 id，专业，班级
     */
    @Override
    public BaseResponse<List<StudentInfo>> searchStudent(String stu_id,String stu_faculty) {
        BaseResponse<List<StudentInfo>> baseResponse = new BaseResponse<>();
        List<StudentInfo> studentInfos = userMapper.searchStudent(stu_id,stu_faculty);
        if(null == studentInfos || studentInfos.size() ==0 ){
            baseResponse.setCode(500);
            baseResponse.setMsg("没有该学生");

        }else {
            baseResponse.setCode(200);
            baseResponse.setMsg("搜索成功");
            baseResponse.setData(studentInfos);
        }
        return baseResponse;
    }

    /**
     * 添加学生
     */
    @Override
    public BaseResponse addStudent(String stu_name, String stu_sex, String stu_age, String stu_address
            , String stu_birth, String stu_phone, String stu_email, String stu_faculty, String stu_major, String stu_class) {
        BaseResponse baseResponse = new BaseResponse();

        TransactionStatus transactionStatus = dataSourceTransactionManager.getTransaction(transactionDefinition);
        try {
            //插入account表，并获取自增id
            UserLogin userLogin = new UserLogin();
            userLogin.setUser_pwd(passwordEncoder.encode("888"));
            userMapper.addUser(userLogin);
            //添加权限
            userMapper.addUserRole(userLogin.getUser_id(),"3");

            //插入学生表
            userMapper.addStudent( userLogin.getUser_id(), stu_name, stu_sex, stu_age, stu_address
                    , stu_birth,stu_phone, stu_email, stu_faculty, stu_major, stu_class
                    , new SimpleDateFormat("yyyy-MM-dd").format(new Date(System.currentTimeMillis())));

            //添加报告表
            userMapper.addReport(userLogin.getUser_id(),stu_name,stu_faculty,stu_class);

            baseResponse.setCode(200);
            baseResponse.setMsg("添加成功");
            dataSourceTransactionManager.commit(transactionStatus);

        }catch (Exception e){

            dataSourceTransactionManager.rollback(transactionStatus);
            baseResponse.setCode(500);
            baseResponse.setMsg("添加失败");
            return baseResponse;
        }

        return baseResponse;
    }

    /**
     * 修改学生信息
     */
    @Override
    public BaseResponse updateStudent(String stu_id, String stu_name, String stu_sex, String stu_age, String stu_address
            , String stu_birth, String stu_phone, String stu_email, String stu_faculty, String stu_major, String stu_class) {

        BaseResponse baseResponse = new BaseResponse();
        int rows=userMapper.updateStudent(stu_id, stu_name, stu_sex, stu_age, stu_address
                ,stu_birth, stu_phone, stu_email, stu_faculty, stu_major, stu_class);
        if (rows>0){
            baseResponse.setCode(200);
            baseResponse.setMsg("修改成功");
        }else {
            baseResponse.setCode(500);
            baseResponse.setMsg("修改失败");
        }
        return baseResponse;
    }

    /**
     * 删除学生信息
     */
    @Override
    public BaseResponse deleteStudent(String stu_id) {
        BaseResponse baseResponse = new BaseResponse();
        int rows=userMapper.deleteStudent(stu_id);
        if (rows>0){
            baseResponse.setCode(200);
            baseResponse.setMsg("删除成功");
        }else {
            baseResponse.setCode(500);
            baseResponse.setMsg("删除失败");
        }
        return baseResponse;
    }

    /**
     * 重置密码
     */
    @Override
    public BaseResponse resetPassword(String user_id) {
        BaseResponse baseResponse = new BaseResponse();
        int rows = userMapper.resetPassword(user_id,passwordEncoder.encode("888"));
        if (rows>0){
            baseResponse.setCode(200);
            baseResponse.setMsg("设置成功");
        }else {
            baseResponse.setCode(500);
            baseResponse.setMsg("设置失败");
        }
        return baseResponse;
    }

    /**
     * 查询所有老师
     * @return
     */
    @Override
    public BaseResponse<List<TeacherInfo>> selectTeacherAll() {
        BaseResponse<List<TeacherInfo>> baseResponse = new BaseResponse<>();
        List<TeacherInfo> teacherInfos = userMapper.selectTeacherAll();
        baseResponse.setData(teacherInfos);
        return baseResponse;
    }

    /**
     * 搜索老师 id，院系
     */
    @Override
    public BaseResponse<List<TeacherInfo>> searchTeacher(String teacher_id, String teacher_faculty) {
        BaseResponse<List<TeacherInfo>> baseResponse = new BaseResponse<>();
        List<TeacherInfo> teacherInfos = userMapper.searchTeacher(teacher_id,teacher_faculty);
        if(null == teacherInfos || teacherInfos.size() ==0 ){
            baseResponse.setCode(500);
            baseResponse.setMsg("没有该教师");

        }else {
            baseResponse.setCode(200);
            baseResponse.setMsg("搜索成功");
            baseResponse.setData(teacherInfos);
        }
        return baseResponse;
    }

    /**
     * 添加教师，管理员
     */
    @Override
    public BaseResponse addTeacher(String teacher_name, String teacher_sex, String teacher_age, String teacher_address
            , String teacher_birth, String teacher_phone, String teacher_email, String teacher_faculty,String role_id) {
        BaseResponse baseResponse = new BaseResponse();

        TransactionStatus transactionStatus = dataSourceTransactionManager.getTransaction(transactionDefinition);

        try {
            //插入account表，并获取自增id
            UserLogin userLogin = new UserLogin();
            userLogin.setUser_pwd(passwordEncoder.encode("888"));
            //添加user
            userMapper.addUser(userLogin);
            //添加权限
            userMapper.addUserRole(userLogin.getUser_id(),role_id);

            //插入学生表
            int rows = userMapper.addTeacher(userLogin.getUser_id(),teacher_name,teacher_sex,teacher_age,teacher_address,teacher_birth
                    ,teacher_phone,teacher_email,teacher_faculty);

            baseResponse.setCode(200);
            baseResponse.setMsg("添加成功");
            dataSourceTransactionManager.commit(transactionStatus);
        }catch ( Exception e){

            dataSourceTransactionManager.rollback(transactionStatus);
            baseResponse.setCode(500);
            baseResponse.setMsg("添加失败");
            return baseResponse;
        }

        return baseResponse;
    }

    /**
     * 修改教师信息
     */
    @Override
    public BaseResponse updateTeacher(String teacher_id, String teacher_name, String teacher_sex, String teacher_age
            , String teacher_address, String teacher_birth, String teacher_phone, String teacher_email, String teacher_faculty, String role_id) {
        BaseResponse baseResponse = new BaseResponse();

        TransactionStatus transactionStatus = dataSourceTransactionManager.getTransaction(transactionDefinition);

        try {

            userMapper.updateTeacher(teacher_id,teacher_name,teacher_sex,teacher_age,teacher_address,teacher_birth
                    ,teacher_phone,teacher_email,teacher_faculty);
            userMapper.updateUserRole(teacher_id,role_id);

            baseResponse.setCode(200);
            baseResponse.setMsg("修改成功");
            dataSourceTransactionManager.commit(transactionStatus);
        }catch ( Exception e){

            dataSourceTransactionManager.rollback(transactionStatus);
            baseResponse.setCode(500);
            baseResponse.setMsg("修改失败");
            return baseResponse;
        }

        return baseResponse;
    }

    /**
     * 删除教师信息
     */
    @Override
    public BaseResponse deleteTeacher(String teacher_id) {
        BaseResponse baseResponse = new BaseResponse();
        int rows = userMapper.deleteTeacher(teacher_id);
        if(rows>0){
            baseResponse.setCode(200);
            baseResponse.setMsg("删除成功");
        }else {
            baseResponse.setCode(500);
            baseResponse.setMsg("删除失败");
        }
        return baseResponse;
    }
}
