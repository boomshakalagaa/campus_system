package com.example.campus_system.service.impl;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.ClockNum;
import com.example.campus_system.entity.LeaveNum;
import com.example.campus_system.entity.NumInfo;
import com.example.campus_system.mapper.NumMapper;
import com.example.campus_system.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IndexServiceImpl implements IndexService {

    @Autowired
    NumMapper numMapper;

    /**
     * 查日志数，通知数，学生数，教师数，申请数，核算报告数
     * @return
     */
    @Override
    public BaseResponse selectNum() {
        BaseResponse baseResponse = new BaseResponse();
        NumInfo numInfo = new NumInfo();
        numInfo.setLog_num(numMapper.selectLogNum());
        numInfo.setWaiting_notice_num(numMapper.selectWaitingNoticeNum());
        numInfo.setWaiting_apply_num(numMapper.selectWatingApplyNum());
        numInfo.setReport_num(numMapper.selectReportNum());
        numInfo.setUser_num(numMapper.selectUserNum());
        numInfo.setErr_num(numMapper.selectErrNum());
        baseResponse.setData(numInfo);
        return baseResponse;
    }

    /**
     * 查离校
     * @return
     */
    @Override
    public List<LeaveNum> selectmap1() {
        return numMapper.selectLeaveSchoolNum();
    }

    /**
     * 查打卡数
     * @return
     */
    @Override
    public List<ClockNum> selectmap2() {
        return numMapper.selectClockNum();
    }
}
