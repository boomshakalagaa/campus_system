package com.example.campus_system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.Log;
import com.example.campus_system.mapper.LogMapper;
import com.example.campus_system.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Wrapper;
import java.util.List;

@Service
public class LogServiceImpl implements LogService {


    @Autowired
    LogMapper logMapper;

    /**
     * 记录日志
     */
    @Override
    public void addLog(Log log) {
        logMapper.insert(log);
    }

    /**
     * 查询日志
     */
    @Override
    public BaseResponse<List<Log>> selectLog(){
        QueryWrapper<Log> wrapper = new QueryWrapper<>();
        List<Log> logs = logMapper.selectList(wrapper);
        BaseResponse<List<Log>> baseResponse = new BaseResponse<>();
        baseResponse.setData(logs);
        return baseResponse;
    }

    /**
     * 搜索日志
     */
    @Override
    public BaseResponse<List<Log>> searchLog(String search) {
        BaseResponse<List<Log>> baseResponse = new BaseResponse<>();
        List<Log> logs = logMapper.searchLog(search);
        if(null == logs || logs.size() ==0 ){
            baseResponse.setCode(500);
            baseResponse.setMsg("没有此日志");

        }else {
            baseResponse.setCode(200);
            baseResponse.setMsg("搜索成功");
            baseResponse.setData(logs);
        }
        return baseResponse;
    }
}
