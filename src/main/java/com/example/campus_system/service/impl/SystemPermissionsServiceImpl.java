package com.example.campus_system.service.impl;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.SystemPermissions;
import com.example.campus_system.mapper.SystemPermissionMapper;
import com.example.campus_system.service.SystemPermissionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SystemPermissionsServiceImpl implements SystemPermissionsService {


    /**
     * 管理员
     * 查询系统权限
     * 权限英文名，权限中文名，权限描述
     */

    @Autowired
    SystemPermissionMapper systemPermissionMapper;

    @Override
    public BaseResponse<List<SystemPermissions>> selectSystemPermissions() {
        BaseResponse<List<SystemPermissions>> baseResponse = new BaseResponse<>();
        List<SystemPermissions> systemPermissions = systemPermissionMapper.selectSystemPermissions();
        baseResponse.setData(systemPermissions);
        return baseResponse;
    }

    /**
     * 管理员
     * 编辑系统权限
     * 权限英文名，权限中文名，权限描述
     */
    @Override
    public BaseResponse editSystemPermissions(SystemPermissions systemPermissions) {
        BaseResponse baseResponse = new BaseResponse();
        int rows= systemPermissionMapper.editSystemPermissions(systemPermissions.getRole_id(),systemPermissions.getRole_name(),systemPermissions.getRole_cname(),systemPermissions.getRole_describe());
        if(rows>0){
            baseResponse.setCode(200);
            baseResponse.setMsg("修改成功");
        }else {
            baseResponse.setCode(500);
            baseResponse.setMsg("修改失败");
        }
        return baseResponse;
    }
}
