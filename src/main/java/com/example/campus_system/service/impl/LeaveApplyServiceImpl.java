package com.example.campus_system.service.impl;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.LeaveApply;
import com.example.campus_system.entity.StudentInfo;
import com.example.campus_system.mapper.LeaveApplyMapper;
import com.example.campus_system.mapper.UserMapper;
import com.example.campus_system.service.LeaveApplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LeaveApplyServiceImpl implements LeaveApplyService {


    @Autowired
    LeaveApplyMapper leaveApplyMapper;

    @Autowired
    UserMapper userMapper;

    /**
     * 管理员
     * 离校申请查询
     * @return
     */
    @Override
    public BaseResponse<List<LeaveApply>> adminSelectLeaveApply() {
        BaseResponse<List<LeaveApply>> baseResponse = new BaseResponse<>();
        List<LeaveApply> leaveApplies = leaveApplyMapper.adminSelectLeaveApply();
        if(null == leaveApplies || leaveApplies.size() ==0 ){
            baseResponse.setCode(500);
            baseResponse.setMsg("暂无申请");

        }else {
            baseResponse.setCode(200);
            baseResponse.setMsg("查询成功");
            baseResponse.setData(leaveApplies);
        }
        return baseResponse;
    }


    /**
     * 管理员
     * 离校申请审核
     * @return
     */
    @Override
    public BaseResponse adminUpdateLeaveApply(String apply_id,String apply_state) {

        BaseResponse baseResponse = new BaseResponse();
        int rows = leaveApplyMapper.adminUpdateLeaveApply(apply_id,apply_state);
        if (rows > 0) {
            baseResponse.setCode(200);
            baseResponse.setMsg("审核成功");
        } else {
            baseResponse.setCode(500);
            baseResponse.setMsg("失败");
        }
        return baseResponse;
    }

    /**
     * 管理员
     * 搜索申请审核
     * @return
     */
    @Override
    public BaseResponse<List<LeaveApply>> adminSearchLeaveApply(String search,String search_faculty) {

        BaseResponse<List<LeaveApply>> baseResponse = new BaseResponse<>();
        List<LeaveApply> leaveApplies = leaveApplyMapper.adminSearchLeaveApply(search,search_faculty);
        if(null == leaveApplies || leaveApplies.size() ==0 ){
            baseResponse.setCode(500);
            baseResponse.setMsg("查无此申请");

        }else {
            baseResponse.setCode(200);
            baseResponse.setMsg("查询成功");
            baseResponse.setData(leaveApplies);
        }
        return baseResponse;
    }

    /**
     * 管理员
     * 删除离校申请
     * @return
     */
    @Override
    public BaseResponse adminDeleteLeaveApply(String apply_id) {
        BaseResponse baseResponse = new BaseResponse();
        int rows = leaveApplyMapper.adminDeleteLeaveApply(apply_id);
        if (rows > 0) {
            baseResponse.setCode(200);
            baseResponse.setMsg("删除成功");
        } else {
            baseResponse.setCode(500);
            baseResponse.setMsg("删除失败");
        }
        return baseResponse;
    }

    /**
     * 学生
     * 离校申请查询
     * @return
     */
    @Override
    public BaseResponse<List<LeaveApply>> studentSelectLeaveApply(String stu_id) {
        BaseResponse<List<LeaveApply>> baseResponse = new BaseResponse<>();
        List<LeaveApply> leaveApplies = leaveApplyMapper.studentSelectLeaveApply(stu_id);
        if(null == leaveApplies || leaveApplies.size() ==0 ){
            baseResponse.setCode(500);
            baseResponse.setMsg("暂无申请");

        }else {
            baseResponse.setCode(200);
            baseResponse.setMsg("查询成功");
            baseResponse.setData(leaveApplies);
        }
        return baseResponse;
    }

    /**
     * 学生
     * 离校申请查询
     * @return
     */
    @Override
    public BaseResponse studentAddLeaveApply(String stu_id,String reason,String travel,String leave_time,String back_time) {
        BaseResponse baseResponse = new BaseResponse();
        StudentInfo studentInfo=userMapper.selectStudent(stu_id);
        int rows = leaveApplyMapper.studentAddLeaveApply(stu_id,studentInfo.getStu_name(),studentInfo.getStu_faculty(),studentInfo.getStu_class(),reason,travel,leave_time,back_time);
        if (rows > 0) {
            baseResponse.setCode(200);
            baseResponse.setMsg("添加成功");
        } else {
            baseResponse.setCode(500);
            baseResponse.setMsg("添加失败");
        }
        return baseResponse;
    }

    /**
     * 老师
     * 离校申请查询
     * @return
     */
    @Override
    public BaseResponse<List<LeaveApply>> teacherSelectLeaveApply(String teacher_id) {
        BaseResponse<List<LeaveApply>> baseResponse = new BaseResponse<>();
        String stu_class = userMapper.selectClass(teacher_id);
        List<LeaveApply> leaveApplies = leaveApplyMapper.teacherSelectLeaveApply(stu_class);
        if(null == leaveApplies || leaveApplies.size() ==0 ){
            baseResponse.setCode(500);
            baseResponse.setMsg("暂无申请");

        }else {
            baseResponse.setCode(200);
            baseResponse.setMsg("查询成功");
            baseResponse.setData(leaveApplies);
        }
        return baseResponse;
    }

    /**
     * 老师
     * 搜索离校申请
     * @return
     */
    @Override
    public BaseResponse<List<LeaveApply>> teacherSearchLeaveApply(String search, String teacher_id) {
        BaseResponse<List<LeaveApply>> baseResponse = new BaseResponse<>();
        String stu_class = userMapper.selectClass(teacher_id);
        List<LeaveApply> leaveApplies = leaveApplyMapper.teacherSearchLeaveApply(search,stu_class);
        if(null == leaveApplies || leaveApplies.size() ==0 ){
            baseResponse.setCode(500);
            baseResponse.setMsg("查无此申请");

        }else {
            baseResponse.setCode(200);
            baseResponse.setMsg("查询成功");
            baseResponse.setData(leaveApplies);
        }
        return baseResponse;
    }
}
