package com.example.campus_system.service.impl;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.StudentInfo;
import com.example.campus_system.entity.TeacherInfo;
import com.example.campus_system.mapper.PersonalInfoMapper;
import com.example.campus_system.mapper.SystemPermissionMapper;
import com.example.campus_system.service.PersonalInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonalInfoServiceImpl implements PersonalInfoService {

    @Autowired
    PersonalInfoMapper personalInfoMapper;

    /**
     * 查询教师个人信息
     */
    @Override
    public BaseResponse<TeacherInfo> selectTeacherInfo(String user_id) {
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setData(personalInfoMapper.selectTeacherInfo(user_id));
        return baseResponse;
    }


    /**
     *老师或管理员
     *修改个人信息
     */
    @Override
    public BaseResponse updateTechaerInfo(TeacherInfo teacherInfo) {
        BaseResponse baseResponse = new BaseResponse();
        int rows;
        if (teacherInfo.getTeacher_photo()==null||teacherInfo.getTeacher_photo().isEmpty())
        {

             rows = personalInfoMapper.updateTechaerInfo1(teacherInfo.getTeacher_id(),teacherInfo.getTeacher_name(),teacherInfo.getTeacher_age()
                    ,teacherInfo.getTeacher_address(),teacherInfo.getTeacher_birth(),teacherInfo.getTeacher_sex()
                    ,teacherInfo.getTeacher_phone(),teacherInfo.getTeacher_email(),teacherInfo.getTeacher_faculty());
        }else {
             rows = personalInfoMapper.updateTechaerInfo(teacherInfo.getTeacher_id(),teacherInfo.getTeacher_name(),teacherInfo.getTeacher_age()
                    ,teacherInfo.getTeacher_photo(),teacherInfo.getTeacher_address(),teacherInfo.getTeacher_birth(),teacherInfo.getTeacher_sex()
                    ,teacherInfo.getTeacher_phone(),teacherInfo.getTeacher_email(),teacherInfo.getTeacher_faculty());
        }

        if(rows>0){
            baseResponse.setMsg("修改成功");
            baseResponse.setCode(200);
        }else {
            baseResponse.setMsg("修改失败");
            baseResponse.setCode(500);
        }
        return baseResponse;
    }

    /**
     * 查询学生个人信息
     */
    @Override
    public BaseResponse<StudentInfo> selectStudentInfo(String stu_id) {
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setData(personalInfoMapper.selectStudentInfo(stu_id));
        return baseResponse;
    }

    /**
     *学生
     *修改个人信息
     */
    @Override
    public BaseResponse updateStudentInfo(StudentInfo studentInfo) {
        BaseResponse baseResponse = new BaseResponse();
        int rows;
        if (studentInfo.getStu_photo()==null||studentInfo.getStu_photo().isEmpty())
        {
            //不修改
            rows = personalInfoMapper.updateStudentInfo1(studentInfo.getStu_id(),studentInfo.getStu_name(),studentInfo.getStu_age()
                    ,studentInfo.getStu_address(),studentInfo.getStu_birth(),studentInfo.getStu_sex()
                    ,studentInfo.getStu_phone(),studentInfo.getStu_email(),studentInfo.getStu_faculty(),studentInfo.getStu_class());
        }else {

            //修改
            rows = personalInfoMapper.updateStudentInfo(studentInfo.getStu_id(),studentInfo.getStu_name(),studentInfo.getStu_age()
                    ,studentInfo.getStu_photo(),studentInfo.getStu_address(),studentInfo.getStu_birth(),studentInfo.getStu_sex()
                    ,studentInfo.getStu_phone(),studentInfo.getStu_email(),studentInfo.getStu_faculty(),studentInfo.getStu_class());
        }

        if(rows>0){
            baseResponse.setMsg("修改成功");
            baseResponse.setCode(200);
        }else {
            baseResponse.setMsg("修改失败");
            baseResponse.setCode(500);
        }
        return baseResponse;
    }
}
