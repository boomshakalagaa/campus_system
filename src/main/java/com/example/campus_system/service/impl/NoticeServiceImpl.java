package com.example.campus_system.service.impl;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.Notice;
import com.example.campus_system.entity.StudentInfo;
import com.example.campus_system.entity.TeacherInfo;
import com.example.campus_system.mapper.NoticeMapper;
import com.example.campus_system.mapper.PersonalInfoMapper;
import com.example.campus_system.mapper.UserMapper;
import com.example.campus_system.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
public class NoticeServiceImpl implements NoticeService {


    @Autowired
    PersonalInfoMapper personalInfoMapper;

    @Autowired
    NoticeMapper noticeMapper;

    @Autowired
    UserMapper userMapper;

    /**
     * 查询总通知
     * @return
     */
    @Override
    public BaseResponse<List<Notice>> selectNotice() {
        BaseResponse<List<Notice>> baseResponse = new BaseResponse<>();
        List<Notice> notices = noticeMapper.selectNotice();
        baseResponse.setData(notices);
        return baseResponse;
    }

    /**
     * 查询学校通知
     * @return
     */
    @Override
    public BaseResponse<List<Notice>> selectIndexNotice() {
        BaseResponse<List<Notice>> baseResponse = new BaseResponse<>();
        List<Notice> notices = noticeMapper.selectIndexNotice();
        baseResponse.setData(notices);
        return baseResponse;
    }

    /**
     * 查询教工通知
     * @return
     */
    @Override
    public BaseResponse<List<Notice>> selectTeacherNotice(String teacher_id) {
        BaseResponse<List<Notice>> baseResponse = new BaseResponse<>();
        List<Notice> notices = noticeMapper.selectTeacherNotice();
        baseResponse.setData(notices);
        return baseResponse;
    }

    /**
     * 查询班级通知
     * @return
     */
    @Override
    public BaseResponse<List<Notice>> selectClassNotice(String stu_id) {
        BaseResponse<List<Notice>> baseResponse = new BaseResponse<>();
        StudentInfo studentInfo=userMapper.selectStudent(stu_id);
        List<Notice> notices = noticeMapper.selectClassNotice(studentInfo.getStu_class());
        baseResponse.setData(notices);
        return baseResponse;
    }

    /**
     * 添加通知
     * @return
     */
    @Override
    public BaseResponse insertNotice(Notice notice) {
        BaseResponse baseResponse = new BaseResponse();
        int rows = noticeMapper.insertNotice(notice.getNotice_title(),notice.getNotice_content(),notice.getNotice_user(),notice.getNotice_type(),notice.getNotice_time(),notice.getNotice_class());
        if(rows>0){
            baseResponse.setCode(200);
            baseResponse.setMsg("添加成功");
        }else {
            baseResponse.setCode(500);
            baseResponse.setMsg("添加失败");
        }
        return baseResponse;
    }
    /**
     * 通知审核
     * @return
     */
    @Override
    public BaseResponse updateNotice(String notice_id, String notice_state) {
        BaseResponse baseResponse = new BaseResponse();
        int rows = noticeMapper.updateNotice(notice_id, notice_state);
        if (rows > 0) {
            baseResponse.setCode(200);
            baseResponse.setMsg("审核成功");
        } else {
            baseResponse.setCode(500);
            baseResponse.setMsg("失败");
        }
        return baseResponse;
    }

    /**
     * 删除通知
     * @return
     */
    @Override
    public BaseResponse deleteNotice(String notice_id) {
        BaseResponse baseResponse = new BaseResponse();
        int rows = noticeMapper.deleteNotice(notice_id);
        if (rows > 0) {
            baseResponse.setCode(200);
            baseResponse.setMsg("删除成功");
        } else {
            baseResponse.setCode(500);
            baseResponse.setMsg("删除失败");
        }
        return baseResponse;
    }

    /**
     * 搜索通知
     * @return
     */
    @Override
    public BaseResponse<List<Notice>> searchNotice(String search,String searchtype) {
        BaseResponse<List<Notice>> baseResponse = new BaseResponse<>();
        List<Notice> notices = noticeMapper.searchNotice(search,searchtype);
        if(null == notices || notices.size() ==0 ){
            baseResponse.setCode(500);
            baseResponse.setMsg("没有此通知");

        }else {
            baseResponse.setCode(200);
            baseResponse.setMsg("搜索成功");
            baseResponse.setData(notices);
        }
        return baseResponse;
    }
    /**
     * 学生
     * 搜索该班级通知
     * @return
     */
    @Override
    public BaseResponse<List<Notice>> searchNoticeClass(String search,String stu_id) {
        BaseResponse<List<Notice>> baseResponse = new BaseResponse<>();
        StudentInfo studentInfo=userMapper.selectStudent(stu_id);
        List<Notice> notices =noticeMapper.searchNoticeClass(search,studentInfo.getStu_class());
        if(null == notices || notices.size() ==0 ){
            baseResponse.setCode(500);
            baseResponse.setMsg("没有此通知");

        }else {
            baseResponse.setCode(200);
            baseResponse.setMsg("搜索成功");
            baseResponse.setData(notices);
        }
        return baseResponse;
    }

    /**
     * 学生
     * 搜索通知公告
     * @return
     */
    @Override
    public BaseResponse<List<Notice>> stuSearchNotice(String search, String searchtype) {
        BaseResponse<List<Notice>> baseResponse = new BaseResponse<>();
        List<Notice> notices =noticeMapper.stuSearchNotice(search,searchtype);
        if(null == notices || notices.size() ==0 ){
            baseResponse.setCode(500);
            baseResponse.setMsg("没有此通知");

        }else {
            baseResponse.setCode(200);
            baseResponse.setMsg("搜索成功");
            baseResponse.setData(notices);
        }
        return baseResponse;
    }

    /**
     * 老师
     * 搜索该院系教工通知
     * @return
     */
    @Override
    public BaseResponse<List<Notice>> searchNoticeTeacher(String search) {
        BaseResponse<List<Notice>> baseResponse = new BaseResponse<>();
        List<Notice> notices =noticeMapper.searchNoticeTeacher(search);
        if(null == notices || notices.size() ==0 ){
            baseResponse.setCode(500);
            baseResponse.setMsg("没有此通知");

        }else {
            baseResponse.setCode(200);
            baseResponse.setMsg("搜索成功");
            baseResponse.setData(notices);
        }
        return baseResponse;
    }

    /**
     * 老师
     * 查询自己班级通知
     * @return
     */
    @Override
    public BaseResponse<List<Notice>> TeacherselectClassNotice(String teacher_id) {
        BaseResponse<List<Notice>> baseResponse = new BaseResponse<>();
        String notice_class = userMapper.selectClass(teacher_id);
        List<Notice> notices =noticeMapper.TeacherselectClassNotice(notice_class);
        if(null == notices || notices.size() ==0 ){
            baseResponse.setCode(500);
            baseResponse.setMsg("暂无通知");

        }else {
            baseResponse.setCode(200);
            baseResponse.setMsg("查询成功");
            baseResponse.setData(notices);
        }
        return baseResponse;
    }

    /**
     * 老师
     * 搜索该班级通知
     * @return
     */
    @Override
    public BaseResponse<List<Notice>> teacherSearchClassNotice(String search, String teacher_id) {
        BaseResponse<List<Notice>> baseResponse = new BaseResponse<>();
        String notice_class = userMapper.selectClass(teacher_id);
        List<Notice> notices =noticeMapper.teacherSearchClassNotice(search,notice_class);
        if(null == notices || notices.size() ==0 ){
            baseResponse.setCode(500);
            baseResponse.setMsg("没有此通知");

        }else {
            baseResponse.setCode(200);
            baseResponse.setMsg("搜索成功");
            baseResponse.setData(notices);
        }
        return baseResponse;
    }
}
