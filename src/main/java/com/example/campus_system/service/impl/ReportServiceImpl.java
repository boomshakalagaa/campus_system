package com.example.campus_system.service.impl;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.Notice;
import com.example.campus_system.entity.Report;
import com.example.campus_system.entity.StudentInfo;
import com.example.campus_system.mapper.ReportMapper;
import com.example.campus_system.mapper.UserMapper;
import com.example.campus_system.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class ReportServiceImpl implements ReportService {


    @Autowired
    ReportMapper reportMapper;

    @Autowired
    UserMapper userMapper;

    /**
     * 查询所有核算报告
     * @return
     */
    @Override
    public BaseResponse<List<Report>> selectReportAll() {
        BaseResponse<List<Report>> baseResponse= new BaseResponse<>();
        List<Report> reports = reportMapper.selectReportAll();
        baseResponse.setData(reports);
        baseResponse.setCode(200);
        baseResponse.setMsg("查询成功");
        return baseResponse;
    }

    /**
     * 搜索报告
     * @return
     */
    @Override
    public BaseResponse<List<Report>> searchReport(String stu_id, String stu_name, String stu_faculty) {
        BaseResponse<List<Report>> baseResponse = new BaseResponse<>();
        List<Report> reports = reportMapper.searchReport(stu_id,stu_name,stu_faculty);
        if(null == reports || reports.size() ==0 ){
            baseResponse.setCode(500);
            baseResponse.setMsg("没有此检测报告");

        }else {
            baseResponse.setCode(200);
            baseResponse.setMsg("搜索成功");
            baseResponse.setData(reports);
        }
        return baseResponse;
    }


    /**
     *学生
     *查询核酸报告
     */
    @Override
    public BaseResponse<Report> StudentselectReport(String stu_id) {
        BaseResponse<Report> baseResponse = new BaseResponse<>();
        Report test_report = reportMapper.StudentselectReport(stu_id);

        baseResponse.setData(test_report);
        return baseResponse;
    }


    /**
     *学生
     *上传核酸报告
     */
    @Override
    public BaseResponse uploadReport(Report report) {
        BaseResponse baseResponse = new BaseResponse();
        StudentInfo studentInfo = userMapper.selectStudent(report.getStu_id());
        int rows=reportMapper.uploadReport(studentInfo.getStu_id(),studentInfo.getStu_name(),studentInfo.getStu_faculty(),studentInfo.getStu_class()
                ,report.getTest_report(),new SimpleDateFormat("yyyy-MM-dd").format(new Date(System.currentTimeMillis())));
        if(rows>0){
            baseResponse.setCode(200);
            baseResponse.setMsg("上传成功");
        }else {
            baseResponse.setCode(500);
            baseResponse.setMsg("上传失败");
        }
        return baseResponse;
    }

    /**
     * 老师
     * 查询班级报告
     * @return
     */
    @Override
    public BaseResponse<List<Report>> teacherSelectReport(String teacher_id) {
        BaseResponse<List<Report>> baseResponse= new BaseResponse<>();
        String stu_class = userMapper.selectClass(teacher_id);
        List<Report> reports = reportMapper.teacherSelectReport(stu_class);
        baseResponse.setData(reports);
        baseResponse.setCode(200);
        baseResponse.setMsg("查询成功");
        return baseResponse;
    }

    /**
     * 老师
     * 搜索报告
     * @return
     */
    @Override
    public BaseResponse<List<Report>> teacherSearchReport(String stu_id, String teacher_id) {
        BaseResponse<List<Report>> baseResponse= new BaseResponse<>();
        String stu_class = userMapper.selectClass(teacher_id);
        List<Report> reports = reportMapper.teacherSearchReport(stu_id,stu_class);
        baseResponse.setData(reports);
        baseResponse.setCode(200);
        baseResponse.setMsg("查询成功");
        return baseResponse;
    }
}
