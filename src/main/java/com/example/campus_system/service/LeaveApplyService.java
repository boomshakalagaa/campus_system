package com.example.campus_system.service;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.LeaveApply;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Service
public interface LeaveApplyService {

    /**
     * 管理员
     * 离校申请查询
     * @return
     */
    BaseResponse<List<LeaveApply>> adminSelectLeaveApply();

    /**
     * 管理员
     * 离校申请审核
     * @return
     */
    BaseResponse adminUpdateLeaveApply(String apply_id,String apply_state);

    /**
     * 管理员
     * 搜索离校申请
     * @return
     */

    BaseResponse<List<LeaveApply>> adminSearchLeaveApply(String search ,String search_faculty);


    /**
     * 管理员
     * 删除离校申请
     * @return
     */
    BaseResponse adminDeleteLeaveApply(String apply_id);

    /**
     * 学生
     * 离校申请查询
     * @return
     */
    BaseResponse<List<LeaveApply>> studentSelectLeaveApply(String stu_id);

    /**
     * 学生
     * 离校申请查询
     * @return
     */
    BaseResponse studentAddLeaveApply(String stu_id,String reason,String travel,String leave_time,String back_time);

    /**
     * 老师
     * 离校申请查询
     * @return
     */
    BaseResponse<List<LeaveApply>> teacherSelectLeaveApply(String teacher_id);


    /**
     * 老师
     * 搜索离校申请
     * @return
     */
    BaseResponse<List<LeaveApply>> teacherSearchLeaveApply(String search,String teacher_id);
}
