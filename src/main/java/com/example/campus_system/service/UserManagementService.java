package com.example.campus_system.service;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.StudentInfo;
import com.example.campus_system.entity.TeacherInfo;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Service
public interface UserManagementService {

    /**
     * 查询所有学生
     *
     * @return
     */
    BaseResponse<List<StudentInfo>> selectStudentAll();

    /**
     * 搜索学生 id，专业
     */
    BaseResponse<List<StudentInfo>> searchStudent(String stu_id, String stu_faculty);

    /**
     * 添加学生
     */
    BaseResponse addStudent(String stu_name, String stu_sex, String stu_age
            , String stu_address, String stu_birth, String stu_phone, String stu_email, String stu_faculty
            , String stu_major, String stu_class);


    /**
     * 修改学生信息
     */
    BaseResponse updateStudent(String stu_id, String stu_name, String stu_sex, String stu_age
            , String stu_address, String stu_birth, String stu_phone, String stu_email, String stu_faculty
            , String stu_major, String stu_class);


    /**
     * 删除学生信息
     */
    BaseResponse deleteStudent(String stu_id);

    /**
     * 重置密码
     */
    BaseResponse resetPassword(String user_id);

    /**
     * 查询所有老师
     * @return
     */
    BaseResponse<List<TeacherInfo>> selectTeacherAll();

    /**
     * 搜索老师 id，院系
     */
    BaseResponse<List<TeacherInfo>> searchTeacher(String teacher_id,String teacher_faculty);

    /**
     * 添加教师，管理员
     */
    BaseResponse addTeacher(String teacher_name,String teacher_sex,String teacher_age
            ,String teacher_address,String teacher_birth,String teacher_phone,String teacher_email,String teacher_faculty,String role_id);

    /**
     * 修改教师信息
     */
    BaseResponse updateTeacher(String teacher_id,String teacher_name,String teacher_sex,String teacher_age
            ,String teacher_address,String teacher_birth,String teacher_phone,String teacher_email,String teacher_faculty,String role_id);

    /**
     * 删除教师信息
     */
    BaseResponse deleteTeacher(String teacher_id);
}
