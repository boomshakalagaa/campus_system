package com.example.campus_system.entity;

public class Report {

    private String stu_id;
    private String stu_name;
    private String stu_faculty;
    private String stu_class;
    private String test_report;
    private String report_time;

    @Override
    public String toString() {
        return "Report{" +
                ", stu_id='" + stu_id + '\'' +
                ", stu_name='" + stu_name + '\'' +
                ", stu_faculty='" + stu_faculty + '\'' +
                ", stu_class='" + stu_class + '\'' +
                ", test_report='" + test_report + '\'' +
                ", report_time='" + report_time + '\'' +
                '}';
    }

    public String getStu_id() {
        return stu_id;
    }

    public void setStu_id(String stu_id) {
        this.stu_id = stu_id;
    }

    public String getStu_name() {
        return stu_name;
    }

    public void setStu_name(String stu_name) {
        this.stu_name = stu_name;
    }

    public String getStu_faculty() {
        return stu_faculty;
    }

    public void setStu_faculty(String stu_faculty) {
        this.stu_faculty = stu_faculty;
    }

    public String getStu_class() {
        return stu_class;
    }

    public void setStu_class(String stu_class) {
        this.stu_class = stu_class;
    }

    public String getTest_report() {
        return test_report;
    }

    public void setTest_report(String test_report) {
        this.test_report = test_report;
    }

    public String getReport_time() {
        return report_time;
    }

    public void setReport_time(String report_time) {
        this.report_time = report_time;
    }
}
