package com.example.campus_system.entity;

public class SystemPermissions {

    private String role_id;
    private String role_name;
    private String role_cname;
    private String role_describe;

    public String getRole_id() {
        return role_id;
    }

    public void setRole_id(String role_id) {
        this.role_id = role_id;
    }

    public String getRole_name() {
        return role_name;
    }

    public void setRole_name(String role_name) {
        this.role_name = role_name;
    }

    public String getRole_cname() {
        return role_cname;
    }

    public void setRole_cname(String role_cname) {
        this.role_cname = role_cname;
    }

    public String getRole_describe() {
        return role_describe;
    }

    public void setRole_describe(String role_describe) {
        this.role_describe = role_describe;
    }

    @Override
    public String toString() {
        return "SystemPermissions{" +
                "role_id='" + role_id + '\'' +
                ", role_name='" + role_name + '\'' +
                ", role_cname='" + role_cname + '\'' +
                ", role_describe='" + role_describe + '\'' +
                '}';
    }
}
