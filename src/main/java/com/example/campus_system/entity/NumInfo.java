package com.example.campus_system.entity;

public class NumInfo {
    private String log_num;
    private String err_num;
    private String user_num;
    private String waiting_notice_num;
    private String waiting_apply_num;
    private String report_num;

    public String getLog_num() {
        return log_num;
    }

    public void setLog_num(String log_num) {
        this.log_num = log_num;
    }

    public String getErr_num() {
        return err_num;
    }

    public void setErr_num(String err_num) {
        this.err_num = err_num;
    }

    public String getUser_num() {
        return user_num;
    }

    public void setUser_num(String user_num) {
        this.user_num = user_num;
    }

    public String getWaiting_notice_num() {
        return waiting_notice_num;
    }

    public void setWaiting_notice_num(String waiting_notice_num) {
        this.waiting_notice_num = waiting_notice_num;
    }

    public String getWaiting_apply_num() {
        return waiting_apply_num;
    }

    public void setWaiting_apply_num(String waiting_apply_num) {
        this.waiting_apply_num = waiting_apply_num;
    }

    public String getReport_num() {
        return report_num;
    }

    public void setReport_num(String report_num) {
        this.report_num = report_num;
    }

    @Override
    public String toString() {
        return "NumInfo{" +
                "log_num='" + log_num + '\'' +
                ", err_num='" + err_num + '\'' +
                ", user_num='" + user_num + '\'' +
                ", waiting_notice_num='" + waiting_notice_num + '\'' +
                ", waiting_apply_num='" + waiting_apply_num + '\'' +
                ", report_num='" + report_num + '\'' +
                '}';
    }
}
