package com.example.campus_system.entity;

public class C_data {

    private String c_name;
    private String p_name;
    private String c_total_nowConfirm;
    private String c_total_wzz;
    private String c_total_confirm;
    private String c_total_dead;
    private String c_total_heal;
    private String c_today_confirm;

    @Override
    public String toString() {
        return "C_data{" +
                "c_name='" + c_name + '\'' +
                ", p_name='" + p_name + '\'' +
                ", c_total_nowConfirm='" + c_total_nowConfirm + '\'' +
                ", c_total_wzz='" + c_total_wzz + '\'' +
                ", c_total_confirm='" + c_total_confirm + '\'' +
                ", c_total_dead='" + c_total_dead + '\'' +
                ", c_total_heal='" + c_total_heal + '\'' +
                ", c_today_confirm='" + c_today_confirm + '\'' +
                '}';
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public String getC_total_nowConfirm() {
        return c_total_nowConfirm;
    }

    public void setC_total_nowConfirm(String c_total_nowConfirm) {
        this.c_total_nowConfirm = c_total_nowConfirm;
    }

    public String getC_total_wzz() {
        return c_total_wzz;
    }

    public void setC_total_wzz(String c_total_wzz) {
        this.c_total_wzz = c_total_wzz;
    }

    public String getC_total_confirm() {
        return c_total_confirm;
    }

    public void setC_total_confirm(String c_total_confirm) {
        this.c_total_confirm = c_total_confirm;
    }

    public String getC_total_dead() {
        return c_total_dead;
    }

    public void setC_total_dead(String c_total_dead) {
        this.c_total_dead = c_total_dead;
    }

    public String getC_total_heal() {
        return c_total_heal;
    }

    public void setC_total_heal(String c_total_heal) {
        this.c_total_heal = c_total_heal;
    }

    public String getC_today_confirm() {
        return c_today_confirm;
    }

    public void setC_today_confirm(String c_today_confirm) {
        this.c_today_confirm = c_today_confirm;
    }
}
