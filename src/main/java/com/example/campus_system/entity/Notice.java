package com.example.campus_system.entity;

public class Notice {

    private String notice_id;
    private String notice_title;
    private String notice_content;
    private String notice_user;
    private String notice_type;
    private String notice_state;
    private String notice_time;
    private String notice_class;

    @Override
    public String toString() {
        return "Notice{" +
                "notice_id='" + notice_id + '\'' +
                ", notice_title='" + notice_title + '\'' +
                ", notice_content='" + notice_content + '\'' +
                ", notice_user='" + notice_user + '\'' +
                ", notice_type='" + notice_type + '\'' +
                ", notice_state='" + notice_state + '\'' +
                ", notice_time='" + notice_time + '\'' +
                ", notice_class='" + notice_class + '\'' +
                '}';
    }

    public String getNotice_id() {
        return notice_id;
    }

    public void setNotice_id(String notice_id) {
        this.notice_id = notice_id;
    }

    public String getNotice_title() {
        return notice_title;
    }

    public void setNotice_title(String notice_title) {
        this.notice_title = notice_title;
    }

    public String getNotice_content() {
        return notice_content;
    }

    public void setNotice_content(String notice_content) {
        this.notice_content = notice_content;
    }

    public String getNotice_user() {
        return notice_user;
    }

    public void setNotice_user(String notice_user) {
        this.notice_user = notice_user;
    }

    public String getNotice_type() {
        return notice_type;
    }

    public void setNotice_type(String notice_type) {
        this.notice_type = notice_type;
    }

    public String getNotice_state() {
        return notice_state;
    }

    public void setNotice_state(String notice_state) {
        this.notice_state = notice_state;
    }

    public String getNotice_time() {
        return notice_time;
    }

    public void setNotice_time(String notice_time) {
        this.notice_time = notice_time;
    }

    public String getNotice_class() {
        return notice_class;
    }

    public void setNotice_class(String notice_class) {
        this.notice_class = notice_class;
    }
}
