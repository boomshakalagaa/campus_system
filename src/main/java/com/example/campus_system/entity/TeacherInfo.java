package com.example.campus_system.entity;

public class TeacherInfo {
    private String teacher_id;
    private String teacher_name;
    private String teacher_age;
    private String teacher_photo;
    private String teacher_address;
    private String teacher_birth;
    private String teacher_sex;
    private String teacher_phone;
    private String teacher_email;
    private String teacher_faculty;
    private String role_id;

    @Override
    public String toString() {
        return "TeacherInfo{" +
                "teacher_id='" + teacher_id + '\'' +
                ", teacher_name='" + teacher_name + '\'' +
                ", teacher_age='" + teacher_age + '\'' +
                ", teacher_photo='" + teacher_photo + '\'' +
                ", teacher_address='" + teacher_address + '\'' +
                ", teacher_birth='" + teacher_birth + '\'' +
                ", teacher_sex='" + teacher_sex + '\'' +
                ", teacher_phone='" + teacher_phone + '\'' +
                ", teacher_email='" + teacher_email + '\'' +
                ", teacher_faculty='" + teacher_faculty + '\'' +
                ", role_id='" + role_id + '\'' +
                '}';
    }

    public String getTeacher_id() {
        return teacher_id;
    }

    public void setTeacher_id(String teacher_id) {
        this.teacher_id = teacher_id;
    }

    public String getTeacher_name() {
        return teacher_name;
    }

    public void setTeacher_name(String teacher_name) {
        this.teacher_name = teacher_name;
    }

    public String getTeacher_age() {
        return teacher_age;
    }

    public void setTeacher_age(String teacher_age) {
        this.teacher_age = teacher_age;
    }

    public String getTeacher_photo() {
        return teacher_photo;
    }

    public void setTeacher_photo(String teacher_photo) {
        this.teacher_photo = teacher_photo;
    }

    public String getTeacher_address() {
        return teacher_address;
    }

    public void setTeacher_address(String teacher_address) {
        this.teacher_address = teacher_address;
    }

    public String getTeacher_birth() {
        return teacher_birth;
    }

    public void setTeacher_birth(String teacher_birth) {
        this.teacher_birth = teacher_birth;
    }

    public String getTeacher_sex() {
        return teacher_sex;
    }

    public void setTeacher_sex(String teacher_sex) {
        this.teacher_sex = teacher_sex;
    }

    public String getTeacher_phone() {
        return teacher_phone;
    }

    public void setTeacher_phone(String teacher_phone) {
        this.teacher_phone = teacher_phone;
    }

    public String getTeacher_email() {
        return teacher_email;
    }

    public void setTeacher_email(String teacher_email) {
        this.teacher_email = teacher_email;
    }

    public String getTeacher_faculty() {
        return teacher_faculty;
    }

    public void setTeacher_faculty(String teacher_faculty) {
        this.teacher_faculty = teacher_faculty;
    }

    public String getRole_id() {
        return role_id;
    }

    public void setRole_id(String role_id) {
        this.role_id = role_id;
    }
}
