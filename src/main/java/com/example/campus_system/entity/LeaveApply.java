package com.example.campus_system.entity;

import java.util.Date;

public class LeaveApply {

    private String apply_id;
    private String stu_id;
    private String stu_name;
    private String stu_faculty;
    private String stu_class;
    private String reason;
    private String travel;
    private Date leave_time;
    private Date back_time;
    private String apply_state;

    @Override
    public String toString() {
        return "LeaveApply{" +
                "apply_id='" + apply_id + '\'' +
                ", stu_id='" + stu_id + '\'' +
                ", stu_name='" + stu_name + '\'' +
                ", stu_faculty='" + stu_faculty + '\'' +
                ", stu_class='" + stu_class + '\'' +
                ", reason='" + reason + '\'' +
                ", travel='" + travel + '\'' +
                ", leave_time=" + leave_time +
                ", back_time=" + back_time +
                ", apply_state='" + apply_state + '\'' +
                '}';
    }

    public String getApply_id() {
        return apply_id;
    }

    public void setApply_id(String apply_id) {
        this.apply_id = apply_id;
    }

    public String getStu_id() {
        return stu_id;
    }

    public void setStu_id(String stu_id) {
        this.stu_id = stu_id;
    }

    public String getStu_name() {
        return stu_name;
    }

    public void setStu_name(String stu_name) {
        this.stu_name = stu_name;
    }

    public String getStu_faculty() {
        return stu_faculty;
    }

    public void setStu_faculty(String stu_faculty) {
        this.stu_faculty = stu_faculty;
    }

    public String getStu_class() {
        return stu_class;
    }

    public void setStu_class(String stu_class) {
        this.stu_class = stu_class;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getTravel() {
        return travel;
    }

    public void setTravel(String travel) {
        this.travel = travel;
    }

    public Date getLeave_time() {
        return leave_time;
    }

    public void setLeave_time(Date leave_time) {
        this.leave_time = leave_time;
    }

    public Date getBack_time() {
        return back_time;
    }

    public void setBack_time(Date back_time) {
        this.back_time = back_time;
    }

    public String getApply_state() {
        return apply_state;
    }

    public void setApply_state(String apply_state) {
        this.apply_state = apply_state;
    }
}
