package com.example.campus_system.entity;

public class StudentInfo {
    private String stu_id;
    private String stu_name;
    private String stu_photo;
    private String stu_sex;
    private String stu_age;
    private String stu_address;
    private String stu_birth;
    private String stu_phone;
    private String stu_email;
    private String stu_faculty;
    private String stu_major;
    private String stu_class;
    private String stu_time;
    private String role_id;

    public String getStu_id() {
        return stu_id;
    }

    public void setStu_id(String stu_id) {
        this.stu_id = stu_id;
    }

    public String getStu_name() {
        return stu_name;
    }

    public void setStu_name(String stu_name) {
        this.stu_name = stu_name;
    }

    public String getStu_photo() {
        return stu_photo;
    }

    public void setStu_photo(String stu_photo) {
        this.stu_photo = stu_photo;
    }

    public String getStu_sex() {
        return stu_sex;
    }

    public void setStu_sex(String stu_sex) {
        this.stu_sex = stu_sex;
    }

    public String getStu_age() {
        return stu_age;
    }

    public void setStu_age(String stu_age) {
        this.stu_age = stu_age;
    }

    public String getStu_address() {
        return stu_address;
    }

    public void setStu_address(String stu_address) {
        this.stu_address = stu_address;
    }

    public String getStu_birth() {
        return stu_birth;
    }

    public void setStu_birth(String stu_birth) {
        this.stu_birth = stu_birth;
    }

    public String getStu_phone() {
        return stu_phone;
    }

    public void setStu_phone(String stu_phone) {
        this.stu_phone = stu_phone;
    }

    public String getStu_email() {
        return stu_email;
    }

    public void setStu_email(String stu_email) {
        this.stu_email = stu_email;
    }

    public String getStu_faculty() {
        return stu_faculty;
    }

    public void setStu_faculty(String stu_faculty) {
        this.stu_faculty = stu_faculty;
    }

    public String getStu_major() {
        return stu_major;
    }

    public void setStu_major(String stu_major) {
        this.stu_major = stu_major;
    }

    public String getStu_class() {
        return stu_class;
    }

    public void setStu_class(String stu_class) {
        this.stu_class = stu_class;
    }

    public String getStu_time() {
        return stu_time;
    }

    public void setStu_time(String stu_time) {
        this.stu_time = stu_time;
    }

    public String getRole_id() {
        return role_id;
    }

    public void setRole_id(String role_id) {
        this.role_id = role_id;
    }

    @Override
    public String toString() {
        return "StudentInfo{" +
                "stu_id='" + stu_id + '\'' +
                ", stu_name='" + stu_name + '\'' +
                ", stu_photo='" + stu_photo + '\'' +
                ", stu_sex='" + stu_sex + '\'' +
                ", stu_age='" + stu_age + '\'' +
                ", stu_address='" + stu_address + '\'' +
                ", stu_birth='" + stu_birth + '\'' +
                ", stu_phone='" + stu_phone + '\'' +
                ", stu_email='" + stu_email + '\'' +
                ", stu_faculty='" + stu_faculty + '\'' +
                ", stu_major='" + stu_major + '\'' +
                ", stu_class='" + stu_class + '\'' +
                ", stu_time='" + stu_time + '\'' +
                ", role_id='" + role_id + '\'' +
                '}';
    }
}
