package com.example.campus_system.entity;

public class P_data {

    private String p_name;
    private String p_total_nowConfirm;
    private String p_total_wzz;
    private String p_total_confirm;
    private String p_total_dead;
    private String p_total_heal;
    private String p_today_confirm;
    private String p_today_wzz_add;

    @Override
    public String toString() {
        return "P_data{" +
                "p_name='" + p_name + '\'' +
                ", p_total_nowConfirm='" + p_total_nowConfirm + '\'' +
                ", p_total_wzz='" + p_total_wzz + '\'' +
                ", p_total_confirm='" + p_total_confirm + '\'' +
                ", p_total_dead='" + p_total_dead + '\'' +
                ", p_total_heal='" + p_total_heal + '\'' +
                ", p_today_confirm='" + p_today_confirm + '\'' +
                ", p_today_wzz_add='" + p_today_wzz_add + '\'' +
                '}';
    }

    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public String getP_total_nowConfirm() {
        return p_total_nowConfirm;
    }

    public void setP_total_nowConfirm(String p_total_nowConfirm) {
        this.p_total_nowConfirm = p_total_nowConfirm;
    }

    public String getP_total_wzz() {
        return p_total_wzz;
    }

    public void setP_total_wzz(String p_total_wzz) {
        this.p_total_wzz = p_total_wzz;
    }

    public String getP_total_confirm() {
        return p_total_confirm;
    }

    public void setP_total_confirm(String p_total_confirm) {
        this.p_total_confirm = p_total_confirm;
    }

    public String getP_total_dead() {
        return p_total_dead;
    }

    public void setP_total_dead(String p_total_dead) {
        this.p_total_dead = p_total_dead;
    }

    public String getP_total_heal() {
        return p_total_heal;
    }

    public void setP_total_heal(String p_total_heal) {
        this.p_total_heal = p_total_heal;
    }

    public String getP_today_confirm() {
        return p_today_confirm;
    }

    public void setP_today_confirm(String p_today_confirm) {
        this.p_today_confirm = p_today_confirm;
    }

    public String getP_today_wzz_add() {
        return p_today_wzz_add;
    }

    public void setP_today_wzz_add(String p_today_wzz_add) {
        this.p_today_wzz_add = p_today_wzz_add;
    }
}
