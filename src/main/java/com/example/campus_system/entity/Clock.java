package com.example.campus_system.entity;

public class Clock {
    private String clock_id;
    private String user_id;
    private String user_name;
    private String user_faculty;
    private String user_class;
    private String clock_time;
    private String clock_time_type;
    private String clock_area;
    private String temperature;
    private String cough;
    private String fever;

    @Override
    public String toString() {
        return "Clock{" +
                "clock_id='" + clock_id + '\'' +
                ", user_id='" + user_id + '\'' +
                ", user_name='" + user_name + '\'' +
                ", user_faculty='" + user_faculty + '\'' +
                ", user_class='" + user_class + '\'' +
                ", clock_time='" + clock_time + '\'' +
                ", clock_time_type='" + clock_time_type + '\'' +
                ", clock_area='" + clock_area + '\'' +
                ", temperature='" + temperature + '\'' +
                ", cough='" + cough + '\'' +
                ", fever='" + fever + '\'' +
                '}';
    }

    public String getClock_id() {
        return clock_id;
    }

    public void setClock_id(String clock_id) {
        this.clock_id = clock_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_faculty() {
        return user_faculty;
    }

    public void setUser_faculty(String user_faculty) {
        this.user_faculty = user_faculty;
    }

    public String getUser_class() {
        return user_class;
    }

    public void setUser_class(String user_class) {
        this.user_class = user_class;
    }

    public String getClock_time() {
        return clock_time;
    }

    public void setClock_time(String clock_time) {
        this.clock_time = clock_time;
    }

    public String getClock_time_type() {
        return clock_time_type;
    }

    public void setClock_time_type(String clock_time_type) {
        this.clock_time_type = clock_time_type;
    }

    public String getClock_area() {
        return clock_area;
    }

    public void setClock_area(String clock_area) {
        this.clock_area = clock_area;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getCough() {
        return cough;
    }

    public void setCough(String cough) {
        this.cough = cough;
    }

    public String getFever() {
        return fever;
    }

    public void setFever(String fever) {
        this.fever = fever;
    }
}
