package com.example.campus_system.entity;

import java.util.List;

public class EpmcInfo {

    private List<C_data> c_data;
    private List<P_data> p_data;
    private List<G_data> g_data;

    public List<C_data> getC_data() {
        return c_data;
    }

    public void setC_data(List<C_data> c_data) {
        this.c_data = c_data;
    }

    public List<P_data> getP_data() {
        return p_data;
    }

    public void setP_data(List<P_data> p_data) {
        this.p_data = p_data;
    }

    public List<G_data> getG_data() {
        return g_data;
    }

    public void setG_data(List<G_data> g_data) {
        this.g_data = g_data;
    }

    @Override
    public String toString() {
        return "EpmcInfo{" +
                "c_data=" + c_data +
                ", p_data=" + p_data +
                ", g_data=" + g_data +
                '}';
    }
}
