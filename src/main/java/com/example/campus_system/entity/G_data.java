package com.example.campus_system.entity;

public class G_data {
    private String g_id;
    private String g_localConfirm;
    private String g_nowConfirm;
    private String g_confirm;
    private String g_wzz;
    private String g_importedCase;
    private String g_dead;
    private String g_localConfirm_add;
    private String g_nowConfirm_add;
    private String g_confirm_add;
    private String g_wzz_add;
    private String g_importedCase_add;
    private String g_dead_add;

    @Override
    public String toString() {
        return "G_data{" +
                "g_id='" + g_id + '\'' +
                ", g_localConfirm='" + g_localConfirm + '\'' +
                ", g_nowConfirm='" + g_nowConfirm + '\'' +
                ", g_confirm='" + g_confirm + '\'' +
                ", g_wzz='" + g_wzz + '\'' +
                ", g_importedCase='" + g_importedCase + '\'' +
                ", g_dead='" + g_dead + '\'' +
                ", g_localConfirm_add='" + g_localConfirm_add + '\'' +
                ", g_nowConfirm_add='" + g_nowConfirm_add + '\'' +
                ", g_confirm_add='" + g_confirm_add + '\'' +
                ", g_wzz_add='" + g_wzz_add + '\'' +
                ", g_importedCase_add='" + g_importedCase_add + '\'' +
                ", g_dead_add='" + g_dead_add + '\'' +
                '}';
    }

    public String getG_id() {
        return g_id;
    }

    public void setG_id(String g_id) {
        this.g_id = g_id;
    }

    public String getG_localConfirm() {
        return g_localConfirm;
    }

    public void setG_localConfirm(String g_localConfirm) {
        this.g_localConfirm = g_localConfirm;
    }

    public String getG_nowConfirm() {
        return g_nowConfirm;
    }

    public void setG_nowConfirm(String g_nowConfirm) {
        this.g_nowConfirm = g_nowConfirm;
    }

    public String getG_confirm() {
        return g_confirm;
    }

    public void setG_confirm(String g_confirm) {
        this.g_confirm = g_confirm;
    }

    public String getG_wzz() {
        return g_wzz;
    }

    public void setG_wzz(String g_wzz) {
        this.g_wzz = g_wzz;
    }

    public String getG_importedCase() {
        return g_importedCase;
    }

    public void setG_importedCase(String g_importedCase) {
        this.g_importedCase = g_importedCase;
    }

    public String getG_dead() {
        return g_dead;
    }

    public void setG_dead(String g_dead) {
        this.g_dead = g_dead;
    }

    public String getG_localConfirm_add() {
        return g_localConfirm_add;
    }

    public void setG_localConfirm_add(String g_localConfirm_add) {
        this.g_localConfirm_add = g_localConfirm_add;
    }

    public String getG_nowConfirm_add() {
        return g_nowConfirm_add;
    }

    public void setG_nowConfirm_add(String g_nowConfirm_add) {
        this.g_nowConfirm_add = g_nowConfirm_add;
    }

    public String getG_confirm_add() {
        return g_confirm_add;
    }

    public void setG_confirm_add(String g_confirm_add) {
        this.g_confirm_add = g_confirm_add;
    }

    public String getG_wzz_add() {
        return g_wzz_add;
    }

    public void setG_wzz_add(String g_wzz_add) {
        this.g_wzz_add = g_wzz_add;
    }

    public String getG_importedCase_add() {
        return g_importedCase_add;
    }

    public void setG_importedCase_add(String g_importedCase_add) {
        this.g_importedCase_add = g_importedCase_add;
    }

    public String getG_dead_add() {
        return g_dead_add;
    }

    public void setG_dead_add(String g_dead_add) {
        this.g_dead_add = g_dead_add;
    }
}
