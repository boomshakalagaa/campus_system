package com.example.campus_system.entity;

public class ClockNum {
    private String clock_time;
    private String clock_num;

    @Override
    public String toString() {
        return "ClockNum{" +
                "clock_time='" + clock_time + '\'' +
                ", clock_num='" + clock_num + '\'' +
                '}';
    }

    public String getClock_time() {
        return clock_time;
    }

    public void setClock_time(String clock_time) {
        this.clock_time = clock_time;
    }

    public String getClock_num() {
        return clock_num;
    }

    public void setClock_num(String clock_num) {
        this.clock_num = clock_num;
    }
}
