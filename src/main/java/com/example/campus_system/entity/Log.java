package com.example.campus_system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

@TableName("log")
public class Log {

    @TableId(type = IdType.AUTO)
    private String log_id;
    private String log_operation;
    private String log_access_addr;
    private String log_access_path;
    private String log_ip;
    private String log_access_method;
    private String log_access_controller;
    private String log_access_time;
    private String log_time_use;
    private String log_parameter;

    @Override
    public String toString() {
        return "Log{" +
                "log_id='" + log_id + '\'' +
                ", log_operation='" + log_operation + '\'' +
                ", log_access_addr='" + log_access_addr + '\'' +
                ", log_access_path='" + log_access_path + '\'' +
                ", log_ip='" + log_ip + '\'' +
                ", log_access_method='" + log_access_method + '\'' +
                ", log_access_controller='" + log_access_controller + '\'' +
                ", log_access_time='" + log_access_time + '\'' +
                ", log_time_use='" + log_time_use + '\'' +
                ", log_parameter='" + log_parameter + '\'' +
                '}';
    }

    public String getLog_id() {
        return log_id;
    }

    public void setLog_id(String log_id) {
        this.log_id = log_id;
    }

    public String getLog_operation() {
        return log_operation;
    }

    public void setLog_operation(String log_operation) {
        this.log_operation = log_operation;
    }

    public String getLog_access_addr() {
        return log_access_addr;
    }

    public void setLog_access_addr(String log_access_addr) {
        this.log_access_addr = log_access_addr;
    }

    public String getLog_access_path() {
        return log_access_path;
    }

    public void setLog_access_path(String log_access_path) {
        this.log_access_path = log_access_path;
    }

    public String getLog_ip() {
        return log_ip;
    }

    public void setLog_ip(String log_ip) {
        this.log_ip = log_ip;
    }

    public String getLog_access_method() {
        return log_access_method;
    }

    public void setLog_access_method(String log_access_method) {
        this.log_access_method = log_access_method;
    }

    public String getLog_access_controller() {
        return log_access_controller;
    }

    public void setLog_access_controller(String log_access_controller) {
        this.log_access_controller = log_access_controller;
    }

    public String getLog_access_time() {
        return log_access_time;
    }

    public void setLog_access_time(String log_access_time) {
        this.log_access_time = log_access_time;
    }

    public String getLog_time_use() {
        return log_time_use;
    }

    public void setLog_time_use(String log_time_use) {
        this.log_time_use = log_time_use;
    }

    public String getLog_parameter() {
        return log_parameter;
    }

    public void setLog_parameter(String log_parameter) {
        this.log_parameter = log_parameter;
    }
}
