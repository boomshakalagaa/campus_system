package com.example.campus_system.entity;

public class LeaveNum {
    private String leave_time;
    private String leave_school_num;

    public String getLeave_time() {
        return leave_time;
    }

    public void setLeave_time(String leave_time) {
        this.leave_time = leave_time;
    }

    public String getLeave_school_num() {
        return leave_school_num;
    }

    public void setLeave_school_num(String leave_school_num) {
        this.leave_school_num = leave_school_num;
    }

    @Override
    public String toString() {
        return "LeaveNum{" +
                "leave_time='" + leave_time + '\'' +
                ", leave_school_num='" + leave_school_num + '\'' +
                '}';
    }
}
