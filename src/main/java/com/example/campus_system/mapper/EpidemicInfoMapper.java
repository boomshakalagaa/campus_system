package com.example.campus_system.mapper;

import com.example.campus_system.entity.C_data;
import com.example.campus_system.entity.G_data;
import com.example.campus_system.entity.P_data;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EpidemicInfoMapper {

    List<C_data> select_C_data(String p_name);


    List<P_data> select_P_data();


    G_data select_G_data();
}
