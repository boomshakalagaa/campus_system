package com.example.campus_system.mapper;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.Notice;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Repository
public interface NoticeMapper {

    /**
     * 查询总通知
     * @return
     */
    List<Notice> selectNotice();

    /**
     * 查询学校通知
     * @return
     */
    List<Notice> selectIndexNotice();

    /**
     * 查询教工通知
     * @return
     */
    List<Notice> selectTeacherNotice();

    /**
     * 查询班级通知
     * @return
     */
    List<Notice> selectClassNotice(String notice_class);

    /**
     * 添加通知
     * @return
     */
    int insertNotice(String notice_title,String notice_content,String notice_user,String notice_type,String notice_time,String notice_class);

    /**
     * 通知审核
     * @return
     */
    int updateNotice(String notice_id,String notice_state);

    /**
     * 删除通知
     * @return
     */
    int deleteNotice(String notice_id);

    /**
     * 搜索通知
     * @return
     */
    List<Notice> searchNotice(String search,String searchtype);

    /**
     * * 学生
     * 搜索该班级通知
     * @return
     */
    List<Notice> searchNoticeClass(String search,String notice_class);

    /**
     * 学生
     * 搜索通知公告
     * @return
     */
    List<Notice> stuSearchNotice(String search, String searchtype);

    /**
     * 老师
     * 搜索该院系教工通知
     * @return
     */
    List<Notice> searchNoticeTeacher(String search);



    /**
     * 老师
     * 查询自己班级通知
     * @return
     */
    List<Notice> TeacherselectClassNotice(String notice_class);

    /**
     * 老师
     * 搜索该班级通知
     * @return
     */
    List<Notice> teacherSearchClassNotice(String search,String notice_class);
}
