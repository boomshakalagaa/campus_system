package com.example.campus_system.mapper;

import com.example.campus_system.entity.ClockNum;
import com.example.campus_system.entity.LeaveNum;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NumMapper {

    //日志数
    String selectLogNum();
    //通知数
    String selectNoticeNum();
    //学生数
    String selectStuNum();
    //老师数
    String selectTacherNum();
    //申请数
    String selectApplyNum();
    //报告数
    String selectReportNum();
    //待审核通知数
    String selectWaitingNoticeNum();
    //用户数
    String selectUserNum();
    //待审核申请数
    String selectWatingApplyNum();
    //系统异常次数
    String selectErrNum();
    //离校数
    List<LeaveNum> selectLeaveSchoolNum();
    //全天打卡数，不分时段
    List<ClockNum> selectClockNum();

}
