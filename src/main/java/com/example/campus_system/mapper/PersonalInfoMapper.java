package com.example.campus_system.mapper;

import com.example.campus_system.entity.StudentInfo;
import com.example.campus_system.entity.TeacherInfo;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonalInfoMapper {

    /**
     * 查询教师个人信息
     */
    TeacherInfo selectTeacherInfo(String user_id);

    /**
     *老师或管理员
     *修改个人信息
     * 修改图片
     */
    int updateTechaerInfo(String teacher_id,String teacher_name, String teacher_age
            ,String teacher_photo, String teacher_address , String teacher_birth,String teacher_sex
            ,String teacher_phone,String teacher_email,String teacher_faculty);

    /**
     *老师或管理员
     *修改个人信息
     * 不修改图片
     */
    int updateTechaerInfo1(String teacher_id,String teacher_name, String teacher_age
            , String teacher_address , String teacher_birth,String teacher_sex
            ,String teacher_phone,String teacher_email,String teacher_faculty);

    /**
     * 查询学生个人信息
     */
    StudentInfo selectStudentInfo(String stu_id);

    /**
     *学生
     *修改个人信息
     * 修改图片
     */
    int updateStudentInfo(String stu_id,String stu_name, String stu_age
            ,String stu_photo, String stu_address , String stu_birth,String stu_sex
            ,String stu_phone,String stu_email,String stu_faculty,String stu_class);

    /**
     *学生
     *修改个人信息
     * 不修改图片
     */
    int updateStudentInfo1(String stu_id,String stu_name, String stu_age
            , String stu_address , String stu_birth,String stu_sex
            ,String stu_phone,String stu_email,String stu_faculty,String stu_class);
}
