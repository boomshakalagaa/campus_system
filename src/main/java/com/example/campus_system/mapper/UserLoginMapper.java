package com.example.campus_system.mapper;

import com.example.campus_system.entity.UserLogin;
import org.springframework.stereotype.Repository;

@Repository
public interface UserLoginMapper{

    String findrole(String user_id);
    UserLogin selectUserLogin(String user_id);

}
