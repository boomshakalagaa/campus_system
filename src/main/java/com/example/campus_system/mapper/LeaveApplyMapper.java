package com.example.campus_system.mapper;

import com.example.campus_system.entity.LeaveApply;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LeaveApplyMapper {

    /**
     * 管理员
     * 离校申请查询
     * @return
     */
    List<LeaveApply> adminSelectLeaveApply();

    /**
     * 管理员
     * 离校申请审核
     * @return
     */
    int adminUpdateLeaveApply(String apply_id,String apply_state);

    /**
     * 管理员
     * 搜索离校申请
     * @return
     */
    List<LeaveApply> adminSearchLeaveApply(String search,String search_faculty);

    /**
     * 管理员
     * 删除离校申请
     * @return
     */
    int adminDeleteLeaveApply(String apply_id);

    /**
     * 学生
     * 离校申请查询
     * @return
     */
    List<LeaveApply> studentSelectLeaveApply(String stu_id);

    /**
     * 学生
     * 离校申请查询
     * @return
     */
    int studentAddLeaveApply(String stu_id,String stu_name,String stu_faculty,String stu_class,String reason
    ,String travel,String leave_time,String back_time);


    /**
     * 老师
     * 离校申请查询
     * @return
     */
    List<LeaveApply> teacherSelectLeaveApply(String stu_class);

    /**
     * 老师
     * 搜索离校申请
     * @return
     */
    List<LeaveApply> teacherSearchLeaveApply(String search,String stu_class);
}
