package com.example.campus_system.mapper;

import com.example.campus_system.entity.SystemPermissions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SystemPermissionMapper {


    /**
     * 管理员
     * 查询系统权限
     * 权限英文名，权限中文名，权限描述
     */
    List<SystemPermissions> selectSystemPermissions();

    /**
     * 管理员
     * 编辑系统权限
     * 权限英文名，权限中文名，权限描述
     */
    int editSystemPermissions(String role_id,String role_name,String role_cname,String role_describe);
}
