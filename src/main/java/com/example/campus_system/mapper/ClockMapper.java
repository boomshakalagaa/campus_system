package com.example.campus_system.mapper;

import com.example.campus_system.entity.Clock;
import com.example.campus_system.entity.StudentInfo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClockMapper {

    /**
     * 管理员
     * 查询打卡
     * @return
     */
    List<Clock> adminSelectAllClock();

    /**
     * 管理员
     * 搜索打卡
     * @return
     */
    List<Clock> adminSearchClock(String user_id,String user_faculty,String cough,String fever,String temperature);

    /**
     * 管理员
     * 打卡
     * @return
     */
    int adminAddClock(String user_id,String user_name,String user_faculty,String clock_time, String clock_time_type, String clock_area, String temperature, String cough, String fever);

    /**
     * 管理员,辅导员
     * 删除打卡
     * @return
     */
    int deleteClock(String clock_id);


    /**
     * 管理员
     * 查询打卡
     * @return
     */
    List<Clock> studentSelectClock(String stu_id);


    /**
     * 学生
     * 打卡
     * @return
     */
    int studentAddClock(String stu_id,String stu_name,String stu_faculty,String stu_class,String clock_time
    ,String clock_time_type,String clock_area,String temperature,String cough,String fever);


    /**
     * 老师
     * 查询班级打卡
     * @return
     */
    List<Clock> teacherSelectAllClock(String user_class);


    /**
     * 老师
     * 查询个人打卡
     * @return
     */
    List<Clock> teacherSelectPSClock(String user_id);

    /**
     * 老师
     * 搜索班级打卡
     * @return
     */
    List<Clock> teacherSearchClock(String user_id,String cough,String fever,String temperature,String user_class);

    List<StudentInfo> teacherSearchNotClock(String clock_time, String user_class);
}
