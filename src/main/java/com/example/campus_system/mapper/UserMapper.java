package com.example.campus_system.mapper;

import com.example.campus_system.entity.StudentInfo;
import com.example.campus_system.entity.TeacherInfo;
import com.example.campus_system.entity.UserLogin;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMapper {

    /**
     * 查询所有学生
     * @return
     */
    List<StudentInfo> selectStudentAll();


    /**
     * 搜索学生 id，专业，班级
     */
    List<StudentInfo> searchStudent(String stu_id,String stu_faculty);

    /**
     * 用id查询学生
     * @return
     */
    StudentInfo selectStudent(String stu_id);

    /**
     * 添加user
     */
    int addUser(UserLogin user);

    /**
     * 添加用户权限
     */
    int addUserRole(String user_id,String role_id);

    /**
     * 修改用户权限
     */
    int updateUserRole(String user_id,String role_id);


    /**
     * 添加学生
     */

    int addStudent(String stu_id,String stu_name, String stu_sex, String stu_age, String stu_address
            , String stu_birth, String stu_phone, String stu_email, String stu_faculty
            , String stu_major, String stu_class,String stu_time);

    /**
     * 添加报告表
     */
    int addReport(String stu_id,String stu_name,String stu_faculty,String stu_class);

    /**
     * 修改学生信息
     */
    int updateStudent(String stu_id, String stu_name, String stu_sex, String stu_age, String stu_address
            , String stu_birth, String stu_phone, String stu_email, String stu_faculty, String stu_major, String stu_class);

    /**
     * 删除学生信息
     */
    int deleteStudent(String stu_id);

    /**
     * 重置密码
     */
    int resetPassword(String user_id,String user_password);

    /**
     * 查询所有老师
     * @return
     */
    List<TeacherInfo> selectTeacherAll();

    /**
     * 搜索老师 id，院系
     */
    List<TeacherInfo> searchTeacher( String teacher_id, String teacher_faculty);

    /**
     * 添加教师，管理员
     */
    int addTeacher(String teacher_id,String teacher_name,String teacher_sex,String teacher_age
            ,String teacher_address,String teacher_birth,String teacher_phone,String teacher_email,String teacher_faculty);

    /**
     * 修改教师，管理员
     */
    int updateTeacher(String teacher_id, String teacher_name, String teacher_sex, String teacher_age
            , String teacher_address, String teacher_birth, String teacher_phone, String teacher_email, String teacher_faculty);

    /**
     * 删除教师信息
     */
    int deleteTeacher(String teacher_id);

    /**
     * 查老师管理的班级
     * @param teacher_id
     * @return
     */
    String selectClass(String teacher_id);


}
