package com.example.campus_system.mapper;

import com.example.campus_system.entity.Report;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReportMapper {

    /**
     * 查询所有核算报告
     * @return
     */
    List<Report> selectReportAll();

    /**
     * 搜索报告
     * @return
     */
    List<Report> searchReport(String stu_id, String stu_name, String stu_faculty);

    /**
     *学生
     *查询核酸报告
     */
    Report StudentselectReport(String stu_id);


    /**
     *学生
     *上传核酸报告
     */
    int uploadReport(String stu_id,String stu_name,String stu_faculty,String stu_class,String test_report,String report_time);

    /**
     * 老师
     * 查询班级报告
     * @return
     */
    List<Report> teacherSelectReport(String stu_class);

    /**
     * 老师
     * 搜索报告
     * @return
     */
    List<Report> teacherSearchReport(String stu_id, String stu_class);

}
