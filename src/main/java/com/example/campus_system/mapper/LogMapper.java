package com.example.campus_system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.example.campus_system.entity.Log;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LogMapper extends BaseMapper<Log> {

    List<Log> searchLog(String search);
}
