package com.example.campus_system.controller;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.StudentInfo;
import com.example.campus_system.entity.TeacherInfo;
import com.example.campus_system.service.PersonalInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;

@Controller
@RequestMapping("/psinfo")
public class PersonalInfoController {

    /**
     * 查询老师个人信息
     */
    @Autowired
    PersonalInfoService personalInfoService;

    @ResponseBody
    @RequestMapping("/select/teacher")
    public BaseResponse<TeacherInfo> selectTeacherInfo(Authentication authentication){

        return personalInfoService.selectTeacherInfo(authentication.getName());
    }


    /**
     *老师或管理员
     *修改个人信息
     */
    @ResponseBody
    @RequestMapping("/update/teacher")
    public synchronized BaseResponse updateTechaerInfo(Authentication authentication, @RequestParam("image") MultipartFile teacher_photo, String teacher_name, String teacher_age
            , String teacher_address , String teacher_birth,String teacher_sex
            ,String teacher_phone,String teacher_email,String teacher_faculty) {

        TeacherInfo teacherInfo = new TeacherInfo();
        System.out.println("teacher_photo.isEmpty() = " + teacher_photo.isEmpty());

        //有图片时
        if (!teacher_photo.isEmpty()) {
            // 获取文件名
            String teacher_photoName = teacher_photo.getOriginalFilename();
            //最后视频的命名
            String teacherphoto = System.currentTimeMillis()+teacher_photoName;
            System.out.println("teacherphoto = " + teacherphoto);
            // 文件上传后的路径
            String coverPath = "E:\\campus_system\\file\\teacher\\";
            File dest = new File(coverPath + teacherphoto);

            // 检测是否存在目录
            if (!dest.getParentFile().exists()) {
                dest.getParentFile().mkdirs();
            }
            try {
                teacher_photo.transferTo(dest);
            } catch (IOException e) {
                e.printStackTrace();
            }

            //存数据库
            String image = "http://localhost:8080/campus_system/file/teacher"+"/"+teacherphoto;


            teacherInfo.setTeacher_id(authentication.getName());
            teacherInfo.setTeacher_name(teacher_name);
            teacherInfo.setTeacher_age(teacher_age);
            teacherInfo.setTeacher_photo(image);
            teacherInfo.setTeacher_address(teacher_address);
            teacherInfo.setTeacher_birth(teacher_birth);
            teacherInfo.setTeacher_sex(teacher_sex);
            teacherInfo.setTeacher_phone(teacher_phone);
            teacherInfo.setTeacher_email(teacher_email);
            teacherInfo.setTeacher_faculty(teacher_faculty);

        }else {
            //无图片时
            teacherInfo.setTeacher_id(authentication.getName());
            teacherInfo.setTeacher_name(teacher_name);
            teacherInfo.setTeacher_age(teacher_age);
            teacherInfo.setTeacher_address(teacher_address);
            teacherInfo.setTeacher_birth(teacher_birth);
            teacherInfo.setTeacher_sex(teacher_sex);
            teacherInfo.setTeacher_phone(teacher_phone);
            teacherInfo.setTeacher_email(teacher_email);
            teacherInfo.setTeacher_faculty(teacher_faculty);
        }
        return  personalInfoService.updateTechaerInfo(teacherInfo);
    }

    /**
     * 查询学生个人信息
     */
    @ResponseBody
    @RequestMapping("/select/student")
    public BaseResponse<StudentInfo> selectStudentInfo(Authentication authentication){

        return personalInfoService.selectStudentInfo(authentication.getName());
    }

    /**
     *学生
     *修改个人信息
     */
    @ResponseBody
    @RequestMapping("/update/student")
    public synchronized BaseResponse updateStudentInfo(Authentication authentication, @RequestParam("image") MultipartFile stu_photo, String stu_name, String stu_age
            , String stu_address , String stu_birth,String stu_sex
            ,String stu_phone,String stu_email,String stu_faculty,String stu_class) {

        StudentInfo studentInfo = new StudentInfo();
        System.out.println("stu_photo.isEmpty() = " + stu_photo.isEmpty());

        //有图片时
        if (!stu_photo.isEmpty()) {
            // 获取文件名
            String stu_photoName = stu_photo.getOriginalFilename();
            //最后视频的命名
            String studentphoto = System.currentTimeMillis()+stu_photoName;
            System.out.println("studentphoto = " + studentphoto);
            // 文件上传后的路径
            String coverPath = "E:\\campus_system\\file\\student\\";
            File dest = new File(coverPath + studentphoto);

            // 检测是否存在目录
            if (!dest.getParentFile().exists()) {
                dest.getParentFile().mkdirs();
            }
            try {
                stu_photo.transferTo(dest);
            } catch (IOException e) {
                e.printStackTrace();
            }

            //存数据库
            String image = "http://localhost:8080/campus_system/file/student"+"/"+studentphoto;


            studentInfo.setStu_id(authentication.getName());
            studentInfo.setStu_name(stu_name);
            studentInfo.setStu_age(stu_age);
            studentInfo.setStu_photo(image);
            studentInfo.setStu_address(stu_address);
            studentInfo.setStu_birth(stu_birth);
            studentInfo.setStu_sex(stu_sex);
            studentInfo.setStu_phone(stu_phone);
            studentInfo.setStu_email(stu_email);
            studentInfo.setStu_faculty(stu_faculty);
            studentInfo.setStu_class(stu_class);

        }else {
            //无图片时
            studentInfo.setStu_id(authentication.getName());
            studentInfo.setStu_name(stu_name);
            studentInfo.setStu_age(stu_age);
            studentInfo.setStu_address(stu_address);
            studentInfo.setStu_birth(stu_birth);
            studentInfo.setStu_sex(stu_sex);
            studentInfo.setStu_phone(stu_phone);
            studentInfo.setStu_email(stu_email);
            studentInfo.setStu_faculty(stu_faculty);
            studentInfo.setStu_class(stu_class);
        }
        return  personalInfoService.updateStudentInfo(studentInfo);
    }
}
