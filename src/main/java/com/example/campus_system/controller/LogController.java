package com.example.campus_system.controller;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.Log;
import com.example.campus_system.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/log")
public class LogController {

    @Autowired
    LogService logService;

    /**
     * 查询日志
     */
    @ResponseBody
    @RequestMapping("/select")
    public BaseResponse<List<Log>> selectLog(){
        return logService.selectLog();
    }


    /**
     * 搜索日志
     */
    @ResponseBody
    @RequestMapping("search")
    public BaseResponse<List<Log>> searchLog(String search){
        return logService.searchLog(search);
    }
}
