package com.example.campus_system.controller;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.Clock;
import com.example.campus_system.entity.ClockNum;
import com.example.campus_system.entity.LeaveNum;
import com.example.campus_system.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class IndexController {

    @Autowired
    IndexService indexService;

    /**
     * 查日志数，待审核通知数，用户数，系统异常次数，待审核申请数，核算报告数
     * @return
     */
    @RequestMapping("/select/num")
    @ResponseBody
    public BaseResponse selectNum(){
        return indexService.selectNum();
    }

    /**
     * 查离校
     * @return
     */
    @RequestMapping("/select/map1")
    @ResponseBody
    public List<LeaveNum> selectmap1(){
        return indexService.selectmap1();
    }

    /**
     * 打卡数
     * @return
     */
    @RequestMapping("/select/map2")
    @ResponseBody
    public List<ClockNum> selectmap2(){
        return indexService.selectmap2();
    }
}
