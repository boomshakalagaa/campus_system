package com.example.campus_system.controller;


import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@RequestMapping
@Controller
public class LoginController {

    @RequestMapping("/login")
    public String login(){
        return "login";
    }
    @RequestMapping("/loginsuccess")
    public String loginsuccess(Authentication authentication){
        System.out.println("authentication.getAuthorities() = " + authentication.getAuthorities());
        if(authentication.getAuthorities().contains("ROLE_admin")){
            return "redict:indexadmin.html";
        }else return "loginsuccess";

    }
    @RequestMapping("/loginfailure")
    public String loginfailure(HttpServletRequest httpServletRequest){
        return "loginfailure";
    }
}
