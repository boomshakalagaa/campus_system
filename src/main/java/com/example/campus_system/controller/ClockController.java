package com.example.campus_system.controller;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.Clock;
import com.example.campus_system.entity.StudentInfo;
import com.example.campus_system.service.ClockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/clock")
public class ClockController {

    @Autowired
    ClockService clockService;

    /**
     * 管理员
     * 查询打卡
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/select")
    public BaseResponse<List<Clock>> adminSelectAllClock(){
        return clockService.adminSelectAllClock();
    }

    /**
     * 管理员
     * 搜索打卡
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/search")
    public BaseResponse<List<Clock>> adminSearchClock(String user_id,String user_faculty ,String cough,String fever,String temperature){
        System.out.println("cough:"+cough);
        return clockService.adminSearchClock(user_id, user_faculty,cough,fever,temperature);
    }

    /**
     * 管理员，老师
     * 打卡
     * @return
     */
    @ResponseBody
    @RequestMapping("/teacher/doclock")
    public BaseResponse adminAddClock(Authentication authentication, String clock_time_type, String clock_area , String temperature, String cough, String fever){
        System.out.println("authentication.getName() = " + authentication.getName());
        return clockService.adminAddClock(authentication.getName(),clock_time_type,clock_area,temperature,cough,fever);
    }

    /**
     * 管理员，辅导员
     * 删除打卡
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public BaseResponse deleteClock(String clock_id){
        return clockService.deleteClock(clock_id);
    }


    /**
     * 学生
     * 查询打卡
     * @return
     */
    @ResponseBody
    @RequestMapping("/stu/select")
    public BaseResponse<List<Clock>> studentSelectClock(Authentication authentication){
        return clockService.studentSelectClock(authentication.getName());
    }

    /**
     * 学生
     * 打卡
     * @return
     */
    @ResponseBody
    @RequestMapping("/stu/doclock")
    public BaseResponse<List<Clock>> studentAddClock(Authentication authentication
            , String clock_time_type, String clock_area , String temperature, String cough, String fever){
        return clockService.studentAddClock(authentication.getName(),clock_time_type,clock_area,temperature,cough,fever);
    }

    /**
     * 老师
     * 查询班级打卡
     * @return
     */
    @ResponseBody
    @RequestMapping("/teacher/select")
    public BaseResponse<List<Clock>> teacherSelectAllClock(Authentication authentication){
        return clockService.teacherSelectAllClock(authentication.getName());
    }

    /**
     * 老师
     * 查询个人打卡
     * @return
     */
    @ResponseBody
    @RequestMapping("/teacher/select/ps")
    public BaseResponse<List<Clock>> teacherSelectPSClock(Authentication authentication){
        return clockService.teacherSelectPSClock(authentication.getName());
    }


    /**
     * 老师
     * 搜索班级打卡
     * @return
     */
    @ResponseBody
    @RequestMapping("/teacher/search")
    public BaseResponse<List<Clock>> teacherSearchClock(String user_id ,String cough,String fever,String temperature,Authentication authentication){
        System.out.println("cough:"+cough);
        System.out.println("temperature = " + temperature);
        return clockService.teacherSearchClock(user_id,cough,fever,temperature,authentication.getName());
    }

    /**
     * 老师
     * 查询班级未打卡
     * @return
     */
    @ResponseBody
    @RequestMapping("/teacher/select/not")
    public BaseResponse<List<StudentInfo>> teacherSearchNotClock(Authentication authentication){
        return clockService.teacherSearchNotClock(authentication.getName());
    }


}
