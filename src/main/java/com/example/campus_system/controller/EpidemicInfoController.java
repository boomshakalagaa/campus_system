package com.example.campus_system.controller;


import com.example.campus_system.entity.*;
import com.example.campus_system.service.EpidemicInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/epmc")
public class EpidemicInfoController {

    @Autowired
    EpidemicInfoService epidemicInfoService;

    @ResponseBody
    @RequestMapping("/select/g")
    public BaseResponse<G_data> selectGuoEpmcInfo(){
        return epidemicInfoService.selectEpmcInfo();
    }

    @ResponseBody
    @RequestMapping("/select/p")
    public BaseResponse<List<P_data>> selectPEpmcInfo(){
        return epidemicInfoService.selectPEpmcInfo();
    }

    @ResponseBody
    @RequestMapping("/select/c")
    public BaseResponse<List<C_data>> selectPEpmcInfo(String p_name){
        return epidemicInfoService.selectCEpmcInfo(p_name);
    }


}
