package com.example.campus_system.controller;


import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.SystemPermissions;
import com.example.campus_system.service.SystemPermissionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/sysp")
public class SystemPermissionsController {


    /**
     * 管理员
     * 查询系统权限
     * 权限英文名，权限中文名，权限描述
     */
    @Autowired
    SystemPermissionsService systemPermissionsService;

    @ResponseBody
    @RequestMapping("/select")
    public BaseResponse<List<SystemPermissions>> selectSystemPermissions(){
        return systemPermissionsService.selectSystemPermissions();
    }
    /**
     * 管理员
     * 编辑系统权限
     * 权限英文名，权限中文名，权限描述
     */
    @ResponseBody
    @RequestMapping("/edit")
    public BaseResponse editSystemPermissions( String role_id,String role_name,String role_cname,String role_describe){
        SystemPermissions systemPermissions = new SystemPermissions();
        systemPermissions.setRole_id(role_id);
        systemPermissions.setRole_name(role_name);
        systemPermissions.setRole_cname(role_cname);
        systemPermissions.setRole_describe(role_describe);
        return systemPermissionsService.editSystemPermissions(systemPermissions);
    }
}
