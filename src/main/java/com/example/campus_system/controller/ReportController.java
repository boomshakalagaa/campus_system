package com.example.campus_system.controller;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.Report;
import com.example.campus_system.entity.StudentInfo;
import com.example.campus_system.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/report")
public class ReportController {

    @Autowired
    ReportService reportService;

    /**
     * 查询所有核算报告
     * @return
     */
    @ResponseBody
    @RequestMapping("/select/all")
    public BaseResponse<List<Report>> selectReportAll(){
        return reportService.selectReportAll();
    }

    /**
     * 管理员
     * 搜索报告
     * @return
     */
    @ResponseBody
    @RequestMapping("/search")
    public BaseResponse<List<Report>> searchReport(String stu_id,String stu_name,String stu_faculty){
        return reportService.searchReport(stu_id,stu_name,stu_faculty);
    }


    /**
     *学生
     *查询核酸报告
     */
    @ResponseBody
    @RequestMapping("/stu/select")
    public BaseResponse<Report> StudentselectReport(Authentication authentication){
        return reportService.StudentselectReport(authentication.getName());
    }


    /**
     *学生
     *上传核酸报告
     */
    @ResponseBody
    @RequestMapping("/stu/upload")
    public synchronized BaseResponse uploadReport(Authentication authentication, @RequestParam("image") MultipartFile test_report) {

        Report reportentity = new Report();
        String re=reportService.StudentselectReport(authentication.getName()).getData().getTest_report();
        if (re==null||re==""){
            //有图片时
            if (!test_report.isEmpty()) {
                // 获取文件名
                String testreport = test_report.getOriginalFilename();
                //最后视频的命名
                String report = System.currentTimeMillis()+testreport;
                // 文件上传后的路径
                String coverPath = "E:\\campus_system\\file\\report\\";
                File dest = new File(coverPath + report);

                // 检测是否存在目录
                if (!dest.getParentFile().exists()) {
                    dest.getParentFile().mkdirs();
                }
                try {
                    test_report.transferTo(dest);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                //存数据库
                String image = "http://localhost:8080/campus_system/file/report"+"/"+report;
                reportentity.setStu_id(authentication.getName());
                reportentity.setTest_report(image);
            }else {
                //无图片时
                reportentity.setStu_id(authentication.getName());
            }
            return  reportService.uploadReport(reportentity);
        }else {
            BaseResponse baseResponse = new BaseResponse();
            baseResponse.setCode(400);
            baseResponse.setMsg("重复上传");
            return baseResponse;
        }
    }


    /**
     * 老师
     * 查询班级报告
     * @return
     */
    @ResponseBody
    @RequestMapping("/teacher/select")
    public BaseResponse<List<Report>> teacherSelectReport(Authentication authentication){
        return reportService.teacherSelectReport(authentication.getName());
    }


    /**
     * 老师
     * 搜索报告
     * @return
     */
    @ResponseBody
    @RequestMapping("/teacher/search")
    public BaseResponse<List<Report>> teacherSearchReport(String stu_id,Authentication authentication){
        return reportService.teacherSearchReport(stu_id,authentication.getName());
    }

}
