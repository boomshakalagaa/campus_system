package com.example.campus_system.controller;

import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.LeaveApply;
import com.example.campus_system.service.LeaveApplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/leave")
public class LeaveApplyController {

    @Autowired
    LeaveApplyService leaveApplyService;

    /**
     * 管理员
     * 离校申请查询
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/select")
    public BaseResponse<List<LeaveApply>> adminSelectLeaveApply(){
        return leaveApplyService.adminSelectLeaveApply();
    }

    /**
     * 管理员
     * 离校申请审核
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/update")
    public BaseResponse adminUpdateLeaveApply(String apply_id,String apply_state){
        System.out.println("apply_id = " + apply_id);
        return leaveApplyService.adminUpdateLeaveApply(apply_id,apply_state);
    }

    /**
     * 管理员
     * 搜索离校申请
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/search")
    public BaseResponse<List<LeaveApply>> adminSearchLeaveApply(String search,String search_faculty){
        return leaveApplyService.adminSearchLeaveApply(search,search_faculty);
    }

    /**
     * 管理员
     * 删除离校申请
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/delete")
    public BaseResponse adminDeleteLeaveApply(String apply_id){
        return leaveApplyService.adminDeleteLeaveApply(apply_id);
    }

    /**
     * 学生
     * 离校申请查询
     * @return
     */
    @ResponseBody
    @RequestMapping("/stu/select")
    public BaseResponse<List<LeaveApply>> studentSelectLeaveApply(Authentication authentication){
        return leaveApplyService.studentSelectLeaveApply(authentication.getName());
    }

    /**
     * 学生
     * 离校申请查询
     * @return
     */
    @ResponseBody
    @RequestMapping("/stu/add")
    public BaseResponse studentAddLeaveApply(Authentication authentication,String reason,String travel,String leave_time,String back_time){
        return leaveApplyService.studentAddLeaveApply(authentication.getName(),reason,travel,leave_time,back_time);
    }

    /**
     * 老师
     * 离校申请查询
     * @return
     */
    @ResponseBody
    @RequestMapping("/teacher/select")
    public BaseResponse<List<LeaveApply>> teacherSelectLeaveApply(Authentication authentication){
        return leaveApplyService.teacherSelectLeaveApply(authentication.getName());
    }


    /**
     * 老师
     * 搜索离校申请
     * @return
     */
    @ResponseBody
    @RequestMapping("/teacher/search")
    public BaseResponse<List<LeaveApply>> teacherSearchLeaveApply(String search,Authentication authentication){
        return leaveApplyService.teacherSearchLeaveApply(search,authentication.getName());
    }

}
