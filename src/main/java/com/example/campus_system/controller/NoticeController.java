package com.example.campus_system.controller;


import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.Notice;
import com.example.campus_system.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/ntc")
public class NoticeController {


    @Autowired
    NoticeService noticeService;


    /**
     * 查询总通知
     * @return
     */
    @RequestMapping("/select/all")
    @ResponseBody
    public BaseResponse<List<Notice>> selectNotice(){
        return noticeService.selectNotice();
    }

    /**
     * 查询通知公告
     * @return
     */
    @RequestMapping("/select/index")
    @ResponseBody
    public BaseResponse<List<Notice>> selectIndexNotice(){
        return noticeService.selectIndexNotice();
    }

    /**
     * 查询教工通知
     * @return
     */
    @RequestMapping("/select/teacher")
    @ResponseBody
    public BaseResponse<List<Notice>> selectTeacherNotice(Authentication authentication){
        return noticeService.selectTeacherNotice(authentication.getName());
    }

    /**
     * 查询班级通知
     * @return
     */
    @RequestMapping("/select/class")
    @ResponseBody
    public BaseResponse<List<Notice>> selectClassNotice(Authentication authentication){
        return noticeService.selectClassNotice(authentication.getName());
    }


    /**
     * 添加通知
     * @return
     */
    @ResponseBody
    @RequestMapping("/insert")
    public BaseResponse insertNotice(String notice_title,String notice_content,String notice_user,String notice_type,String notice_class){
        Notice notice = new Notice();
        notice.setNotice_title(notice_title);
        notice.setNotice_content(notice_content);
        notice.setNotice_user(notice_user);
        notice.setNotice_type(notice_type);
        notice.setNotice_class(notice_class);
        notice.setNotice_time(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())));
        return noticeService.insertNotice(notice);
    }

    /**
     * 通知审核
     * @return
     */
    @ResponseBody
    @RequestMapping("/update")
    public BaseResponse updateNotice(String notice_id,String notice_state){

        return noticeService.updateNotice(notice_id,notice_state);
    }

    /**
     * 删除通知
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete")
    public BaseResponse deleteNotice(String notice_id){

        return noticeService.deleteNotice(notice_id);
    }

    /**
     * 搜索通知
     * @return
     */
    @ResponseBody
    @RequestMapping("/search")
    public BaseResponse<List<Notice>> searchNotice(String search,String searchtype){
        System.out.println("search:"+search);
        System.out.println("searchtype = " + searchtype);
        return noticeService.searchNotice(search,searchtype);
    }

    /**
     * 学生
     * 搜索该班级通知
     * @return
     */
    @ResponseBody
    @RequestMapping("/search/class")
    public BaseResponse<List<Notice>> searchNoticeClass(String search,Authentication authentication){
        System.out.println("search:"+search);
        return noticeService.searchNoticeClass(search,authentication.getName());
    }

    /**
     * 学生
     * 搜索通知公告
     * @return
     */
    @ResponseBody
    @RequestMapping("/stu/search")
    public BaseResponse<List<Notice>> stuSearchNotice(String search,String searchtype){
        System.out.println("search:"+search);
        System.out.println("searchtype = " + searchtype);
        return noticeService.stuSearchNotice(search,searchtype);
    }

    /**
     * 老师
     * 搜索该院系教工通知
     * @return
     */
    @ResponseBody
    @RequestMapping("/search/teacher")
    public BaseResponse<List<Notice>> searchNoticeTeacher(String search){
        System.out.println("search:"+search);
        return noticeService.searchNoticeTeacher(search);
    }

    /**
     * 老师
     * 查询自己班级通知
     * @return
     */
    @RequestMapping("/teacher/select/class")
    @ResponseBody
    public BaseResponse<List<Notice>> TeacherselectClassNotice(Authentication authentication){
        return noticeService.TeacherselectClassNotice(authentication.getName());
    }

    /**
     * 老师
     * 搜索该班级通知
     * @return
     */
    @ResponseBody
    @RequestMapping("/teacher/search/class")
    public BaseResponse<List<Notice>> teacherSearchClassNotice(String search,Authentication authentication){
        System.out.println("search:"+search);
        return noticeService.teacherSearchClassNotice(search,authentication.getName());
    }


}
