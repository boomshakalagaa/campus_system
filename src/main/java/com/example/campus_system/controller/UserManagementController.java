package com.example.campus_system.controller;


import com.example.campus_system.entity.BaseResponse;
import com.example.campus_system.entity.StudentInfo;
import com.example.campus_system.entity.TeacherInfo;
import com.example.campus_system.service.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserManagementController {

    @Autowired
    UserManagementService userManagementService;

    /**
     * 查询所有学生
     * @return
     */
    @ResponseBody
    @RequestMapping("/select/stu/all")
    public BaseResponse<List<StudentInfo>> selectStudentAll(){
        return userManagementService.selectStudentAll();
    }

    /**
     * 搜索学生 id，院系
     */
    @ResponseBody
    @RequestMapping("/search/stu")
    public BaseResponse<List<StudentInfo>> searchStudent(String stu_id,String stu_faculty){
        return userManagementService.searchStudent(stu_id,stu_faculty);
    }

    /**
     * 添加学生
     */
    @ResponseBody
    @RequestMapping("/add/stu")
    public BaseResponse addStudent(String stu_name,String stu_sex,String stu_age
            ,String stu_address,String stu_birth,String stu_phone,String stu_email,String stu_faculty
            ,String stu_major,String stu_class){

        return userManagementService.addStudent( stu_name, stu_sex,stu_age, stu_address, stu_birth, stu_phone, stu_email, stu_faculty, stu_major, stu_class);
    }

    /**
     * 修改学生信息
     */
    @ResponseBody
    @RequestMapping("/update/stu")
    public BaseResponse updateStudent(String stu_id,String stu_name,String stu_sex,String stu_age
            ,String stu_address,String stu_birth,String stu_phone,String stu_email,String stu_faculty
            ,String stu_major,String stu_class){
        System.out.println("stu_id = " + stu_id);
        return userManagementService.updateStudent(stu_id, stu_name, stu_sex, stu_age, stu_address
                ,stu_birth, stu_phone, stu_email, stu_faculty, stu_major, stu_class);
    }

    /**
     * 删除学生信息
     */
    @ResponseBody
    @RequestMapping("/delete/stu")
    public BaseResponse deleteStudent(String stu_id){
        return userManagementService.deleteStudent(stu_id);
    }

    /**
     * 重置密码
     */
    @ResponseBody
    @RequestMapping("/reset")
    public BaseResponse resetPassword(String user_id){
        return userManagementService.resetPassword(user_id);
    }

    /**
     * 查询所有老师
     * @return
     */
    @ResponseBody
    @RequestMapping("/select/teacher/all")
    public BaseResponse<List<TeacherInfo>> selectTeacherAll(){
        return userManagementService.selectTeacherAll();
    }

    /**
     * 搜索老师 id，院系
     */
    @ResponseBody
    @RequestMapping("/search/teacher")
    public BaseResponse<List<TeacherInfo>> searchTeacher(String teacher_id,String teacher_faculty){
        return userManagementService.searchTeacher(teacher_id,teacher_faculty);
    }

    /**
     * 添加教师或管理员
     */
    @ResponseBody
    @RequestMapping("/add/teacher")
    public BaseResponse addTeacher(String teacher_name,String teacher_sex,String teacher_age
            ,String teacher_address,String teacher_birth,String teacher_phone,String teacher_email,String teacher_faculty,String role_id){
        System.out.println("teacher_name = " + teacher_name);
        return userManagementService.addTeacher( teacher_name, teacher_sex,teacher_age, teacher_address
                , teacher_birth, teacher_phone, teacher_email, teacher_faculty,role_id);
    }

    /**
     * 修改教师信息
     */
    @ResponseBody
    @RequestMapping("/update/teacher")
    public BaseResponse updateTeacher(String teacher_id,String teacher_name,String teacher_sex,String teacher_age
            ,String teacher_address,String teacher_birth,String teacher_phone,String teacher_email,String teacher_faculty,String role_id){

        return userManagementService.updateTeacher( teacher_id,teacher_name, teacher_sex, teacher_age, teacher_address
                ,teacher_birth, teacher_phone, teacher_email, teacher_faculty, role_id);
    }

    /**
     * 删除教师信息
     */
    @ResponseBody
    @RequestMapping("/delete/teacher")
    public BaseResponse deleteTeacher(String teacher_id){
        return userManagementService.deleteTeacher(teacher_id);
    }
}
