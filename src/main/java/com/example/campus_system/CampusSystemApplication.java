package com.example.campus_system;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@MapperScan("com.example.campus_system.mapper")
@SpringBootApplication
//开启事务
@EnableTransactionManagement
public class CampusSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(CampusSystemApplication.class, args);
    }

}
