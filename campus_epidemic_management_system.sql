/*
Navicat MySQL Data Transfer

Source Server         : my_sql
Source Server Version : 50731
Source Host           : localhost:3306
Source Database       : campus_epidemic_management_system

Target Server Type    : MYSQL
Target Server Version : 50731
File Encoding         : 65001

Date: 2022-12-02 14:43:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `account`
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `user_id` int(20) NOT NULL AUTO_INCREMENT,
  `user_pwd` varchar(250) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of account
-- ----------------------------
INSERT INTO `account` VALUES ('1', '$2a$10$RLyE23G43F6zdHhI2SInAOj7CtwmKqexYUI/CSL/UMfv5lYQ/YHzC');
INSERT INTO `account` VALUES ('2', '$2a$10$NfSwBOcNnqk48jTx.Cwavu7Tqo0N9MXDrYFUb0hE8t48xxzcQISAK');
INSERT INTO `account` VALUES ('3', '$2a$10$ys0tLe7RDTxH4afuXPx0Qe5GsQTnzb9jkyJzd4lSuvJ4cP6thfLOK');
INSERT INTO `account` VALUES ('4', '$2a$10$ys0tLe7RDTxH4afuXPx0Qe5GsQTnzb9jkyJzd4lSuvJ4cP6thfLOK');
INSERT INTO `account` VALUES ('19', '$2a$10$FoCQDPdIsG5NvZMMxlIAae8lFLoWS15E5rqZINkfhuRHHnbmmSLpm');
INSERT INTO `account` VALUES ('25', '$2a$10$FoCQDPdIsG5NvZMMxlIAae8lFLoWS15E5rqZINkfhuRHHnbmmSLpm');
INSERT INTO `account` VALUES ('26', '$2a$10$FoCQDPdIsG5NvZMMxlIAae8lFLoWS15E5rqZINkfhuRHHnbmmSLpm');
INSERT INTO `account` VALUES ('30', '$2a$10$0YlUcTI/kTOltE/8pYRC5.CLcM0e05UFEmLcelXPqeU1ZarCYIfb.');
INSERT INTO `account` VALUES ('41', '$2a$10$9rJAZYSOU6VyXATFk1Pep.3CsWc1aBUtY4g9Rw07qdx54xyPSzDpS');
INSERT INTO `account` VALUES ('42', '$2a$10$aJ4HEVzg9W7fcdiqfmTmjO3M.jQ89kKBpPCdvxCQRFajjMl5YpMOC');

-- ----------------------------
-- Table structure for `class_info`
-- ----------------------------
DROP TABLE IF EXISTS `class_info`;
CREATE TABLE `class_info` (
  `class` varchar(20) NOT NULL,
  `class_grade` varchar(20) NOT NULL,
  `teacher_id` varchar(20) NOT NULL,
  `class_major` varchar(20) NOT NULL,
  `class_faculty` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of class_info
-- ----------------------------
INSERT INTO `class_info` VALUES ('软工183', '18', '25', '软件工程', '计算机科学与技术学院');
INSERT INTO `class_info` VALUES ('英语181', '18', '26', '英语', '外国语学院');

-- ----------------------------
-- Table structure for `clock_state`
-- ----------------------------
DROP TABLE IF EXISTS `clock_state`;
CREATE TABLE `clock_state` (
  `clock_id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `user_faculty` varchar(20) DEFAULT NULL,
  `user_class` varchar(20) DEFAULT '无',
  `clock_time` date NOT NULL,
  `clock_time_type` varchar(20) NOT NULL,
  `clock_area` varchar(20) NOT NULL,
  `temperature` varchar(20) NOT NULL,
  `cough` varchar(20) NOT NULL,
  `fever` varchar(20) NOT NULL,
  PRIMARY KEY (`clock_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of clock_state
-- ----------------------------
INSERT INTO `clock_state` VALUES ('1', '2', '张三', '计算机科学与技术学院', '软工183', '2022-04-04', '早上', '北京大学', '36.6', '无', '无');
INSERT INTO `clock_state` VALUES ('4', '3', '李四', '计算机科学与技术学院', '软工183', '2022-04-04', '早上', '北京大学', '37.1', '有', '无');
INSERT INTO `clock_state` VALUES ('7', '4', '麻子', '外国语学院', '英语181', '2022-04-04', '早上', '北京大学', '37.5', '有', '有');
INSERT INTO `clock_state` VALUES ('10', '1', '王五', '管理员', '无', '2022-04-04', '早上', '北京大学', '36.6', '无', '无');
INSERT INTO `clock_state` VALUES ('13', '1', '王五', '管理员', '无', '2022-04-05', '中午', '北京大学', '36.5', '无', '无');
INSERT INTO `clock_state` VALUES ('16', '1', '王五', '管理员', '无', '2022-04-10', '晚上', '北京大学', '36.6', '有', '无');
INSERT INTO `clock_state` VALUES ('17', '1', '王五', '管理员', '无', '2022-04-12', '晚上', '北京大学', '36.5', '有', '无');
INSERT INTO `clock_state` VALUES ('18', '1', '王五', '管理员', '无', '2022-04-16', '晚上', '北京大学', '38.8', '无', '有');
INSERT INTO `clock_state` VALUES ('19', '2', '张三', '计算机科学与技术学院', '软工183', '2022-04-16', '晚上', '北京大学', '36.5', '有', '无');
INSERT INTO `clock_state` VALUES ('20', '25', '毛老师', '计算机科学与技术学院', '无', '2022-04-17', '晚上', '北京大学', '36.6', '无', '无');
INSERT INTO `clock_state` VALUES ('21', '1', '王五', '管理员', '无', '2022-04-20', '中午', '北京大学', '38.2', '有', '有');
INSERT INTO `clock_state` VALUES ('22', '2', '张三', '计算机科学与技术学院', '软工183', '2022-04-20', '中午', '北京大学', '36.6', '无', '无');
INSERT INTO `clock_state` VALUES ('23', '1', '王五', '管理员', '无', '2022-04-21', '早上', '北京市', '36.6', '有', '无');
INSERT INTO `clock_state` VALUES ('24', '1', '王五', '管理员', '无', '2022-06-06', '早上', '北京市', '36.6', '有', '无');
INSERT INTO `clock_state` VALUES ('25', '4', '麻子', '外国语学院', '英语181', '2022-06-06', '早上', '北京大学', '37.5', '有', '有');
INSERT INTO `clock_state` VALUES ('26', '2', '张三', '计算机科学与技术学院', '软工183', '2022-06-06', '早上', '北京大学', '36.6', '无', '无');
INSERT INTO `clock_state` VALUES ('27', '1', '王五', '管理员', '无', '2022-06-18', '早上', '北京市', '37.5', '有', '无');
INSERT INTO `clock_state` VALUES ('28', '1', '王五', '管理员', '无', '2022-06-18', '中午', '北京市', '37.5', '无', '无');
INSERT INTO `clock_state` VALUES ('29', '1', '王五', '管理员', '无', '2022-06-18', '早上', '北京市', '38', '有', '有');
INSERT INTO `clock_state` VALUES ('30', '1', '王五', '管理员', '无', '2022-07-23', '晚上', '北京市', '36.6', '无', '无');

-- ----------------------------
-- Table structure for `c_data`
-- ----------------------------
DROP TABLE IF EXISTS `c_data`;
CREATE TABLE `c_data` (
  `p_name` varchar(20) NOT NULL,
  `c_name` varchar(20) NOT NULL,
  `c_total_nowConfirm` varchar(20) NOT NULL,
  `c_total_wzz` varchar(20) NOT NULL,
  `c_total_confirm` varchar(20) NOT NULL,
  `c_total_dead` varchar(20) NOT NULL,
  `c_total_heal` varchar(20) NOT NULL,
  `c_today_confirm` varchar(20) NOT NULL,
  PRIMARY KEY (`c_name`,`p_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of c_data
-- ----------------------------
INSERT INTO `c_data` VALUES ('黑龙江', '七台河', '0', '0', '18', '0', '18', '0');
INSERT INTO `c_data` VALUES ('海南', '万宁', '1', '0', '14', '0', '13', '0');
INSERT INTO `c_data` VALUES ('重庆', '万州区', '0', '0', '118', '4', '114', '0');
INSERT INTO `c_data` VALUES ('重庆', '万盛经开区', '0', '0', '1', '0', '1', '0');
INSERT INTO `c_data` VALUES ('海南', '三亚', '71', '0', '129', '1', '57', '0');
INSERT INTO `c_data` VALUES ('福建', '三明', '1', '0', '15', '0', '14', '0');
INSERT INTO `c_data` VALUES ('海南', '三沙', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('河南', '三门峡', '0', '0', '8', '1', '7', '0');
INSERT INTO `c_data` VALUES ('江西', '上饶', '2', '0', '328', '0', '326', '0');
INSERT INTO `c_data` VALUES ('天津', '东丽区', '4', '0', '36', '0', '32', '0');
INSERT INTO `c_data` VALUES ('北京', '东城', '60', '0', '66', '0', '6', '0');
INSERT INTO `c_data` VALUES ('海南', '东方', '0', '0', '3', '1', '2', '0');
INSERT INTO `c_data` VALUES ('广东', '东莞', '0', '0', '348', '1', '347', '0');
INSERT INTO `c_data` VALUES ('山东', '东营', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('重庆', '两江新区', '0', '0', '18', '0', '18', '0');
INSERT INTO `c_data` VALUES ('宁夏', '中卫', '0', '0', '4', '0', '4', '0');
INSERT INTO `c_data` VALUES ('广东', '中山', '13', '0', '84', '0', '71', '0');
INSERT INTO `c_data` VALUES ('北京', '丰台', '461', '0', '698', '0', '237', '0');
INSERT INTO `c_data` VALUES ('重庆', '丰都县', '0', '0', '10', '0', '10', '0');
INSERT INTO `c_data` VALUES ('甘肃', '临夏', '0', '0', '3', '0', '3', '0');
INSERT INTO `c_data` VALUES ('山西', '临汾', '0', '0', '3', '0', '3', '0');
INSERT INTO `c_data` VALUES ('山东', '临沂', '4', '0', '66', '0', '62', '0');
INSERT INTO `c_data` VALUES ('云南', '临沧', '12', '0', '13', '0', '1', '0');
INSERT INTO `c_data` VALUES ('海南', '临高县', '0', '0', '6', '0', '6', '0');
INSERT INTO `c_data` VALUES ('辽宁', '丹东', '43', '0', '54', '0', '11', '0');
INSERT INTO `c_data` VALUES ('浙江', '丽水', '5', '0', '22', '0', '17', '0');
INSERT INTO `c_data` VALUES ('云南', '丽江市', '0', '0', '7', '0', '7', '0');
INSERT INTO `c_data` VALUES ('内蒙古', '乌兰察布', '0', '0', '3', '0', '3', '0');
INSERT INTO `c_data` VALUES ('内蒙古', '乌海', '0', '0', '2', '0', '2', '0');
INSERT INTO `c_data` VALUES ('新疆', '乌鲁木齐', '0', '0', '857', '0', '857', '0');
INSERT INTO `c_data` VALUES ('海南', '乐东', '0', '0', '2', '0', '2', '0');
INSERT INTO `c_data` VALUES ('四川', '乐山', '4', '0', '12', '0', '8', '0');
INSERT INTO `c_data` VALUES ('江西', '九江', '0', '0', '121', '0', '121', '0');
INSERT INTO `c_data` VALUES ('重庆', '九龙坡区', '0', '0', '23', '1', '22', '0');
INSERT INTO `c_data` VALUES ('广东', '云浮', '7', '0', '7', '0', '0', '0');
INSERT INTO `c_data` VALUES ('重庆', '云阳县', '0', '0', '28', '0', '28', '0');
INSERT INTO `c_data` VALUES ('安徽', '亳州', '0', '0', '108', '0', '108', '0');
INSERT INTO `c_data` VALUES ('湖北', '仙桃', '0', '0', '575', '22', '553', '0');
INSERT INTO `c_data` VALUES ('黑龙江', '伊春', '0', '0', '1', '0', '1', '0');
INSERT INTO `c_data` VALUES ('新疆', '伊犁哈萨克自治州', '0', '0', '31', '0', '31', '0');
INSERT INTO `c_data` VALUES ('新疆', '伊犁州', '0', '0', '31', '0', '31', '0');
INSERT INTO `c_data` VALUES ('广东', '佛山', '5', '0', '106', '0', '101', '0');
INSERT INTO `c_data` VALUES ('黑龙江', '佳木斯', '0', '0', '56', '0', '56', '0');
INSERT INTO `c_data` VALUES ('海南', '保亭', '0', '0', '3', '0', '3', '0');
INSERT INTO `c_data` VALUES ('河北', '保定', '18', '0', '56', '0', '38', '0');
INSERT INTO `c_data` VALUES ('云南', '保山市', '2', '0', '11', '0', '9', '0');
INSERT INTO `c_data` VALUES ('河南', '信阳', '11', '0', '288', '2', '275', '0');
INSERT INTO `c_data` VALUES ('海南', '儋州', '2', '0', '17', '1', '14', '0');
INSERT INTO `c_data` VALUES ('新疆', '克孜州', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('新疆', '克拉玛依', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('吉林', '公主岭', '0', '0', '6', '0', '6', '0');
INSERT INTO `c_data` VALUES ('安徽', '六安', '0', '0', '84', '0', '84', '0');
INSERT INTO `c_data` VALUES ('新疆', '六师五家渠', '0', '0', '2', '0', '2', '0');
INSERT INTO `c_data` VALUES ('贵州', '六盘水', '0', '0', '10', '1', '9', '0');
INSERT INTO `c_data` VALUES ('甘肃', '兰州', '0', '0', '355', '2', '353', '0');
INSERT INTO `c_data` VALUES ('内蒙古', '兴安盟', '0', '0', '3', '0', '3', '0');
INSERT INTO `c_data` VALUES ('新疆', '兵团第九师', '0', '0', '4', '1', '3', '0');
INSERT INTO `c_data` VALUES ('新疆', '兵团第十二师', '0', '0', '3', '0', '3', '0');
INSERT INTO `c_data` VALUES ('新疆', '兵团第四师', '0', '0', '13', '1', '12', '0');
INSERT INTO `c_data` VALUES ('四川', '内江', '0', '0', '22', '0', '22', '0');
INSERT INTO `c_data` VALUES ('四川', '凉山', '0', '0', '13', '0', '13', '0');
INSERT INTO `c_data` VALUES ('内蒙古', '包头', '0', '0', '39', '0', '39', '0');
INSERT INTO `c_data` VALUES ('广西', '北海', '0', '0', '45', '1', '44', '0');
INSERT INTO `c_data` VALUES ('重庆', '北碚区', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('天津', '北辰区', '10', '0', '33', '0', '23', '0');
INSERT INTO `c_data` VALUES ('湖北', '十堰', '0', '0', '673', '8', '665', '0');
INSERT INTO `c_data` VALUES ('江苏', '南京', '44', '0', '375', '0', '331', '0');
INSERT INTO `c_data` VALUES ('四川', '南充', '0', '0', '39', '0', '39', '0');
INSERT INTO `c_data` VALUES ('广西', '南宁', '0', '0', '60', '0', '60', '0');
INSERT INTO `c_data` VALUES ('重庆', '南岸区', '0', '0', '18', '0', '18', '0');
INSERT INTO `c_data` VALUES ('重庆', '南川区', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('福建', '南平', '3', '0', '23', '0', '20', '0');
INSERT INTO `c_data` VALUES ('天津', '南开区', '5', '0', '42', '0', '37', '0');
INSERT INTO `c_data` VALUES ('江西', '南昌', '0', '0', '455', '0', '455', '0');
INSERT INTO `c_data` VALUES ('江苏', '南通', '1', '0', '41', '0', '40', '0');
INSERT INTO `c_data` VALUES ('河南', '南阳', '0', '0', '156', '3', '153', '0');
INSERT INTO `c_data` VALUES ('新疆', '博尔塔拉州', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('福建', '厦门', '36', '0', '321', '0', '285', '0');
INSERT INTO `c_data` VALUES ('黑龙江', '双鸭山', '0', '0', '52', '3', '49', '0');
INSERT INTO `c_data` VALUES ('浙江', '台州', '2', '0', '149', '0', '147', '0');
INSERT INTO `c_data` VALUES ('重庆', '合川区', '0', '0', '23', '0', '23', '0');
INSERT INTO `c_data` VALUES ('安徽', '合肥', '0', '0', '178', '1', '177', '0');
INSERT INTO `c_data` VALUES ('江西', '吉安', '0', '0', '23', '0', '23', '0');
INSERT INTO `c_data` VALUES ('吉林', '吉林', '128', '0', '14096', '1', '13967', '1');
INSERT INTO `c_data` VALUES ('吉林', '吉林市', '3', '0', '14097', '1', '14093', '0');
INSERT INTO `c_data` VALUES ('新疆', '吐鲁番', '0', '0', '3', '0', '3', '0');
INSERT INTO `c_data` VALUES ('山西', '吕梁', '0', '0', '6', '0', '6', '0');
INSERT INTO `c_data` VALUES ('宁夏', '吴忠', '0', '0', '39', '0', '39', '0');
INSERT INTO `c_data` VALUES ('河南', '周口', '82', '0', '183', '1', '100', '0');
INSERT INTO `c_data` VALUES ('内蒙古', '呼伦贝尔', '0', '0', '658', '0', '658', '0');
INSERT INTO `c_data` VALUES ('内蒙古', '呼和浩特', '0', '0', '446', '0', '446', '0');
INSERT INTO `c_data` VALUES ('天津', '和平区', '7', '0', '13', '0', '6', '0');
INSERT INTO `c_data` VALUES ('新疆', '和田', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('湖北', '咸宁', '0', '0', '836', '15', '821', '0');
INSERT INTO `c_data` VALUES ('陕西', '咸阳', '0', '0', '35', '0', '35', '0');
INSERT INTO `c_data` VALUES ('新疆', '哈密', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('黑龙江', '哈尔滨', '0', '0', '1341', '4', '1337', '0');
INSERT INTO `c_data` VALUES ('新疆', '哈萨克自治州', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('河北', '唐山', '20', '0', '96', '1', '75', '0');
INSERT INTO `c_data` VALUES ('河南', '商丘', '3', '0', '112', '3', '106', '0');
INSERT INTO `c_data` VALUES ('陕西', '商洛', '0', '0', '8', '0', '8', '0');
INSERT INTO `c_data` VALUES ('新疆', '喀什', '0', '0', '80', '0', '80', '0');
INSERT INTO `c_data` VALUES ('浙江', '嘉兴', '130', '0', '176', '0', '46', '0');
INSERT INTO `c_data` VALUES ('上海', '嘉定', '273', '0', '2633', '2', '2358', '0');
INSERT INTO `c_data` VALUES ('甘肃', '嘉峪关', '0', '0', '5', '0', '5', '0');
INSERT INTO `c_data` VALUES ('吉林', '四平', '1', '0', '244', '1', '242', '0');
INSERT INTO `c_data` VALUES ('宁夏', '固原', '0', '0', '5', '0', '5', '0');
INSERT INTO `c_data` VALUES ('上海', '地区待确认', '-9420', '0', '10', '588', '8842', '0');
INSERT INTO `c_data` VALUES ('云南', '地区待确认', '-72', '0', '0', '0', '72', '0');
INSERT INTO `c_data` VALUES ('北京', '地区待确认', '-1898', '0', '88', '0', '1986', '1');
INSERT INTO `c_data` VALUES ('台湾', '地区待确认', '2258261', '0', '2274666', '2663', '13742', '76505');
INSERT INTO `c_data` VALUES ('四川', '地区待确认', '-302', '0', '0', '0', '302', '0');
INSERT INTO `c_data` VALUES ('宁夏', '地区待确认', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('山东', '地区待确认', '-4', '0', '0', '0', '4', '0');
INSERT INTO `c_data` VALUES ('广东', '地区待确认', '-1116', '0', '0', '0', '1116', '0');
INSERT INTO `c_data` VALUES ('广西', '地区待确认', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('新疆', '地区待确认', '0', '0', '1', '0', '1', '0');
INSERT INTO `c_data` VALUES ('江苏', '地区待确认', '-271', '0', '0', '0', '271', '0');
INSERT INTO `c_data` VALUES ('江西', '地区待确认', '-2', '0', '0', '0', '2', '0');
INSERT INTO `c_data` VALUES ('河北', '地区待确认', '-299', '0', '0', '0', '299', '0');
INSERT INTO `c_data` VALUES ('河南', '地区待确认', '-390', '0', '0', '0', '390', '0');
INSERT INTO `c_data` VALUES ('浙江', '地区待确认', '-869', '0', '0', '0', '869', '0');
INSERT INTO `c_data` VALUES ('海南', '地区待确认', '-95', '0', '0', '0', '95', '0');
INSERT INTO `c_data` VALUES ('湖南', '地区待确认', '-65', '0', '0', '0', '65', '0');
INSERT INTO `c_data` VALUES ('澳门', '地区待确认', '1', '0', '83', '0', '82', '0');
INSERT INTO `c_data` VALUES ('甘肃', '地区待确认', '0', '0', '1', '0', '1', '0');
INSERT INTO `c_data` VALUES ('福建', '地区待确认', '-164', '0', '0', '0', '164', '0');
INSERT INTO `c_data` VALUES ('贵州', '地区待确认', '-1', '0', '0', '0', '1', '0');
INSERT INTO `c_data` VALUES ('辽宁', '地区待确认', '-277', '0', '0', '0', '277', '0');
INSERT INTO `c_data` VALUES ('陕西', '地区待确认', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('青海', '地区待确认', '-39', '0', '20', '0', '59', '0');
INSERT INTO `c_data` VALUES ('香港', '地区待确认', '260800', '0', '332619', '9382', '62437', '72');
INSERT INTO `c_data` VALUES ('黑龙江', '地区待确认', '0', '0', '2', '0', '2', '0');
INSERT INTO `c_data` VALUES ('重庆', '垫江县', '0', '0', '20', '0', '20', '0');
INSERT INTO `c_data` VALUES ('重庆', '城口县', '0', '0', '2', '0', '2', '0');
INSERT INTO `c_data` VALUES ('新疆', '塔城', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('上海', '境外来沪', '-1', '0', '1', '0', '2', '0');
INSERT INTO `c_data` VALUES ('上海', '境外输入', '19', '0', '4623', '0', '4604', '2');
INSERT INTO `c_data` VALUES ('云南', '境外输入', '18', '0', '1520', '0', '1502', '2');
INSERT INTO `c_data` VALUES ('内蒙古', '境外输入', '0', '0', '347', '0', '347', '0');
INSERT INTO `c_data` VALUES ('北京', '境外输入', '167', '0', '724', '0', '557', '4');
INSERT INTO `c_data` VALUES ('吉林', '境外输入', '38', '0', '73', '0', '35', '0');
INSERT INTO `c_data` VALUES ('四川', '境外输入', '96', '0', '1361', '0', '1265', '4');
INSERT INTO `c_data` VALUES ('天津', '境外输入', '0', '0', '616', '0', '616', '0');
INSERT INTO `c_data` VALUES ('宁夏', '境外输入', '0', '0', '4', '0', '4', '0');
INSERT INTO `c_data` VALUES ('安徽', '境外输入', '0', '0', '15', '0', '15', '0');
INSERT INTO `c_data` VALUES ('山东', '境外输入', '0', '0', '442', '0', '442', '0');
INSERT INTO `c_data` VALUES ('山西', '境外输入', '1', '0', '138', '0', '137', '0');
INSERT INTO `c_data` VALUES ('广东', '境外输入', '26', '0', '3916', '0', '3890', '4');
INSERT INTO `c_data` VALUES ('广西', '境外输入', '14', '0', '1004', '0', '990', '1');
INSERT INTO `c_data` VALUES ('江苏', '境外输入', '7', '0', '240', '0', '233', '0');
INSERT INTO `c_data` VALUES ('江西', '境外输入', '0', '0', '14', '0', '14', '0');
INSERT INTO `c_data` VALUES ('河北', '境外输入', '0', '0', '37', '0', '37', '0');
INSERT INTO `c_data` VALUES ('河南', '境外输入', '0', '0', '211', '0', '211', '0');
INSERT INTO `c_data` VALUES ('浙江', '境外输入', '224', '0', '598', '0', '374', '1');
INSERT INTO `c_data` VALUES ('海南', '境外输入', '0', '0', '19', '0', '19', '0');
INSERT INTO `c_data` VALUES ('湖北', '境外输入', '0', '0', '123', '0', '123', '0');
INSERT INTO `c_data` VALUES ('湖南', '境外输入', '0', '0', '133', '0', '133', '0');
INSERT INTO `c_data` VALUES ('甘肃', '境外输入', '0', '0', '184', '0', '184', '0');
INSERT INTO `c_data` VALUES ('福建', '境外输入', '64', '0', '1220', '0', '1156', '7');
INSERT INTO `c_data` VALUES ('贵州', '境外输入', '0', '0', '1', '0', '1', '0');
INSERT INTO `c_data` VALUES ('辽宁', '境外输入', '2', '0', '214', '0', '212', '0');
INSERT INTO `c_data` VALUES ('重庆', '境外输入', '3', '0', '50', '0', '47', '0');
INSERT INTO `c_data` VALUES ('陕西', '境外输入', '0', '0', '483', '0', '483', '0');
INSERT INTO `c_data` VALUES ('黑龙江', '境外输入', '1', '0', '436', '0', '435', '0');
INSERT INTO `c_data` VALUES ('北京', '外地来京', '23', '0', '25', '0', '2', '0');
INSERT INTO `c_data` VALUES ('上海', '外地来沪', '0', '0', '113', '1', '112', '0');
INSERT INTO `c_data` VALUES ('天津', '外地来津', '0', '0', '8', '0', '8', '0');
INSERT INTO `c_data` VALUES ('北京', '大兴', '51', '0', '162', '0', '111', '0');
INSERT INTO `c_data` VALUES ('黑龙江', '大兴安岭', '0', '0', '3', '0', '3', '0');
INSERT INTO `c_data` VALUES ('山西', '大同', '0', '0', '15', '0', '15', '0');
INSERT INTO `c_data` VALUES ('黑龙江', '大庆', '0', '0', '37', '1', '36', '0');
INSERT INTO `c_data` VALUES ('重庆', '大渡口区', '0', '0', '7', '0', '7', '0');
INSERT INTO `c_data` VALUES ('云南', '大理', '0', '0', '13', '0', '13', '0');
INSERT INTO `c_data` VALUES ('重庆', '大足区', '0', '0', '15', '0', '15', '0');
INSERT INTO `c_data` VALUES ('辽宁', '大连', '17', '0', '620', '0', '603', '0');
INSERT INTO `c_data` VALUES ('甘肃', '天水', '0', '0', '55', '0', '55', '0');
INSERT INTO `c_data` VALUES ('湖北', '天门', '0', '0', '498', '15', '483', '0');
INSERT INTO `c_data` VALUES ('山西', '太原', '0', '0', '109', '0', '109', '0');
INSERT INTO `c_data` VALUES ('重庆', '奉节县', '0', '0', '22', '0', '22', '0');
INSERT INTO `c_data` VALUES ('上海', '奉贤', '87', '0', '240', '0', '153', '0');
INSERT INTO `c_data` VALUES ('山东', '威海', '0', '0', '107', '1', '106', '0');
INSERT INTO `c_data` VALUES ('湖南', '娄底', '0', '0', '82', '0', '82', '0');
INSERT INTO `c_data` VALUES ('湖北', '孝感', '0', '0', '3518', '129', '3389', '0');
INSERT INTO `c_data` VALUES ('宁夏', '宁东管委会', '0', '0', '1', '0', '1', '0');
INSERT INTO `c_data` VALUES ('福建', '宁德', '51', '0', '77', '0', '26', '0');
INSERT INTO `c_data` VALUES ('天津', '宁河区', '0', '0', '6', '0', '6', '0');
INSERT INTO `c_data` VALUES ('浙江', '宁波', '61', '0', '330', '0', '269', '0');
INSERT INTO `c_data` VALUES ('安徽', '安庆', '0', '0', '83', '0', '83', '0');
INSERT INTO `c_data` VALUES ('陕西', '安康', '0', '0', '34', '0', '34', '0');
INSERT INTO `c_data` VALUES ('河南', '安阳', '33', '0', '555', '0', '522', '0');
INSERT INTO `c_data` VALUES ('贵州', '安顺', '0', '0', '5', '0', '5', '0');
INSERT INTO `c_data` VALUES ('海南', '定安县', '0', '0', '3', '1', '2', '0');
INSERT INTO `c_data` VALUES ('河北', '定州', '6', '0', '8', '0', '2', '0');
INSERT INTO `c_data` VALUES ('甘肃', '定西', '0', '0', '9', '0', '9', '0');
INSERT INTO `c_data` VALUES ('四川', '宜宾', '1', '0', '14', '0', '13', '0');
INSERT INTO `c_data` VALUES ('湖北', '宜昌', '0', '0', '932', '37', '895', '0');
INSERT INTO `c_data` VALUES ('江西', '宜春', '0', '0', '106', '0', '106', '0');
INSERT INTO `c_data` VALUES ('天津', '宝坻区', '0', '0', '66', '2', '64', '0');
INSERT INTO `c_data` VALUES ('上海', '宝山', '622', '0', '3083', '1', '2460', '0');
INSERT INTO `c_data` VALUES ('陕西', '宝鸡', '0', '0', '253', '0', '253', '0');
INSERT INTO `c_data` VALUES ('安徽', '宣城', '0', '0', '9', '0', '9', '0');
INSERT INTO `c_data` VALUES ('安徽', '宿州', '0', '0', '42', '0', '42', '0');
INSERT INTO `c_data` VALUES ('江苏', '宿迁', '13', '0', '29', '0', '16', '0');
INSERT INTO `c_data` VALUES ('北京', '密云', '2', '0', '9', '0', '7', '0');
INSERT INTO `c_data` VALUES ('西藏', '山南', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('湖南', '岳阳', '1', '0', '159', '1', '157', '0');
INSERT INTO `c_data` VALUES ('广西', '崇左', '0', '0', '2', '0', '2', '0');
INSERT INTO `c_data` VALUES ('上海', '崇明', '171', '0', '396', '0', '225', '0');
INSERT INTO `c_data` VALUES ('重庆', '巫山县', '0', '0', '11', '0', '11', '0');
INSERT INTO `c_data` VALUES ('重庆', '巫溪县', '0', '0', '14', '0', '14', '0');
INSERT INTO `c_data` VALUES ('四川', '巴中', '10', '0', '34', '0', '24', '0');
INSERT INTO `c_data` VALUES ('重庆', '巴南区', '0', '0', '13', '0', '13', '0');
INSERT INTO `c_data` VALUES ('新疆', '巴州', '0', '0', '3', '0', '3', '0');
INSERT INTO `c_data` VALUES ('内蒙古', '巴彦淖尔', '0', '0', '13', '1', '12', '0');
INSERT INTO `c_data` VALUES ('新疆', '巴音郭楞州', '0', '0', '3', '0', '3', '0');
INSERT INTO `c_data` VALUES ('江苏', '常州', '30', '0', '84', '0', '54', '0');
INSERT INTO `c_data` VALUES ('湖南', '常德', '0', '0', '82', '0', '82', '0');
INSERT INTO `c_data` VALUES ('甘肃', '平凉', '0', '0', '9', '0', '9', '0');
INSERT INTO `c_data` VALUES ('北京', '平谷区', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('河南', '平顶山', '12', '0', '71', '1', '58', '0');
INSERT INTO `c_data` VALUES ('四川', '广元', '1', '0', '7', '0', '6', '0');
INSERT INTO `c_data` VALUES ('四川', '广安', '224', '0', '255', '0', '31', '0');
INSERT INTO `c_data` VALUES ('广东', '广州', '339', '0', '949', '1', '609', '0');
INSERT INTO `c_data` VALUES ('甘肃', '庆阳', '0', '0', '3', '0', '3', '0');
INSERT INTO `c_data` VALUES ('河北', '廊坊', '140', '0', '315', '0', '175', '0');
INSERT INTO `c_data` VALUES ('陕西', '延安', '0', '0', '21', '0', '21', '0');
INSERT INTO `c_data` VALUES ('北京', '延庆', '2', '0', '2', '0', '0', '0');
INSERT INTO `c_data` VALUES ('吉林', '延边', '2', '0', '191', '0', '189', '0');
INSERT INTO `c_data` VALUES ('河南', '开封', '6', '0', '39', '0', '33', '0');
INSERT INTO `c_data` VALUES ('重庆', '开州区', '0', '0', '21', '1', '20', '0');
INSERT INTO `c_data` VALUES ('河北', '张家口', '0', '0', '43', '0', '43', '0');
INSERT INTO `c_data` VALUES ('湖南', '张家界', '0', '0', '77', '0', '77', '0');
INSERT INTO `c_data` VALUES ('甘肃', '张掖', '0', '0', '17', '0', '17', '0');
INSERT INTO `c_data` VALUES ('重庆', '彭水县', '0', '0', '9', '0', '9', '0');
INSERT INTO `c_data` VALUES ('吉林', '待确认', '-414', '0', '0', '2', '412', '0');
INSERT INTO `c_data` VALUES ('天津', '待确认', '81', '0', '103', '0', '22', '0');
INSERT INTO `c_data` VALUES ('江苏', '徐州', '74', '0', '153', '0', '79', '0');
INSERT INTO `c_data` VALUES ('上海', '徐汇', '1160', '0', '4677', '1', '3516', '1');
INSERT INTO `c_data` VALUES ('云南', '德宏州', '0', '0', '377', '0', '377', '0');
INSERT INTO `c_data` VALUES ('山东', '德州', '0', '0', '118', '2', '116', '0');
INSERT INTO `c_data` VALUES ('四川', '德阳', '2', '0', '21', '0', '19', '0');
INSERT INTO `c_data` VALUES ('重庆', '忠县', '0', '0', '21', '0', '21', '0');
INSERT INTO `c_data` VALUES ('山西', '忻州', '0', '0', '14', '0', '14', '0');
INSERT INTO `c_data` VALUES ('湖南', '怀化', '1', '0', '65', '0', '64', '0');
INSERT INTO `c_data` VALUES ('北京', '怀柔', '0', '0', '8', '0', '8', '0');
INSERT INTO `c_data` VALUES ('云南', '怒江州', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('湖北', '恩施州', '0', '0', '253', '7', '246', '0');
INSERT INTO `c_data` VALUES ('广东', '惠州', '8', '0', '70', '0', '62', '0');
INSERT INTO `c_data` VALUES ('四川', '成都', '72', '0', '323', '3', '248', '0');
INSERT INTO `c_data` VALUES ('北京', '房山', '319', '0', '331', '0', '12', '0');
INSERT INTO `c_data` VALUES ('江苏', '扬州', '1', '0', '594', '0', '593', '0');
INSERT INTO `c_data` VALUES ('河北', '承德', '1', '0', '8', '0', '7', '0');
INSERT INTO `c_data` VALUES ('江西', '抚州', '0', '0', '73', '0', '73', '0');
INSERT INTO `c_data` VALUES ('辽宁', '抚顺', '0', '0', '1', '0', '1', '0');
INSERT INTO `c_data` VALUES ('西藏', '拉萨', '0', '0', '1', '0', '1', '0');
INSERT INTO `c_data` VALUES ('广东', '揭阳', '0', '0', '10', '0', '10', '0');
INSERT INTO `c_data` VALUES ('四川', '攀枝花', '0', '0', '16', '0', '16', '0');
INSERT INTO `c_data` VALUES ('云南', '文山州', '8', '0', '13', '0', '5', '0');
INSERT INTO `c_data` VALUES ('海南', '文昌', '0', '0', '3', '0', '3', '0');
INSERT INTO `c_data` VALUES ('河南', '新乡', '0', '0', '57', '3', '54', '0');
INSERT INTO `c_data` VALUES ('江西', '新余', '0', '0', '129', '0', '129', '0');
INSERT INTO `c_data` VALUES ('江苏', '无锡', '19', '0', '74', '0', '55', '0');
INSERT INTO `c_data` VALUES ('西藏', '日喀则', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('山东', '日照', '0', '0', '37', '0', '37', '0');
INSERT INTO `c_data` VALUES ('云南', '昆明', '6', '0', '68', '0', '62', '0');
INSERT INTO `c_data` VALUES ('新疆', '昌吉州', '0', '0', '5', '0', '5', '0');
INSERT INTO `c_data` VALUES ('北京', '昌平', '44', '0', '94', '0', '50', '2');
INSERT INTO `c_data` VALUES ('海南', '昌江县', '0', '0', '7', '0', '7', '0');
INSERT INTO `c_data` VALUES ('西藏', '昌都', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('云南', '昭通市', '1', '0', '26', '0', '25', '0');
INSERT INTO `c_data` VALUES ('山西', '晋中', '0', '0', '59', '0', '59', '0');
INSERT INTO `c_data` VALUES ('山西', '晋城', '0', '0', '11', '0', '11', '0');
INSERT INTO `c_data` VALUES ('云南', '普洱', '8', '0', '12', '0', '4', '0');
INSERT INTO `c_data` VALUES ('上海', '普陀', '494', '0', '1785', '0', '1291', '0');
INSERT INTO `c_data` VALUES ('江西', '景德镇', '0', '0', '6', '0', '6', '0');
INSERT INTO `c_data` VALUES ('云南', '曲靖', '2', '0', '15', '0', '13', '0');
INSERT INTO `c_data` VALUES ('山西', '朔州', '0', '0', '20', '0', '20', '0');
INSERT INTO `c_data` VALUES ('北京', '朝阳', '489', '0', '510', '0', '21', '2');
INSERT INTO `c_data` VALUES ('辽宁', '朝阳市', '0', '0', '7', '1', '6', '0');
INSERT INTO `c_data` VALUES ('辽宁', '本溪', '0', '0', '4', '0', '4', '0');
INSERT INTO `c_data` VALUES ('广西', '来宾', '0', '0', '11', '0', '11', '0');
INSERT INTO `c_data` VALUES ('陕西', '杨凌', '0', '0', '3', '0', '3', '0');
INSERT INTO `c_data` VALUES ('上海', '杨浦', '1188', '0', '2382', '0', '1194', '2');
INSERT INTO `c_data` VALUES ('浙江', '杭州', '202', '0', '530', '0', '328', '0');
INSERT INTO `c_data` VALUES ('吉林', '松原', '0', '0', '18', '0', '18', '0');
INSERT INTO `c_data` VALUES ('上海', '松江', '80', '0', '2974', '0', '2894', '0');
INSERT INTO `c_data` VALUES ('西藏', '林芝', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('青海', '果洛州', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('山东', '枣庄', '0', '0', '33', '0', '33', '0');
INSERT INTO `c_data` VALUES ('广西', '柳州', '0', '0', '24', '0', '24', '0');
INSERT INTO `c_data` VALUES ('湖南', '株洲', '0', '0', '110', '0', '110', '0');
INSERT INTO `c_data` VALUES ('广西', '桂林', '0', '0', '34', '0', '34', '0');
INSERT INTO `c_data` VALUES ('重庆', '梁平区', '0', '0', '4', '0', '4', '0');
INSERT INTO `c_data` VALUES ('广东', '梅州', '1', '0', '18', '0', '17', '0');
INSERT INTO `c_data` VALUES ('吉林', '梅河口市', '0', '0', '1', '0', '1', '0');
INSERT INTO `c_data` VALUES ('广西', '梧州', '0', '0', '5', '0', '5', '0');
INSERT INTO `c_data` VALUES ('云南', '楚雄州', '0', '0', '4', '0', '4', '0');
INSERT INTO `c_data` VALUES ('陕西', '榆林', '0', '0', '3', '0', '3', '0');
INSERT INTO `c_data` VALUES ('甘肃', '武威', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('湖北', '武汉', '1', '0', '50424', '3869', '46554', '0');
INSERT INTO `c_data` VALUES ('天津', '武清区', '0', '0', '151', '0', '151', '0');
INSERT INTO `c_data` VALUES ('重庆', '武隆区', '0', '0', '1', '0', '1', '0');
INSERT INTO `c_data` VALUES ('贵州', '毕节', '0', '0', '23', '0', '23', '0');
INSERT INTO `c_data` VALUES ('重庆', '永川区', '0', '0', '9', '0', '9', '0');
INSERT INTO `c_data` VALUES ('湖南', '永州', '1', '0', '45', '0', '44', '0');
INSERT INTO `c_data` VALUES ('陕西', '汉中', '0', '0', '57', '0', '57', '0');
INSERT INTO `c_data` VALUES ('广东', '汕头', '1', '0', '26', '0', '25', '0');
INSERT INTO `c_data` VALUES ('广东', '汕尾', '2', '0', '8', '0', '6', '0');
INSERT INTO `c_data` VALUES ('重庆', '江北区', '0', '0', '29', '0', '29', '0');
INSERT INTO `c_data` VALUES ('重庆', '江津区', '0', '0', '7', '0', '7', '0');
INSERT INTO `c_data` VALUES ('广东', '江门', '5', '0', '28', '0', '23', '0');
INSERT INTO `c_data` VALUES ('安徽', '池州', '0', '0', '21', '0', '21', '0');
INSERT INTO `c_data` VALUES ('辽宁', '沈阳', '95', '0', '246', '0', '151', '0');
INSERT INTO `c_data` VALUES ('重庆', '沙坪坝区', '0', '0', '10', '0', '10', '0');
INSERT INTO `c_data` VALUES ('河北', '沧州', '77', '0', '138', '3', '58', '0');
INSERT INTO `c_data` VALUES ('天津', '河东区', '2', '0', '36', '1', '33', '0');
INSERT INTO `c_data` VALUES ('天津', '河北区', '24', '0', '119', '0', '95', '0');
INSERT INTO `c_data` VALUES ('广西', '河池', '0', '0', '30', '1', '29', '0');
INSERT INTO `c_data` VALUES ('广东', '河源', '2', '0', '7', '0', '5', '0');
INSERT INTO `c_data` VALUES ('天津', '河西区', '5', '0', '33', '0', '28', '0');
INSERT INTO `c_data` VALUES ('福建', '泉州', '8', '0', '1187', '0', '1179', '0');
INSERT INTO `c_data` VALUES ('山东', '泰安', '1', '0', '47', '2', '44', '0');
INSERT INTO `c_data` VALUES ('江苏', '泰州', '10', '0', '47', '0', '37', '0');
INSERT INTO `c_data` VALUES ('四川', '泸州', '0', '0', '25', '0', '25', '0');
INSERT INTO `c_data` VALUES ('河南', '洛阳', '4', '0', '45', '1', '40', '0');
INSERT INTO `c_data` VALUES ('天津', '津南区', '3', '0', '367', '0', '364', '0');
INSERT INTO `c_data` VALUES ('山东', '济南', '0', '0', '135', '0', '135', '0');
INSERT INTO `c_data` VALUES ('山东', '济宁', '0', '0', '263', '0', '263', '0');
INSERT INTO `c_data` VALUES ('河南', '济源示范区', '0', '0', '5', '0', '5', '0');
INSERT INTO `c_data` VALUES ('上海', '浦东', '1208', '0', '17111', '1', '15902', '1');
INSERT INTO `c_data` VALUES ('青海', '海东', '1', '0', '5', '0', '4', '0');
INSERT INTO `c_data` VALUES ('青海', '海北州', '0', '0', '3', '0', '3', '0');
INSERT INTO `c_data` VALUES ('青海', '海南州', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('海南', '海口', '10', '0', '51', '0', '41', '0');
INSERT INTO `c_data` VALUES ('北京', '海淀', '317', '0', '343', '0', '26', '0');
INSERT INTO `c_data` VALUES ('青海', '海西州', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('北京', '涉奥闭环人员', '4', '0', '4', '0', '0', '0');
INSERT INTO `c_data` VALUES ('重庆', '涪陵区', '0', '0', '5', '0', '5', '0');
INSERT INTO `c_data` VALUES ('山东', '淄博', '0', '0', '77', '1', '76', '0');
INSERT INTO `c_data` VALUES ('安徽', '淮北', '0', '0', '28', '0', '28', '0');
INSERT INTO `c_data` VALUES ('安徽', '淮南', '0', '0', '36', '0', '36', '0');
INSERT INTO `c_data` VALUES ('江苏', '淮安', '5', '0', '83', '0', '78', '0');
INSERT INTO `c_data` VALUES ('广东', '深圳', '604', '0', '1385', '3', '778', '0');
INSERT INTO `c_data` VALUES ('广东', '清远', '0', '0', '12', '0', '12', '0');
INSERT INTO `c_data` VALUES ('重庆', '渝中区', '0', '0', '20', '0', '20', '0');
INSERT INTO `c_data` VALUES ('重庆', '渝北区', '0', '0', '21', '0', '21', '0');
INSERT INTO `c_data` VALUES ('浙江', '温州', '17', '0', '521', '1', '503', '0');
INSERT INTO `c_data` VALUES ('陕西', '渭南', '0', '0', '22', '0', '22', '0');
INSERT INTO `c_data` VALUES ('浙江', '湖州', '27', '0', '37', '0', '10', '0');
INSERT INTO `c_data` VALUES ('湖南', '湘潭', '2', '0', '41', '0', '39', '0');
INSERT INTO `c_data` VALUES ('湖南', '湘西自治州', '3', '0', '13', '0', '10', '0');
INSERT INTO `c_data` VALUES ('广东', '湛江', '88', '0', '111', '0', '23', '0');
INSERT INTO `c_data` VALUES ('安徽', '滁州', '0', '0', '15', '0', '15', '0');
INSERT INTO `c_data` VALUES ('山东', '滨州', '0', '0', '217', '0', '217', '0');
INSERT INTO `c_data` VALUES ('天津', '滨海新区', '2', '0', '88', '0', '86', '0');
INSERT INTO `c_data` VALUES ('河南', '漯河', '8', '0', '44', '0', '36', '0');
INSERT INTO `c_data` VALUES ('福建', '漳州', '17', '0', '40', '0', '23', '0');
INSERT INTO `c_data` VALUES ('山东', '潍坊', '0', '0', '52', '0', '52', '0');
INSERT INTO `c_data` VALUES ('湖北', '潜江', '0', '0', '198', '9', '189', '0');
INSERT INTO `c_data` VALUES ('广东', '潮州', '0', '0', '5', '0', '5', '0');
INSERT INTO `c_data` VALUES ('重庆', '潼南区', '0', '0', '18', '0', '18', '0');
INSERT INTO `c_data` VALUES ('海南', '澄迈县', '0', '0', '9', '1', '8', '0');
INSERT INTO `c_data` VALUES ('河南', '濮阳', '10', '0', '27', '0', '17', '0');
INSERT INTO `c_data` VALUES ('山东', '烟台', '0', '0', '91', '0', '91', '0');
INSERT INTO `c_data` VALUES ('河南', '焦作', '0', '0', '46', '1', '45', '0');
INSERT INTO `c_data` VALUES ('黑龙江', '牡丹江', '0', '0', '100', '0', '100', '0');
INSERT INTO `c_data` VALUES ('广西', '玉林', '0', '0', '12', '0', '12', '0');
INSERT INTO `c_data` VALUES ('青海', '玉树州', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('云南', '玉溪', '0', '0', '14', '1', '13', '0');
INSERT INTO `c_data` VALUES ('广东', '珠海', '30', '0', '146', '1', '115', '0');
INSERT INTO `c_data` VALUES ('海南', '琼中县', '0', '0', '1', '0', '1', '0');
INSERT INTO `c_data` VALUES ('海南', '琼海', '5', '0', '11', '1', '5', '0');
INSERT INTO `c_data` VALUES ('重庆', '璧山区', '0', '0', '9', '0', '9', '0');
INSERT INTO `c_data` VALUES ('甘肃', '甘南州', '0', '0', '8', '0', '8', '0');
INSERT INTO `c_data` VALUES ('四川', '甘孜', '0', '0', '78', '0', '78', '0');
INSERT INTO `c_data` VALUES ('吉林', '白城', '0', '0', '145', '0', '145', '0');
INSERT INTO `c_data` VALUES ('吉林', '白山', '0', '0', '24', '0', '24', '0');
INSERT INTO `c_data` VALUES ('甘肃', '白银', '0', '0', '23', '0', '23', '0');
INSERT INTO `c_data` VALUES ('广西', '百色', '0', '0', '283', '0', '283', '0');
INSERT INTO `c_data` VALUES ('湖南', '益阳', '0', '0', '65', '0', '65', '0');
INSERT INTO `c_data` VALUES ('江苏', '盐城', '41', '0', '68', '0', '27', '0');
INSERT INTO `c_data` VALUES ('辽宁', '盘锦', '0', '0', '14', '0', '14', '0');
INSERT INTO `c_data` VALUES ('浙江', '省十里丰监狱', '0', '0', '36', '0', '36', '0');
INSERT INTO `c_data` VALUES ('四川', '眉山', '0', '0', '9', '0', '9', '0');
INSERT INTO `c_data` VALUES ('宁夏', '石嘴山', '0', '0', '1', '0', '1', '0');
INSERT INTO `c_data` VALUES ('河北', '石家庄', '0', '0', '977', '1', '976', '0');
INSERT INTO `c_data` VALUES ('北京', '石景山', '20', '0', '32', '0', '12', '0');
INSERT INTO `c_data` VALUES ('重庆', '石柱县', '0', '0', '15', '0', '15', '0');
INSERT INTO `c_data` VALUES ('湖北', '神农架', '0', '0', '11', '0', '11', '0');
INSERT INTO `c_data` VALUES ('福建', '福州', '2', '0', '74', '1', '71', '0');
INSERT INTO `c_data` VALUES ('重庆', '秀山县', '0', '0', '1', '0', '1', '0');
INSERT INTO `c_data` VALUES ('河北', '秦皇岛', '12', '0', '22', '1', '9', '0');
INSERT INTO `c_data` VALUES ('新疆', '第七师', '0', '0', '1', '0', '1', '0');
INSERT INTO `c_data` VALUES ('新疆', '第八师石河子', '0', '0', '4', '1', '3', '0');
INSERT INTO `c_data` VALUES ('重庆', '綦江区', '0', '0', '23', '0', '23', '0');
INSERT INTO `c_data` VALUES ('天津', '红桥区', '2', '0', '38', '0', '36', '0');
INSERT INTO `c_data` VALUES ('云南', '红河', '29', '0', '38', '0', '9', '0');
INSERT INTO `c_data` VALUES ('浙江', '绍兴', '23', '0', '453', '0', '430', '0');
INSERT INTO `c_data` VALUES ('北京', '经济开发区', '4', '0', '4', '0', '0', '0');
INSERT INTO `c_data` VALUES ('黑龙江', '绥化', '0', '0', '539', '4', '535', '0');
INSERT INTO `c_data` VALUES ('四川', '绵阳', '0', '0', '23', '0', '23', '0');
INSERT INTO `c_data` VALUES ('山东', '聊城', '0', '0', '38', '0', '38', '0');
INSERT INTO `c_data` VALUES ('广东', '肇庆', '8', '0', '27', '1', '18', '0');
INSERT INTO `c_data` VALUES ('四川', '自贡', '0', '0', '10', '0', '10', '0');
INSERT INTO `c_data` VALUES ('浙江', '舟山', '18', '0', '28', '0', '10', '0');
INSERT INTO `c_data` VALUES ('安徽', '芜湖', '0', '0', '40', '0', '40', '0');
INSERT INTO `c_data` VALUES ('江苏', '苏州', '23', '0', '228', '0', '205', '0');
INSERT INTO `c_data` VALUES ('广东', '茂名', '7', '0', '21', '0', '14', '0');
INSERT INTO `c_data` VALUES ('湖北', '荆州', '0', '0', '1582', '52', '1530', '0');
INSERT INTO `c_data` VALUES ('湖北', '荆门', '0', '0', '971', '41', '930', '0');
INSERT INTO `c_data` VALUES ('重庆', '荣昌区', '0', '0', '9', '0', '9', '0');
INSERT INTO `c_data` VALUES ('福建', '莆田', '47', '0', '322', '0', '275', '0');
INSERT INTO `c_data` VALUES ('山东', '菏泽', '0', '0', '20', '0', '20', '0');
INSERT INTO `c_data` VALUES ('江西', '萍乡', '0', '0', '33', '0', '33', '0');
INSERT INTO `c_data` VALUES ('辽宁', '营口', '93', '0', '218', '0', '125', '0');
INSERT INTO `c_data` VALUES ('辽宁', '葫芦岛', '19', '0', '227', '1', '207', '0');
INSERT INTO `c_data` VALUES ('天津', '蓟州区', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('上海', '虹口', '1058', '0', '3690', '0', '2632', '0');
INSERT INTO `c_data` VALUES ('安徽', '蚌埠', '0', '0', '160', '5', '155', '0');
INSERT INTO `c_data` VALUES ('河北', '衡水', '1', '0', '14', '0', '13', '0');
INSERT INTO `c_data` VALUES ('湖南', '衡阳', '0', '0', '53', '0', '53', '0');
INSERT INTO `c_data` VALUES ('浙江', '衢州', '94', '0', '116', '0', '22', '0');
INSERT INTO `c_data` VALUES ('湖北', '襄阳', '0', '0', '1175', '40', '1135', '0');
INSERT INTO `c_data` VALUES ('云南', '西双版纳州', '6', '0', '21', '1', '14', '0');
INSERT INTO `c_data` VALUES ('北京', '西城', '91', '0', '97', '0', '6', '0');
INSERT INTO `c_data` VALUES ('青海', '西宁', '38', '0', '119', '0', '81', '0');
INSERT INTO `c_data` VALUES ('陕西', '西安', '2', '0', '2338', '3', '2333', '0');
INSERT INTO `c_data` VALUES ('天津', '西青区', '3', '0', '209', '0', '206', '0');
INSERT INTO `c_data` VALUES ('河南', '许昌', '155', '0', '568', '1', '412', '0');
INSERT INTO `c_data` VALUES ('广西', '贵港', '0', '0', '8', '0', '8', '0');
INSERT INTO `c_data` VALUES ('贵州', '贵阳', '0', '0', '39', '1', '38', '0');
INSERT INTO `c_data` VALUES ('广西', '贺州', '0', '0', '4', '0', '4', '0');
INSERT INTO `c_data` VALUES ('四川', '资阳', '1', '0', '5', '0', '4', '0');
INSERT INTO `c_data` VALUES ('江西', '赣州', '0', '0', '76', '1', '75', '0');
INSERT INTO `c_data` VALUES ('江西', '赣江新区', '0', '0', '1', '0', '1', '0');
INSERT INTO `c_data` VALUES ('内蒙古', '赤峰', '0', '0', '13', '0', '13', '0');
INSERT INTO `c_data` VALUES ('河北', '辛集市', '0', '0', '71', '0', '71', '0');
INSERT INTO `c_data` VALUES ('吉林', '辽源', '0', '0', '9', '0', '9', '0');
INSERT INTO `c_data` VALUES ('辽宁', '辽阳', '0', '0', '12', '0', '12', '0');
INSERT INTO `c_data` VALUES ('四川', '达州', '3', '0', '45', '0', '42', '0');
INSERT INTO `c_data` VALUES ('山西', '运城', '0', '0', '33', '0', '33', '0');
INSERT INTO `c_data` VALUES ('江苏', '连云港', '2', '0', '195', '0', '193', '0');
INSERT INTO `c_data` VALUES ('云南', '迪庆州', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('吉林', '通化', '1', '0', '314', '1', '312', '0');
INSERT INTO `c_data` VALUES ('北京', '通州', '89', '0', '101', '9', '3', '0');
INSERT INTO `c_data` VALUES ('内蒙古', '通辽', '1', '0', '19', '0', '18', '1');
INSERT INTO `c_data` VALUES ('四川', '遂宁', '1', '0', '22', '0', '21', '0');
INSERT INTO `c_data` VALUES ('贵州', '遵义', '1', '0', '56', '0', '55', '0');
INSERT INTO `c_data` VALUES ('河北', '邢台', '0', '0', '136', '1', '135', '0');
INSERT INTO `c_data` VALUES ('西藏', '那曲', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('河北', '邯郸', '23', '0', '64', '0', '41', '0');
INSERT INTO `c_data` VALUES ('湖南', '邵阳', '51', '0', '154', '1', '102', '0');
INSERT INTO `c_data` VALUES ('河南', '郑州', '114', '0', '606', '5', '487', '0');
INSERT INTO `c_data` VALUES ('湖南', '郴州', '1', '0', '40', '0', '39', '0');
INSERT INTO `c_data` VALUES ('内蒙古', '鄂尔多斯', '0', '0', '15', '0', '15', '0');
INSERT INTO `c_data` VALUES ('湖北', '鄂州', '0', '0', '1395', '59', '1336', '0');
INSERT INTO `c_data` VALUES ('重庆', '酉阳县', '0', '0', '1', '0', '1', '0');
INSERT INTO `c_data` VALUES ('甘肃', '酒泉', '0', '0', '1', '0', '1', '0');
INSERT INTO `c_data` VALUES ('浙江', '金华', '85', '0', '142', '0', '57', '0');
INSERT INTO `c_data` VALUES ('上海', '金山', '11', '0', '339', '0', '328', '0');
INSERT INTO `c_data` VALUES ('甘肃', '金昌', '0', '0', '1', '0', '1', '0');
INSERT INTO `c_data` VALUES ('广西', '钦州', '0', '0', '27', '0', '27', '0');
INSERT INTO `c_data` VALUES ('辽宁', '铁岭', '2', '0', '14', '0', '12', '0');
INSERT INTO `c_data` VALUES ('贵州', '铜仁', '0', '0', '12', '0', '12', '0');
INSERT INTO `c_data` VALUES ('陕西', '铜川', '0', '0', '26', '0', '26', '0');
INSERT INTO `c_data` VALUES ('重庆', '铜梁区', '0', '0', '10', '0', '10', '0');
INSERT INTO `c_data` VALUES ('安徽', '铜陵', '0', '0', '33', '0', '33', '0');
INSERT INTO `c_data` VALUES ('宁夏', '银川', '0', '0', '68', '0', '68', '0');
INSERT INTO `c_data` VALUES ('内蒙古', '锡林郭勒', '13', '0', '42', '0', '29', '10');
INSERT INTO `c_data` VALUES ('辽宁', '锦州', '1', '0', '15', '0', '14', '0');
INSERT INTO `c_data` VALUES ('江苏', '镇江', '12', '0', '24', '0', '12', '0');
INSERT INTO `c_data` VALUES ('上海', '长宁', '532', '0', '2408', '0', '1876', '0');
INSERT INTO `c_data` VALUES ('重庆', '长寿区', '0', '0', '24', '0', '24', '0');
INSERT INTO `c_data` VALUES ('吉林', '长春', '515', '0', '25176', '0', '24661', '0');
INSERT INTO `c_data` VALUES ('湖南', '长沙', '5', '0', '274', '2', '267', '0');
INSERT INTO `c_data` VALUES ('山西', '长治', '0', '0', '8', '0', '8', '0');
INSERT INTO `c_data` VALUES ('吉林', '长白山管委会', '0', '0', '1', '0', '1', '0');
INSERT INTO `c_data` VALUES ('北京', '门头沟', '9', '0', '13', '0', '4', '0');
INSERT INTO `c_data` VALUES ('上海', '闵行', '680', '0', '5393', '0', '4713', '0');
INSERT INTO `c_data` VALUES ('辽宁', '阜新', '2', '0', '10', '0', '8', '0');
INSERT INTO `c_data` VALUES ('安徽', '阜阳', '0', '0', '163', '0', '163', '0');
INSERT INTO `c_data` VALUES ('广西', '防城港', '0', '0', '90', '0', '90', '0');
INSERT INTO `c_data` VALUES ('广东', '阳江', '1', '0', '15', '0', '14', '0');
INSERT INTO `c_data` VALUES ('山西', '阳泉', '0', '0', '5', '0', '5', '0');
INSERT INTO `c_data` VALUES ('新疆', '阿克苏', '0', '0', '1', '0', '1', '0');
INSERT INTO `c_data` VALUES ('新疆', '阿勒泰', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('四川', '阿坝', '0', '0', '1', '0', '1', '0');
INSERT INTO `c_data` VALUES ('内蒙古', '阿拉善盟', '0', '0', '167', '0', '167', '0');
INSERT INTO `c_data` VALUES ('西藏', '阿里地区', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('甘肃', '陇南', '0', '0', '7', '0', '7', '0');
INSERT INTO `c_data` VALUES ('海南', '陵水县', '6', '0', '10', '0', '4', '0');
INSERT INTO `c_data` VALUES ('湖北', '随州', '0', '0', '1307', '45', '1262', '0');
INSERT INTO `c_data` VALUES ('河北', '雄安新区', '2', '0', '20', '0', '18', '0');
INSERT INTO `c_data` VALUES ('四川', '雅安', '0', '0', '8', '0', '8', '0');
INSERT INTO `c_data` VALUES ('山东', '青岛', '0', '0', '992', '1', '991', '0');
INSERT INTO `c_data` VALUES ('上海', '青浦', '71', '0', '1324', '0', '1253', '0');
INSERT INTO `c_data` VALUES ('上海', '静安', '823', '0', '3240', '1', '2416', '0');
INSERT INTO `c_data` VALUES ('天津', '静海区', '0', '0', '11', '0', '11', '0');
INSERT INTO `c_data` VALUES ('辽宁', '鞍山', '5', '0', '19', '0', '14', '0');
INSERT INTO `c_data` VALUES ('陕西', '韩城', '0', '0', '1', '0', '1', '0');
INSERT INTO `c_data` VALUES ('广东', '韶关', '2', '0', '12', '1', '9', '0');
INSERT INTO `c_data` VALUES ('北京', '顺义', '54', '0', '99', '0', '45', '0');
INSERT INTO `c_data` VALUES ('安徽', '马鞍山', '0', '0', '41', '0', '41', '0');
INSERT INTO `c_data` VALUES ('河南', '驻马店', '0', '0', '143', '0', '143', '0');
INSERT INTO `c_data` VALUES ('重庆', '高新区', '0', '0', '51', '0', '51', '0');
INSERT INTO `c_data` VALUES ('黑龙江', '鸡西', '0', '0', '52', '0', '52', '0');
INSERT INTO `c_data` VALUES ('河南', '鹤壁', '0', '0', '19', '0', '19', '0');
INSERT INTO `c_data` VALUES ('黑龙江', '鹤岗', '0', '0', '5', '0', '5', '0');
INSERT INTO `c_data` VALUES ('江西', '鹰潭', '0', '0', '18', '0', '18', '0');
INSERT INTO `c_data` VALUES ('湖北', '黄冈', '0', '0', '2913', '125', '2788', '0');
INSERT INTO `c_data` VALUES ('青海', '黄南州', '0', '0', '0', '0', '0', '0');
INSERT INTO `c_data` VALUES ('安徽', '黄山', '0', '0', '9', '0', '9', '0');
INSERT INTO `c_data` VALUES ('上海', '黄浦', '1393', '0', '6611', '0', '5218', '1');
INSERT INTO `c_data` VALUES ('湖北', '黄石', '0', '0', '1015', '39', '976', '0');
INSERT INTO `c_data` VALUES ('黑龙江', '黑河', '0', '0', '297', '0', '297', '0');
INSERT INTO `c_data` VALUES ('贵州', '黔东南州', '0', '0', '14', '0', '14', '0');
INSERT INTO `c_data` VALUES ('贵州', '黔南州', '0', '0', '17', '0', '17', '0');
INSERT INTO `c_data` VALUES ('重庆', '黔江区', '0', '0', '2', '0', '2', '0');
INSERT INTO `c_data` VALUES ('贵州', '黔西南州', '0', '0', '8', '0', '8', '0');
INSERT INTO `c_data` VALUES ('黑龙江', '齐齐哈尔', '0', '0', '45', '1', '44', '0');
INSERT INTO `c_data` VALUES ('福建', '龙岩', '0', '0', '6', '0', '6', '0');

-- ----------------------------
-- Table structure for `g_data`
-- ----------------------------
DROP TABLE IF EXISTS `g_data`;
CREATE TABLE `g_data` (
  `g_id` varchar(20) NOT NULL,
  `g_localConfirm` varchar(20) NOT NULL,
  `g_nowConfirm` varchar(20) NOT NULL,
  `g_confirm` varchar(20) NOT NULL,
  `g_wzz` varchar(20) NOT NULL,
  `g_importedCase` varchar(20) NOT NULL,
  `g_dead` varchar(20) NOT NULL,
  `g_localConfirm_add` varchar(20) NOT NULL,
  `g_nowConfirm_add` varchar(20) NOT NULL,
  `g_confirm_add` varchar(20) NOT NULL,
  `g_wzz_add` varchar(20) NOT NULL,
  `g_importedCase_add` varchar(20) NOT NULL,
  `g_dead_add` varchar(20) NOT NULL,
  PRIMARY KEY (`g_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of g_data
-- ----------------------------
INSERT INTO `g_data` VALUES ('1', '1260', '4727944', '5054540', '7197', '20500', '23396', '79', '0', '0', '583', '0', '0');

-- ----------------------------
-- Table structure for `leave_apply`
-- ----------------------------
DROP TABLE IF EXISTS `leave_apply`;
CREATE TABLE `leave_apply` (
  `apply_id` int(20) NOT NULL AUTO_INCREMENT,
  `stu_id` int(20) NOT NULL,
  `stu_name` varchar(20) NOT NULL,
  `stu_faculty` varchar(20) NOT NULL,
  `stu_class` varchar(20) NOT NULL,
  `reason` varchar(200) NOT NULL,
  `travel` varchar(20) NOT NULL,
  `leave_time` date NOT NULL,
  `back_time` date NOT NULL,
  `apply_state` varchar(20) NOT NULL DEFAULT '待审批',
  PRIMARY KEY (`apply_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of leave_apply
-- ----------------------------
INSERT INTO `leave_apply` VALUES ('1', '2', '张三', '计算机科学与技术学院', '软工183', '回家', '北京-成都', '2022-04-01', '2022-04-03', '拒绝');
INSERT INTO `leave_apply` VALUES ('2', '3', '李四', '计算机科学与技术学院', '软工183', '实习', '北京-兰州', '2022-03-04', '2022-04-13', '拒绝');
INSERT INTO `leave_apply` VALUES ('3', '4', '麻子', '外国语学院', '英语181', '回家探亲', '北京-兴义', '2022-04-04', '2022-04-04', '通过');
INSERT INTO `leave_apply` VALUES ('4', '19', '贝壳汉姆', '计算机科学与技术学院', '软工183', '回家', '北京-南京', '2022-04-04', '2022-04-17', '待审批');

-- ----------------------------
-- Table structure for `log`
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `log_id` int(20) NOT NULL AUTO_INCREMENT,
  `log_operation` varchar(200) DEFAULT NULL,
  `log_access_addr` varchar(200) DEFAULT NULL,
  `log_access_path` varchar(200) DEFAULT NULL,
  `log_ip` varchar(200) DEFAULT NULL,
  `log_access_method` varchar(200) DEFAULT NULL,
  `log_access_controller` varchar(200) DEFAULT NULL,
  `log_access_time` datetime DEFAULT NULL,
  `log_time_use` varchar(200) DEFAULT NULL,
  `log_parameter` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4094 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of log
-- ----------------------------
INSERT INTO `log` VALUES ('4000', 'selectTeacherAll', 'http://localhost:8080/user/select/teacher/all', '/user/select/teacher/all', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.UserManagementController.selectTeacherAll()', 'class com.example.campus_system.controller.UserManagementController', '2022-07-29 16:13:56', '3', 'page=1&limit=10');
INSERT INTO `log` VALUES ('4001', 'selectStudentAll', 'http://localhost:8080/user/select/stu/all', '/user/select/stu/all', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.UserManagementController.selectStudentAll()', 'class com.example.campus_system.controller.UserManagementController', '2022-07-29 16:14:10', '2', 'page=1&limit=10');
INSERT INTO `log` VALUES ('4002', 'addStudent', 'http://localhost:8080/user/add/stu', '/user/add/stu', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.UserManagementController.addStudent(String,String,String,String,String,String,String,String,String,String)', 'class com.example.campus_system.controller.UserManagementController', '2022-07-29 16:16:06', '105', 'null');
INSERT INTO `log` VALUES ('4003', 'selectStudentAll', 'http://localhost:8080/user/select/stu/all', '/user/select/stu/all', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.UserManagementController.selectStudentAll()', 'class com.example.campus_system.controller.UserManagementController', '2022-07-29 16:16:06', '1', 'page=1&limit=10');
INSERT INTO `log` VALUES ('4004', 'searchStudent', 'http://localhost:8080/user/search/stu', '/user/search/stu', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.UserManagementController.searchStudent(String,String)', 'class com.example.campus_system.controller.UserManagementController', '2022-07-29 16:16:21', '5', 'page=1&limit=10&stu_id=&stu_faculty=外国语学院');
INSERT INTO `log` VALUES ('4005', 'selectGuoEpmcInfo', 'http://localhost:8080/epmc/select/g', '/epmc/select/g', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.EpidemicInfoController.selectGuoEpmcInfo()', 'class com.example.campus_system.controller.EpidemicInfoController', '2022-07-29 16:16:41', '6', 'null');
INSERT INTO `log` VALUES ('4006', 'selectPEpmcInfo', 'http://localhost:8080/epmc/select/p', '/epmc/select/p', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.EpidemicInfoController.selectPEpmcInfo()', 'class com.example.campus_system.controller.EpidemicInfoController', '2022-07-29 16:16:41', '8', 'page=1&limit=10');
INSERT INTO `log` VALUES ('4007', 'selectPEpmcInfo', 'http://localhost:8080/epmc/select/c', '/epmc/select/c', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.EpidemicInfoController.selectPEpmcInfo(String)', 'class com.example.campus_system.controller.EpidemicInfoController', '2022-07-29 16:17:59', '5', 'page=1&limit=10&p_name=上海');
INSERT INTO `log` VALUES ('4008', 'selectNotice', 'http://localhost:8080/ntc/select/all', '/ntc/select/all', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.NoticeController.selectNotice()', 'class com.example.campus_system.controller.NoticeController', '2022-07-29 16:18:01', '9', 'page=1&limit=10');
INSERT INTO `log` VALUES ('4009', 'selectTeacherInfo', 'http://localhost:8080/psinfo/select/teacher', '/psinfo/select/teacher', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.PersonalInfoController.selectTeacherInfo(Authentication)', 'class com.example.campus_system.controller.PersonalInfoController', '2022-07-29 16:18:04', '10', 'null');
INSERT INTO `log` VALUES ('4010', 'selectIndexNotice', 'http://localhost:8080/ntc/select/index', '/ntc/select/index', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.NoticeController.selectIndexNotice()', 'class com.example.campus_system.controller.NoticeController', '2022-07-29 16:18:06', '2', 'page=1&limit=10');
INSERT INTO `log` VALUES ('4011', 'selectmap1', 'http://localhost:8080/select/map1', '/select/map1', '0:0:0:0:0:0:0:1', 'List com.example.campus_system.controller.IndexController.selectmap1()', 'class com.example.campus_system.controller.IndexController', '2022-07-29 16:18:06', '7', 'null');
INSERT INTO `log` VALUES ('4012', 'selectmap2', 'http://localhost:8080/select/map2', '/select/map2', '0:0:0:0:0:0:0:1', 'List com.example.campus_system.controller.IndexController.selectmap2()', 'class com.example.campus_system.controller.IndexController', '2022-07-29 16:18:06', '7', 'null');
INSERT INTO `log` VALUES ('4013', 'selectNum', 'http://localhost:8080/select/num', '/select/num', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.IndexController.selectNum()', 'class com.example.campus_system.controller.IndexController', '2022-07-29 16:18:06', '19', 'null');
INSERT INTO `log` VALUES ('4014', 'selectLog', 'http://localhost:8080/log/select', '/log/select', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.LogController.selectLog()', 'class com.example.campus_system.controller.LogController', '2022-07-29 16:18:17', '151', 'page=1&limit=10');
INSERT INTO `log` VALUES ('4015', 'selectNotice', 'http://localhost:8080/ntc/select/all', '/ntc/select/all', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.NoticeController.selectNotice()', 'class com.example.campus_system.controller.NoticeController', '2022-07-29 16:18:22', '1', 'page=1&limit=10');
INSERT INTO `log` VALUES ('4016', 'selectTeacherInfo', 'http://localhost:8080/psinfo/select/teacher', '/psinfo/select/teacher', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.PersonalInfoController.selectTeacherInfo(Authentication)', 'class com.example.campus_system.controller.PersonalInfoController', '2022-07-29 16:18:38', '3', 'null');
INSERT INTO `log` VALUES ('4017', 'selectSystemPermissions', 'http://localhost:8080/sysp/select', '/sysp/select', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.SystemPermissionsController.selectSystemPermissions()', 'class com.example.campus_system.controller.SystemPermissionsController', '2022-07-29 16:18:40', '2', 'page=1&limit=10');
INSERT INTO `log` VALUES ('4018', 'adminSelectAllClock', 'http://localhost:8080/clock/admin/select', '/clock/admin/select', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.ClockController.adminSelectAllClock()', 'class com.example.campus_system.controller.ClockController', '2022-07-29 16:18:42', '5', 'page=1&limit=10');
INSERT INTO `log` VALUES ('4019', 'login', 'http://localhost:8080/login', '/login', '0:0:0:0:0:0:0:1', 'String com.example.campus_system.controller.LoginController.login()', 'class com.example.campus_system.controller.LoginController', '2022-08-12 15:02:50', '2', 'null');
INSERT INTO `log` VALUES ('4020', 'login', 'http://localhost:8080/login', '/login', '0:0:0:0:0:0:0:1', 'String com.example.campus_system.controller.LoginController.login()', 'class com.example.campus_system.controller.LoginController', '2022-08-12 15:02:50', '0', 'null');
INSERT INTO `log` VALUES ('4021', 'selectTeacherInfo', 'http://localhost:8080/psinfo/select/teacher', '/psinfo/select/teacher', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.PersonalInfoController.selectTeacherInfo(Authentication)', 'class com.example.campus_system.controller.PersonalInfoController', '2022-08-12 15:02:57', '10', 'null');
INSERT INTO `log` VALUES ('4022', 'error', 'http://localhost:8080/error', '/error', '0:0:0:0:0:0:0:1', 'ResponseEntity org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController.error(HttpServletRequest)', 'class org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController', '2022-08-12 15:02:57', '5', 'null');
INSERT INTO `log` VALUES ('4023', 'selectIndexNotice', 'http://localhost:8080/ntc/select/index', '/ntc/select/index', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.NoticeController.selectIndexNotice()', 'class com.example.campus_system.controller.NoticeController', '2022-08-12 15:02:58', '8', 'page=1&limit=10');
INSERT INTO `log` VALUES ('4024', 'selectmap2', 'http://localhost:8080/select/map2', '/select/map2', '0:0:0:0:0:0:0:1', 'List com.example.campus_system.controller.IndexController.selectmap2()', 'class com.example.campus_system.controller.IndexController', '2022-08-12 15:02:58', '11', 'null');
INSERT INTO `log` VALUES ('4025', 'selectmap1', 'http://localhost:8080/select/map1', '/select/map1', '0:0:0:0:0:0:0:1', 'List com.example.campus_system.controller.IndexController.selectmap1()', 'class com.example.campus_system.controller.IndexController', '2022-08-12 15:02:58', '11', 'null');
INSERT INTO `log` VALUES ('4026', 'selectNum', 'http://localhost:8080/select/num', '/select/num', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.IndexController.selectNum()', 'class com.example.campus_system.controller.IndexController', '2022-08-12 15:02:58', '27', 'null');
INSERT INTO `log` VALUES ('4027', 'selectTeacherInfo', 'http://localhost:8080/psinfo/select/teacher', '/psinfo/select/teacher', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.PersonalInfoController.selectTeacherInfo(Authentication)', 'class com.example.campus_system.controller.PersonalInfoController', '2022-08-12 15:02:59', '3', 'null');
INSERT INTO `log` VALUES ('4028', 'error', 'http://localhost:8080/error', '/error', '0:0:0:0:0:0:0:1', 'ResponseEntity org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController.error(HttpServletRequest)', 'class org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController', '2022-08-12 15:02:59', '0', 'null');
INSERT INTO `log` VALUES ('4029', 'selectTeacherInfo', 'http://localhost:8080/psinfo/select/teacher', '/psinfo/select/teacher', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.PersonalInfoController.selectTeacherInfo(Authentication)', 'class com.example.campus_system.controller.PersonalInfoController', '2022-08-12 15:04:38', '2', 'null');
INSERT INTO `log` VALUES ('4030', 'error', 'http://localhost:8080/error', '/error', '0:0:0:0:0:0:0:1', 'ResponseEntity org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController.error(HttpServletRequest)', 'class org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController', '2022-08-12 15:04:38', '0', 'null');
INSERT INTO `log` VALUES ('4031', 'error', 'http://localhost:8080/error', '/error', '0:0:0:0:0:0:0:1', 'ResponseEntity org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController.error(HttpServletRequest)', 'class org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController', '2022-08-12 15:05:29', '0', 'null');
INSERT INTO `log` VALUES ('4032', 'selectTeacherInfo', 'http://localhost:8080/psinfo/select/teacher', '/psinfo/select/teacher', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.PersonalInfoController.selectTeacherInfo(Authentication)', 'class com.example.campus_system.controller.PersonalInfoController', '2022-08-12 15:05:40', '4', 'null');
INSERT INTO `log` VALUES ('4033', 'error', 'http://localhost:8080/error', '/error', '0:0:0:0:0:0:0:1', 'ResponseEntity org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController.error(HttpServletRequest)', 'class org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController', '2022-08-12 15:05:40', '0', 'null');
INSERT INTO `log` VALUES ('4034', 'login', 'http://localhost:8080/login', '/login', '0:0:0:0:0:0:0:1', 'String com.example.campus_system.controller.LoginController.login()', 'class com.example.campus_system.controller.LoginController', '2022-08-12 15:07:42', '3', 'null');
INSERT INTO `log` VALUES ('4035', 'selectTeacherInfo', 'http://localhost:8080/psinfo/select/teacher', '/psinfo/select/teacher', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.PersonalInfoController.selectTeacherInfo(Authentication)', 'class com.example.campus_system.controller.PersonalInfoController', '2022-08-12 15:07:50', '8', 'null');
INSERT INTO `log` VALUES ('4036', 'error', 'http://localhost:8080/error', '/error', '0:0:0:0:0:0:0:1', 'ResponseEntity org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController.error(HttpServletRequest)', 'class org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController', '2022-08-12 15:07:50', '7', 'null');
INSERT INTO `log` VALUES ('4037', 'login', 'http://localhost:8080/login', '/login', '0:0:0:0:0:0:0:1', 'String com.example.campus_system.controller.LoginController.login()', 'class com.example.campus_system.controller.LoginController', '2022-08-12 15:11:03', '3', 'null');
INSERT INTO `log` VALUES ('4038', 'selectTeacherInfo', 'http://localhost:8080/psinfo/select/teacher', '/psinfo/select/teacher', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.PersonalInfoController.selectTeacherInfo(Authentication)', 'class com.example.campus_system.controller.PersonalInfoController', '2022-08-12 15:11:12', '10', 'null');
INSERT INTO `log` VALUES ('4039', 'error', 'http://localhost:8080/error', '/error', '0:0:0:0:0:0:0:1', 'ResponseEntity org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController.error(HttpServletRequest)', 'class org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController', '2022-08-12 15:11:12', '7', 'null');
INSERT INTO `log` VALUES ('4040', 'errorHtml', 'http://localhost:8080/error', '/error', '0:0:0:0:0:0:0:1', 'ModelAndView org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController.errorHtml(HttpServletRequest,HttpServletResponse)', 'class org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController', '2022-08-12 15:11:31', '12', 'null');
INSERT INTO `log` VALUES ('4041', 'errorHtml', 'http://localhost:8080/error', '/error', '0:0:0:0:0:0:0:1', 'ModelAndView org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController.errorHtml(HttpServletRequest,HttpServletResponse)', 'class org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController', '2022-08-12 15:11:59', '1', 'null');
INSERT INTO `log` VALUES ('4042', 'login', 'http://localhost:8080/login', '/login', '0:0:0:0:0:0:0:1', 'String com.example.campus_system.controller.LoginController.login()', 'class com.example.campus_system.controller.LoginController', '2022-08-12 15:15:42', '2', 'null');
INSERT INTO `log` VALUES ('4043', 'selectTeacherInfo', 'http://localhost:8080/psinfo/select/teacher', '/psinfo/select/teacher', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.PersonalInfoController.selectTeacherInfo(Authentication)', 'class com.example.campus_system.controller.PersonalInfoController', '2022-08-12 15:15:47', '5', 'null');
INSERT INTO `log` VALUES ('4044', 'selectReportAll', 'http://localhost:8080/report/select/all', '/report/select/all', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.ReportController.selectReportAll()', 'class com.example.campus_system.controller.ReportController', '2022-08-12 15:15:51', '9', 'page=1&limit=10');
INSERT INTO `log` VALUES ('4045', 'errorHtml', 'http://localhost:8080/error', '/error', '0:0:0:0:0:0:0:1', 'ModelAndView org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController.errorHtml(HttpServletRequest,HttpServletResponse)', 'class org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController', '2022-08-12 15:15:52', '23', 'null');
INSERT INTO `log` VALUES ('4046', 'selectGuoEpmcInfo', 'http://localhost:8080/epmc/select/g', '/epmc/select/g', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.EpidemicInfoController.selectGuoEpmcInfo()', 'class com.example.campus_system.controller.EpidemicInfoController', '2022-08-12 15:15:55', '6', 'null');
INSERT INTO `log` VALUES ('4047', 'selectPEpmcInfo', 'http://localhost:8080/epmc/select/p', '/epmc/select/p', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.EpidemicInfoController.selectPEpmcInfo()', 'class com.example.campus_system.controller.EpidemicInfoController', '2022-08-12 15:15:55', '8', 'page=1&limit=10');
INSERT INTO `log` VALUES ('4048', 'selectReportAll', 'http://localhost:8080/report/select/all', '/report/select/all', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.ReportController.selectReportAll()', 'class com.example.campus_system.controller.ReportController', '2022-08-12 15:16:07', '1', 'page=1&limit=10');
INSERT INTO `log` VALUES ('4049', 'selectTeacherInfo', 'http://localhost:8080/psinfo/select/teacher', '/psinfo/select/teacher', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.PersonalInfoController.selectTeacherInfo(Authentication)', 'class com.example.campus_system.controller.PersonalInfoController', '2022-08-12 15:16:08', '2', 'null');
INSERT INTO `log` VALUES ('4050', 'login', 'http://localhost:8080/login', '/login', '0:0:0:0:0:0:0:1', 'String com.example.campus_system.controller.LoginController.login()', 'class com.example.campus_system.controller.LoginController', '2022-08-12 15:16:09', '0', 'null');
INSERT INTO `log` VALUES ('4051', 'selectStudentInfo', 'http://localhost:8080/psinfo/select/student', '/psinfo/select/student', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.PersonalInfoController.selectStudentInfo(Authentication)', 'class com.example.campus_system.controller.PersonalInfoController', '2022-08-12 15:16:14', '3', 'null');
INSERT INTO `log` VALUES ('4052', 'selectGuoEpmcInfo', 'http://localhost:8080/epmc/select/g', '/epmc/select/g', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.EpidemicInfoController.selectGuoEpmcInfo()', 'class com.example.campus_system.controller.EpidemicInfoController', '2022-08-12 15:16:55', '1', 'null');
INSERT INTO `log` VALUES ('4053', 'selectPEpmcInfo', 'http://localhost:8080/epmc/select/p', '/epmc/select/p', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.EpidemicInfoController.selectPEpmcInfo()', 'class com.example.campus_system.controller.EpidemicInfoController', '2022-08-12 15:16:55', '5', 'page=1&limit=10');
INSERT INTO `log` VALUES ('4054', 'selectReportAll', 'http://localhost:8080/report/select/all', '/report/select/all', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.ReportController.selectReportAll()', 'class com.example.campus_system.controller.ReportController', '2022-08-12 15:16:56', '1', 'page=1&limit=10');
INSERT INTO `log` VALUES ('4055', 'errorHtml', 'http://localhost:8080/error', '/error', '0:0:0:0:0:0:0:1', 'ModelAndView org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController.errorHtml(HttpServletRequest,HttpServletResponse)', 'class org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController', '2022-08-12 15:17:01', '2', 'null');
INSERT INTO `log` VALUES ('4056', 'login', 'http://localhost:8080/login', '/login', '0:0:0:0:0:0:0:1', 'String com.example.campus_system.controller.LoginController.login()', 'class com.example.campus_system.controller.LoginController', '2022-08-12 15:19:06', '2', 'null');
INSERT INTO `log` VALUES ('4057', 'selectStudentInfo', 'http://localhost:8080/psinfo/select/student', '/psinfo/select/student', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.PersonalInfoController.selectStudentInfo(Authentication)', 'class com.example.campus_system.controller.PersonalInfoController', '2022-08-12 15:19:12', '7', 'null');
INSERT INTO `log` VALUES ('4058', 'updateStudentInfo', 'http://localhost:8080/psinfo/update/student', '/psinfo/update/student', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.PersonalInfoController.updateStudentInfo(Authentication,MultipartFile,String,String,String,String,String,String,String,String,String)', 'class com.example.campus_system.controller.PersonalInfoController', '2022-08-12 15:19:24', '9', 'null');
INSERT INTO `log` VALUES ('4059', 'selectStudentInfo', 'http://localhost:8080/psinfo/select/student', '/psinfo/select/student', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.PersonalInfoController.selectStudentInfo(Authentication)', 'class com.example.campus_system.controller.PersonalInfoController', '2022-08-12 15:19:26', '2', 'null');
INSERT INTO `log` VALUES ('4060', 'selectStudentInfo', 'http://localhost:8080/psinfo/select/student', '/psinfo/select/student', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.PersonalInfoController.selectStudentInfo(Authentication)', 'class com.example.campus_system.controller.PersonalInfoController', '2022-08-12 15:19:26', '2', 'null');
INSERT INTO `log` VALUES ('4061', 'selectStudentInfo', 'http://localhost:8080/psinfo/select/student', '/psinfo/select/student', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.PersonalInfoController.selectStudentInfo(Authentication)', 'class com.example.campus_system.controller.PersonalInfoController', '2022-08-12 15:19:27', '3', 'null');
INSERT INTO `log` VALUES ('4062', 'selectGuoEpmcInfo', 'http://localhost:8080/epmc/select/g', '/epmc/select/g', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.EpidemicInfoController.selectGuoEpmcInfo()', 'class com.example.campus_system.controller.EpidemicInfoController', '2022-08-12 15:19:28', '5', 'null');
INSERT INTO `log` VALUES ('4063', 'selectPEpmcInfo', 'http://localhost:8080/epmc/select/p', '/epmc/select/p', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.EpidemicInfoController.selectPEpmcInfo()', 'class com.example.campus_system.controller.EpidemicInfoController', '2022-08-12 15:19:28', '9', 'page=1&limit=10');
INSERT INTO `log` VALUES ('4064', 'StudentselectReport', 'http://localhost:8080/report/stu/select', '/report/stu/select', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.ReportController.StudentselectReport(Authentication)', 'class com.example.campus_system.controller.ReportController', '2022-08-12 15:19:29', '7', 'null');
INSERT INTO `log` VALUES ('4065', 'uploadReport', 'http://localhost:8080/report/stu/upload', '/report/stu/upload', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.ReportController.uploadReport(Authentication,MultipartFile)', 'class com.example.campus_system.controller.ReportController', '2022-08-12 15:19:35', '9', 'null');
INSERT INTO `log` VALUES ('4066', 'selectGuoEpmcInfo', 'http://localhost:8080/epmc/select/g', '/epmc/select/g', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.EpidemicInfoController.selectGuoEpmcInfo()', 'class com.example.campus_system.controller.EpidemicInfoController', '2022-08-12 15:19:36', '1', 'null');
INSERT INTO `log` VALUES ('4067', 'selectPEpmcInfo', 'http://localhost:8080/epmc/select/p', '/epmc/select/p', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.EpidemicInfoController.selectPEpmcInfo()', 'class com.example.campus_system.controller.EpidemicInfoController', '2022-08-12 15:19:36', '4', 'page=1&limit=10');
INSERT INTO `log` VALUES ('4068', 'StudentselectReport', 'http://localhost:8080/report/stu/select', '/report/stu/select', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.ReportController.StudentselectReport(Authentication)', 'class com.example.campus_system.controller.ReportController', '2022-08-12 15:19:37', '1', 'null');
INSERT INTO `log` VALUES ('4069', 'uploadReport', 'http://localhost:8080/report/stu/upload', '/report/stu/upload', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.ReportController.uploadReport(Authentication,MultipartFile)', 'class com.example.campus_system.controller.ReportController', '2022-08-12 15:19:38', '2', 'null');
INSERT INTO `log` VALUES ('4070', 'uploadReport', 'http://localhost:8080/report/stu/upload', '/report/stu/upload', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.ReportController.uploadReport(Authentication,MultipartFile)', 'class com.example.campus_system.controller.ReportController', '2022-08-12 15:19:39', '2', 'null');
INSERT INTO `log` VALUES ('4071', 'uploadReport', 'http://localhost:8080/report/stu/upload', '/report/stu/upload', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.ReportController.uploadReport(Authentication,MultipartFile)', 'class com.example.campus_system.controller.ReportController', '2022-08-12 15:19:39', '1', 'null');
INSERT INTO `log` VALUES ('4072', 'uploadReport', 'http://localhost:8080/report/stu/upload', '/report/stu/upload', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.ReportController.uploadReport(Authentication,MultipartFile)', 'class com.example.campus_system.controller.ReportController', '2022-08-12 15:19:39', '2', 'null');
INSERT INTO `log` VALUES ('4073', 'uploadReport', 'http://localhost:8080/report/stu/upload', '/report/stu/upload', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.ReportController.uploadReport(Authentication,MultipartFile)', 'class com.example.campus_system.controller.ReportController', '2022-08-12 15:19:39', '1', 'null');
INSERT INTO `log` VALUES ('4074', 'uploadReport', 'http://localhost:8080/report/stu/upload', '/report/stu/upload', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.ReportController.uploadReport(Authentication,MultipartFile)', 'class com.example.campus_system.controller.ReportController', '2022-08-12 15:19:40', '1', 'null');
INSERT INTO `log` VALUES ('4075', 'uploadReport', 'http://localhost:8080/report/stu/upload', '/report/stu/upload', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.ReportController.uploadReport(Authentication,MultipartFile)', 'class com.example.campus_system.controller.ReportController', '2022-08-12 15:19:40', '10', 'null');
INSERT INTO `log` VALUES ('4076', 'uploadReport', 'http://localhost:8080/report/stu/upload', '/report/stu/upload', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.ReportController.uploadReport(Authentication,MultipartFile)', 'class com.example.campus_system.controller.ReportController', '2022-08-12 15:19:40', '1', 'null');
INSERT INTO `log` VALUES ('4077', 'uploadReport', 'http://localhost:8080/report/stu/upload', '/report/stu/upload', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.ReportController.uploadReport(Authentication,MultipartFile)', 'class com.example.campus_system.controller.ReportController', '2022-08-12 15:19:40', '2', 'null');
INSERT INTO `log` VALUES ('4078', 'StudentselectReport', 'http://localhost:8080/report/stu/select', '/report/stu/select', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.ReportController.StudentselectReport(Authentication)', 'class com.example.campus_system.controller.ReportController', '2022-08-12 15:19:50', '1', 'null');
INSERT INTO `log` VALUES ('4079', 'login', 'http://localhost:8080/login', '/login', '0:0:0:0:0:0:0:1', 'String com.example.campus_system.controller.LoginController.login()', 'class com.example.campus_system.controller.LoginController', '2022-08-16 16:58:29', '3', 'null');
INSERT INTO `log` VALUES ('4080', 'login', 'http://localhost:8080/login', '/login', '0:0:0:0:0:0:0:1', 'String com.example.campus_system.controller.LoginController.login()', 'class com.example.campus_system.controller.LoginController', '2022-08-16 16:58:30', '0', 'null');
INSERT INTO `log` VALUES ('4081', 'selectTeacherInfo', 'http://localhost:8080/psinfo/select/teacher', '/psinfo/select/teacher', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.PersonalInfoController.selectTeacherInfo(Authentication)', 'class com.example.campus_system.controller.PersonalInfoController', '2022-08-16 16:58:37', '7', 'null');
INSERT INTO `log` VALUES ('4082', 'selectSystemPermissions', 'http://localhost:8080/sysp/select', '/sysp/select', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.SystemPermissionsController.selectSystemPermissions()', 'class com.example.campus_system.controller.SystemPermissionsController', '2022-08-16 16:58:38', '6', 'page=1&limit=10');
INSERT INTO `log` VALUES ('4083', 'selectStudentAll', 'http://localhost:8080/user/select/stu/all', '/user/select/stu/all', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.UserManagementController.selectStudentAll()', 'class com.example.campus_system.controller.UserManagementController', '2022-08-16 16:58:43', '14', 'page=1&limit=10');
INSERT INTO `log` VALUES ('4084', 'selectTeacherAll', 'http://localhost:8080/user/select/teacher/all', '/user/select/teacher/all', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.UserManagementController.selectTeacherAll()', 'class com.example.campus_system.controller.UserManagementController', '2022-08-16 16:58:46', '4', 'page=1&limit=10');
INSERT INTO `log` VALUES ('4085', 'adminSelectAllClock', 'http://localhost:8080/clock/admin/select', '/clock/admin/select', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.ClockController.adminSelectAllClock()', 'class com.example.campus_system.controller.ClockController', '2022-08-16 16:59:29', '12', 'page=1&limit=10');
INSERT INTO `log` VALUES ('4086', 'login', 'http://localhost:8080/login', '/login', '0:0:0:0:0:0:0:1', 'String com.example.campus_system.controller.LoginController.login()', 'class com.example.campus_system.controller.LoginController', '2022-09-05 09:37:46', '7', 'null');
INSERT INTO `log` VALUES ('4087', 'login', 'http://localhost:8080/login', '/login', '0:0:0:0:0:0:0:1', 'String com.example.campus_system.controller.LoginController.login()', 'class com.example.campus_system.controller.LoginController', '2022-09-05 09:37:48', '0', 'null');
INSERT INTO `log` VALUES ('4088', 'loginfailure', 'http://localhost:8080/loginfailure', '/loginfailure', '0:0:0:0:0:0:0:1', 'String com.example.campus_system.controller.LoginController.loginfailure(HttpServletRequest)', 'class com.example.campus_system.controller.LoginController', '2022-09-05 09:37:54', '0', 'null');
INSERT INTO `log` VALUES ('4089', 'login', 'http://localhost:8080/login', '/login', '0:0:0:0:0:0:0:1', 'String com.example.campus_system.controller.LoginController.login()', 'class com.example.campus_system.controller.LoginController', '2022-09-05 09:37:54', '0', 'null');
INSERT INTO `log` VALUES ('4090', 'selectTeacherInfo', 'http://localhost:8080/psinfo/select/teacher', '/psinfo/select/teacher', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.PersonalInfoController.selectTeacherInfo(Authentication)', 'class com.example.campus_system.controller.PersonalInfoController', '2022-09-05 09:38:01', '15', 'null');
INSERT INTO `log` VALUES ('4091', 'selectNotice', 'http://localhost:8080/ntc/select/all', '/ntc/select/all', '0:0:0:0:0:0:0:1', 'BaseResponse com.example.campus_system.controller.NoticeController.selectNotice()', 'class com.example.campus_system.controller.NoticeController', '2022-09-05 09:38:03', '35', 'page=1&limit=10');
INSERT INTO `log` VALUES ('4092', 'login', 'http://localhost:8080/login', '/login', '0:0:0:0:0:0:0:1', 'String com.example.campus_system.controller.LoginController.login()', 'class com.example.campus_system.controller.LoginController', '2022-09-05 14:58:46', '6', 'null');
INSERT INTO `log` VALUES ('4093', 'login', 'http://localhost:8080/login', '/login', '0:0:0:0:0:0:0:1', 'String com.example.campus_system.controller.LoginController.login()', 'class com.example.campus_system.controller.LoginController', '2022-09-05 14:58:47', '0', 'null');

-- ----------------------------
-- Table structure for `notice`
-- ----------------------------
DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice` (
  `notice_id` int(20) NOT NULL AUTO_INCREMENT,
  `notice_title` varchar(200) NOT NULL,
  `notice_content` varchar(200) NOT NULL,
  `notice_user` varchar(20) NOT NULL,
  `notice_type` varchar(20) NOT NULL,
  `notice_state` varchar(20) NOT NULL DEFAULT '待审批',
  `notice_time` date NOT NULL,
  `notice_class` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of notice
-- ----------------------------
INSERT INTO `notice` VALUES ('1', '系统升级', '今晚系统升级', '王五', '通知公告', '拒绝', '2022-04-13', null);
INSERT INTO `notice` VALUES ('2', '测试一下', '系统今晚测试', '王五', '系统通知', '通过', '2022-04-01', null);
INSERT INTO `notice` VALUES ('3', '系统升级', '系统升级成功', '王五', '通知公告', '通过', '2022-04-07', null);
INSERT INTO `notice` VALUES ('7', '副食补贴', '副食补贴已发放', '王五', '通知公告', '通过', '2022-03-31', null);
INSERT INTO `notice` VALUES ('8', '系统通知', '系统今天维护', '王五', '通知公告', '通过', '2022-04-13', null);
INSERT INTO `notice` VALUES ('9', '软工183班集合', '软工183班全体成员到操场集合', '王五', '班级通知', '通过', '2022-04-13', '软工183');
INSERT INTO `notice` VALUES ('10', '教职工大会', '明天在大礼堂召开全体教职工大会', '王五', '教工通知', '拒绝', '2022-04-08', null);
INSERT INTO `notice` VALUES ('11', '核酸检测', '软工183班在今天19:00在老操场做核酸检测', '王五', '班级通知', '通过', '2022-04-07', '软工183');
INSERT INTO `notice` VALUES ('13', '肺结核检测', '明天软工183班全体去校医院检测肺结核', '王五', '班级通知', '通过', '2022-04-13', '软工183');
INSERT INTO `notice` VALUES ('14', '肺结核检查结果', '4月17日去校医院拿肺结核检查结果', '王五', '班级通知', '待审批', '2022-04-16', '软工183');
INSERT INTO `notice` VALUES ('15', '系统升级', '今天系统升级', '王五', '系统通知', '待审批', '2022-04-16', '');
INSERT INTO `notice` VALUES ('16', '副食补贴', '副食补贴已发放', '王五', '班级通知', '通过', '2022-03-31', '软工183');
INSERT INTO `notice` VALUES ('17', '教职工工资', '工资已发放', '王五', '教工通知', '通过', '2022-04-08', '');
INSERT INTO `notice` VALUES ('18', '加强对学生管理', '从今天开始加强对学生管理', '王五', '教工通知', '待审批', '2022-04-17', null);
INSERT INTO `notice` VALUES ('19', '教师节放假', '教师节当天轮换放假', '王五', '教工通知', '待审批', '2022-04-17', null);
INSERT INTO `notice` VALUES ('20', '核酸检测', '明天全体学生进行核酸检测', '王五', '通知公告', '待审批', '2022-04-17', null);
INSERT INTO `notice` VALUES ('22', 'java期中考试', '明天java期中考试', '王五', '班级通知', '通过', '2022-04-17', '软工183');
INSERT INTO `notice` VALUES ('23', '系统审计', '今天系统升级', '王五', '通知公告', '待审批', '2022-04-20', '');
INSERT INTO `notice` VALUES ('24', '期中考试', '明天期中考试', '毛老师', '班级通知', '通过', '2022-04-20', '软工183');
INSERT INTO `notice` VALUES ('25', '毕业答辩', '6月初进行毕业答辩', '王五', '通知公告', '通过', '2022-05-19', '');
INSERT INTO `notice` VALUES ('26', '123', 'qq', '王五', '通知公告', '待审批', '2022-06-21', '');

-- ----------------------------
-- Table structure for `p_data`
-- ----------------------------
DROP TABLE IF EXISTS `p_data`;
CREATE TABLE `p_data` (
  `p_name` varchar(20) NOT NULL,
  `p_total_nowConfirm` varchar(20) NOT NULL,
  `p_total_wzz` varchar(20) NOT NULL,
  `p_total_confirm` varchar(20) NOT NULL,
  `p_total_dead` varchar(20) NOT NULL,
  `p_total_heal` varchar(20) NOT NULL,
  `p_today_confirm` varchar(20) NOT NULL,
  `p_today_wzz_add` varchar(20) NOT NULL,
  PRIMARY KEY (`p_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of p_data
-- ----------------------------
INSERT INTO `p_data` VALUES ('上海', '449', '17111', '63033', '595', '61989', '7', '9');
INSERT INTO `p_data` VALUES ('云南', '20', '5', '2152', '2', '2130', '2', '1');
INSERT INTO `p_data` VALUES ('内蒙古', '14', '9', '1767', '1', '1752', '11', '5');
INSERT INTO `p_data` VALUES ('北京', '308', '300', '3410', '9', '3093', '9', '1');
INSERT INTO `p_data` VALUES ('台湾', '2258261', '0', '2274666', '2663', '13742', '76505', '0');
INSERT INTO `p_data` VALUES ('吉林', '146', '6217', '40293', '5', '40142', '0', '5');
INSERT INTO `p_data` VALUES ('四川', '113', '1147', '2343', '3', '2227', '4', '3');
INSERT INTO `p_data` VALUES ('天津', '148', '155', '1975', '3', '1824', '0', '0');
INSERT INTO `p_data` VALUES ('宁夏', '0', '0', '122', '0', '122', '0', '0');
INSERT INTO `p_data` VALUES ('安徽', '0', '1', '1065', '6', '1059', '0', '0');
INSERT INTO `p_data` VALUES ('山东', '1', '19', '2735', '7', '2727', '0', '0');
INSERT INTO `p_data` VALUES ('山西', '1', '0', '421', '0', '420', '0', '0');
INSERT INTO `p_data` VALUES ('广东', '33', '7', '7311', '8', '7270', '4', '0');
INSERT INTO `p_data` VALUES ('广西', '14', '27', '1639', '2', '1623', '1', '2');
INSERT INTO `p_data` VALUES ('新疆', '0', '13', '1008', '3', '1005', '0', '0');
INSERT INTO `p_data` VALUES ('江苏', '11', '9', '2235', '0', '2224', '0', '0');
INSERT INTO `p_data` VALUES ('江西', '0', '16', '1383', '1', '1382', '0', '0');
INSERT INTO `p_data` VALUES ('河北', '1', '66', '2005', '7', '1997', '0', '0');
INSERT INTO `p_data` VALUES ('河南', '48', '177', '3183', '22', '3113', '0', '3');
INSERT INTO `p_data` VALUES ('浙江', '19', '285', '3138', '1', '3118', '1', '0');
INSERT INTO `p_data` VALUES ('海南', '0', '40', '288', '6', '282', '0', '0');
INSERT INTO `p_data` VALUES ('湖北', '1', '1', '68399', '4512', '63886', '0', '0');
INSERT INTO `p_data` VALUES ('湖南', '0', '0', '1393', '4', '1389', '0', '0');
INSERT INTO `p_data` VALUES ('澳门', '1', '0', '83', '0', '82', '0', '0');
INSERT INTO `p_data` VALUES ('甘肃', '0', '4', '681', '2', '679', '0', '0');
INSERT INTO `p_data` VALUES ('福建', '65', '1', '3285', '1', '3219', '7', '0');
INSERT INTO `p_data` VALUES ('西藏', '0', '0', '1', '0', '1', '0', '0');
INSERT INTO `p_data` VALUES ('贵州', '0', '0', '185', '2', '183', '0', '0');
INSERT INTO `p_data` VALUES ('辽宁', '2', '100', '1675', '2', '1671', '0', '26');
INSERT INTO `p_data` VALUES ('重庆', '3', '0', '713', '6', '704', '0', '0');
INSERT INTO `p_data` VALUES ('陕西', '2', '0', '3283', '3', '3278', '0', '0');
INSERT INTO `p_data` VALUES ('青海', '0', '2', '147', '0', '147', '0', '0');
INSERT INTO `p_data` VALUES ('香港', '260800', '0', '332619', '9382', '62437', '72', '0');
INSERT INTO `p_data` VALUES ('黑龙江', '1', '0', '2984', '13', '2970', '0', '0');

-- ----------------------------
-- Table structure for `report`
-- ----------------------------
DROP TABLE IF EXISTS `report`;
CREATE TABLE `report` (
  `stu_id` int(20) NOT NULL,
  `stu_name` varchar(20) NOT NULL,
  `stu_faculty` varchar(20) NOT NULL,
  `stu_class` varchar(20) NOT NULL,
  `test_report` varchar(200) DEFAULT NULL,
  `report_time` date DEFAULT NULL,
  PRIMARY KEY (`stu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of report
-- ----------------------------
INSERT INTO `report` VALUES ('2', '张三', '计算机科学与技术学院', '软工183', '', '2022-04-15');
INSERT INTO `report` VALUES ('3', '李四', '计算机科学与技术学院', '软工183', 'http://localhost:8080/java_project/campus_system/file/report/16502080887811650110047291mmexport1649039147405.jpg', '2022-04-17');
INSERT INTO `report` VALUES ('4', '麻子', '外国语学院', '英语181', 'http://localhost:8080/java_project/campus_system/file/report/16502074444371650110047291mmexport1649039147405.jpg', '2022-04-17');
INSERT INTO `report` VALUES ('19', '贝壳汉姆', '计算机科学与技术学院', '软工183', 'http://localhost:8080/java_project/campus_system/file/report/16502033130411650110047291mmexport1649039147405.jpg', '2022-04-17');

-- ----------------------------
-- Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `role_id` int(20) NOT NULL,
  `role_name` varchar(20) NOT NULL,
  `role_cname` varchar(20) NOT NULL,
  `role_describe` varchar(20) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', 'admin', '管理员', '超级权利');
INSERT INTO `role` VALUES ('2', 'teacher', '老师', '审核');
INSERT INTO `role` VALUES ('3', 'stu', '学生', '普通使用');

-- ----------------------------
-- Table structure for `stu_info`
-- ----------------------------
DROP TABLE IF EXISTS `stu_info`;
CREATE TABLE `stu_info` (
  `stu_id` int(20) NOT NULL,
  `stu_name` varchar(20) NOT NULL,
  `stu_photo` varchar(200) DEFAULT NULL,
  `stu_sex` varchar(2) NOT NULL,
  `stu_age` varchar(20) NOT NULL,
  `stu_address` varchar(20) NOT NULL,
  `stu_birth` date NOT NULL,
  `stu_phone` varchar(20) NOT NULL,
  `stu_email` varchar(20) NOT NULL,
  `stu_faculty` varchar(20) NOT NULL,
  `stu_major` varchar(20) NOT NULL,
  `stu_class` varchar(20) NOT NULL,
  `stu_time` date NOT NULL,
  `stu_password` varchar(20) NOT NULL DEFAULT '888',
  PRIMARY KEY (`stu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stu_info
-- ----------------------------
INSERT INTO `stu_info` VALUES ('2', '张三', 'http://localhost:8080/java_project/campus_system/file/student/1650102771159IMG_20210617_170856.jpg', '男', '21', '重庆市', '1999-11-02', '15326458945', '1054526568@qq.com', '计算机科学与技术学院', '软件工程', '软工183', '2018-09-01', '888');
INSERT INTO `stu_info` VALUES ('3', '李四', '', '男', '23', '南京市', '1998-08-03', '15324215655', '405596617@qq.com', '计算机科学与技术学院', '软件工程', '软工183', '2018-09-01', '888');
INSERT INTO `stu_info` VALUES ('4', '麻子', '', '男', '25', '内蒙古', '1996-08-03', '13243545215', '1254599878@qq.com', '外国语学院', '英语', '英语181', '2018-09-01', '888');
INSERT INTO `stu_info` VALUES ('19', '贝壳汉姆', '', '男', '20', '杭州市', '1999-11-02', '13542151541', '1254685498@qq.com', '计算机科学与技术学院', '软件工程', '软工183', '2022-04-10', '888');
INSERT INTO `stu_info` VALUES ('41', '李淳', null, '女', '22', '贵州省毕节市', '2015-04-12', '13232432523', '1324325345@qq.com', '计算机科学与技术学院', '软件工程', '软工183', '2022-04-12', '888');
INSERT INTO `stu_info` VALUES ('42', '张迪', '', '女', '23', '浙江省温州市', '1998-05-29', '17685310878', '1235468745@qq.com', '外国语学院', '英语', '英语181', '2022-07-29', '888');

-- ----------------------------
-- Table structure for `teacher`
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher` (
  `teacher_id` int(20) NOT NULL AUTO_INCREMENT,
  `teacher_name` varchar(20) NOT NULL,
  `teacher_age` varchar(20) NOT NULL,
  `teacher_photo` varchar(200) DEFAULT NULL,
  `teacher_address` varchar(200) NOT NULL,
  `teacher_birth` date NOT NULL,
  `teacher_sex` varchar(2) NOT NULL,
  `teacher_phone` varchar(20) NOT NULL,
  `teacher_email` varchar(20) NOT NULL,
  `teacher_faculty` varchar(20) NOT NULL,
  `teacher_password` varchar(20) NOT NULL DEFAULT '888',
  PRIMARY KEY (`teacher_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of teacher
-- ----------------------------
INSERT INTO `teacher` VALUES ('1', '王五', '22', 'http://localhost:8080/java_project/campus_system/file/teacher/165020840669820210918122925.jpg', '北京市', '2000-10-28', '男', '15329062755', '1057199797@qq.com', '管理员', '888');
INSERT INTO `teacher` VALUES ('25', '毛老师', '30', 'http://localhost:8080/java_project/campus_system/file/teacher/1650187149190p2559774751.jpg', '贵阳市', '1992-08-28', '女', '15254565284', '2154566518@qq.com', '计算机科学与技术学院', '888');
INSERT INTO `teacher` VALUES ('26', '李老师', '28', '', '南京市', '1994-08-28', '男', '15254562154', '2154552154@qq.com', '外国语学院', '888');
INSERT INTO `teacher` VALUES ('30', '王老师', '30', null, '温州市', '1992-08-02', '男', '15326458795', '1526499875@qq.com', '外国语学院', '888');

-- ----------------------------
-- Table structure for `user_role`
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `user_id` int(20) NOT NULL,
  `role_id` int(20) NOT NULL,
  KEY `role_id` (`role_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`),
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `account` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES ('1', '1');
INSERT INTO `user_role` VALUES ('2', '3');
INSERT INTO `user_role` VALUES ('25', '2');
INSERT INTO `user_role` VALUES ('26', '2');
INSERT INTO `user_role` VALUES ('30', '2');
INSERT INTO `user_role` VALUES ('3', '3');
INSERT INTO `user_role` VALUES ('4', '3');
INSERT INTO `user_role` VALUES ('19', '3');
INSERT INTO `user_role` VALUES ('41', '3');
INSERT INTO `user_role` VALUES ('42', '3');
